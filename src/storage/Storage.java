package storage;

import java.util.ArrayList;

import application.model.Customer;
import application.model.Order;
import application.model.PriceList;
import application.model.Product;
import application.model.Reservation;
import application.model.TourBooking;

public class Storage {
	private ArrayList<Customer> lstCustomers;
	private ArrayList<Product> lstProducts;
	private ArrayList<PriceList> lstPriceLists;
	private ArrayList<Order> lstOrders;
	private ArrayList<TourBooking> lstTourBookings;
	private ArrayList<Reservation> lstReservations;

	public Storage() {
		lstCustomers = new ArrayList<Customer>();
		lstProducts = new ArrayList<Product>();
		lstPriceLists = new ArrayList<PriceList>();
		lstOrders = new ArrayList<Order>();
		lstTourBookings = new ArrayList<TourBooking>();
		lstReservations = new ArrayList<Reservation>();
	}

	public ArrayList<Customer> getCustomers() {
		return new ArrayList<Customer>(lstCustomers);
	}

	public ArrayList<Product> getProducts() {
		return new ArrayList<Product>(lstProducts);
	}

	public ArrayList<PriceList> getPriceLists() {
		return new ArrayList<PriceList>(lstPriceLists);
	}

	public ArrayList<Order> getOrders() {
		return new ArrayList<Order>(lstOrders);
	}

	public ArrayList<TourBooking> getTourBookings() {
		return new ArrayList<TourBooking>(lstTourBookings);
	}

	public ArrayList<Reservation> getReservations() {
		return new ArrayList<Reservation>(lstReservations);
	}

	public void removeCustomer(Customer c) {
		if (lstCustomers.contains(c)) {
			lstCustomers.remove(c);
		}
	}

	public void removeProduct(Product p) {
		if (lstProducts.contains(p)) {
			lstProducts.remove(p);
		}
	}

	public void removePriceList(PriceList p) {
		if (lstPriceLists.contains(p)) {
			lstPriceLists.remove(p);
		}
	}

	public void removeOrder(Order o) {
		if (lstOrders.contains(o)) {
			lstOrders.remove(o);
		}
	}

	public void removeTourBooking(TourBooking t) {
		if (lstTourBookings.contains(t)) {
			lstTourBookings.remove(t);
		}
	}

	public void removeReservation(Reservation r) {
		if (lstReservations.contains(r)) {
			lstReservations.remove(r);
		}
	}

	public void addCustomer(Customer c) {
		if (!lstCustomers.contains(c)) {
			lstCustomers.add(c);
		}
	}

	public void addProduct(Product p) {
		if (!lstProducts.contains(p)) {
			lstProducts.add(p);
		}
	}

	public void addPriceList(PriceList p) {
		if (!lstPriceLists.contains(p)) {
			lstPriceLists.add(p);
		}
	}

	public void addOrder(Order o) {
		if (!lstOrders.contains(o)) {
			lstOrders.add(o);
		}
	}

	public void addTourBooking(TourBooking t) {
		if (!lstTourBookings.contains(t)) {
			lstTourBookings.add(t);
		}
	}

	public void addReservation(Reservation r) {
		if (!lstReservations.contains(r)) {
			lstReservations.add(r);
		}
	}
}
