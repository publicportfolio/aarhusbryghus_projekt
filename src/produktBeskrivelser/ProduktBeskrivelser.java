package produktBeskrivelser;

public class ProduktBeskrivelser {
	public static String klosterbrygBeskrivelse = "Klosterbryg, 6% alk. er udviklet til Øm Kloster museum, i anledning af Øl på Øm. Øl på Øm er et årligt arrangement, der sætter fokus på livet i middelalderen, herunder øllets betydning.\r\n" +
			"\r\n" +
			"I middelalderen var vandet ikke rent. Man kendte ikke til vigtigheden af rent vand. Ofte lå brønden og møddingen side om side. Man var dog klar over at det var farligt at drikke vandet. Maden var røget og saltet. Man spiste kød, fisk og kål.\r\n" +
			"\r\n" +
			"Ved ølbrygning skal den knuste malt opløses i varmt vand. Man fandt hurtigt ud af, at hvis man kogte vandet og tilsatte humle til øllet, så fik man en drik, man ikke blev syg af, og som kunne holde sig.\r\n" +
			"\r\n" +
			"Hver dansker drak i middelalderen 6-8 liter øl om dagen! En almindelig landhusholdning med 15 personer skulle derfor forsynes med 90-120 liter øl hver dag! Bryggerset var vigtigere end køkkenet, og oftest ville en landhusholdning have en pige, der ikke lavede andet end fremstille malt og brygge øl.\r\n" +
			"\r\n" +
			"På Øm Kloster kan man i dag se ruinerne af både køkken og bryggers. Bryggerset var større end køkkenet! Der var på Øm Kloster 3 bryggerier, som var i anvendelse samtidig.\r\n" +
			"\r\n" +
			"Klosterbryg er brygget på pilsnermalt, hvedemalt og karamelmalt. Det er krydret med tre forskellige typer af aromahumle, som matcher maltene. Dette er en øl med 6% alkohol, som er i stil med India Pale Ale.\r\n" +
			"\r\n" +
			"Der er udelukkende anvendt aromahumle. Engelsk Fuggles og Styrian Goldings sammen med tysk Perle. Fuggles og Goldings er de to mest klassiske engelske aromahumletyper. De giver øllen en svagt parfumeret duft, med mindelser om clementiner.\r\n" +
			"\r\n" +
			"Aromahumlen gør øllen meget blød i smagen. Med en temmelig stor dosering oplever man ikke den grove bitterhed, som humle ellers kan give.\r\n" +
			"\r\n" +
			"Dette er en øl, som er velegnet til fisk og lyse kødretter. Den er absolut også velegnet til at nyde som den er.\r\n" +
			"\r\n" +
			"Drik den ved kældertemperatur, cirka 6 grader Celsius, evt. en anelse varmere, for at aromaen kan komme til sin ret.";

	public static String sweetGeogiaBrown = "Den blev lavet til Fodvarmeriets telt på jazz-festivalen i Århus, sommeren 2009. Sweet Georgia Brown\r\n" +
			"\r\n" +
			"Der var tale om en bestilling fra Mike Wilson (Cockney Pub) som bad om en mørk, fyldig øl med dette navn. Den er brygget  og gæret efter inspiration fra de engelske brown ales, men er undergæret.\r\n" +
			"\r\n" +
			"Den resulterende øl på 5,5% er mørk, fyldig og med en let sødme, som klæder bitterheden fra de mørke malte og de engelske aromahumler.\r\n" +
			"\r\n" +
			"Fås filtreret på 60 cl flasker.";

	public static String extraPilsner ="Lanceredes som ufiltreret øl til Danmarks Grimmeste Festival som “danmarks Grimmeste Pilsner – med den største smag”. Som filtreret øl kendes den som Aarhus Extra Pilsner. Den kåredes af Børsen ved en blindsmagning af 38 forskellige danske pilsnere til den 3. bedste pilsner i Danmark.\r\n" +
			"\nDer er tale om en øl med 5% alkohol, brygget på pilsnermalt, hvedemalt og karamelmalt. Den er som de øvrige øl fra Aarhus Bryghus udelukkende humlet med aromahumle. Humlingen er inspireret af de tjekkiske øl, og derfor er denne øl mere bitter end de gængse danske pilsnere.";

	public static String celebration = "I anledning af vores 5-års jubilæum i 2010 bryggede vi Celebration.\r\n" +
			"\r\n" +
			"Der er tale om en gylden øl på 6,5%, brygget på pilsnermalt, münchenermalt, karamelmalt og hvedemalt, krydret med Perle, Fuggles, Goldings og Cascade humle i rigt mål.\r\n" +
			"\r\n" +
			"Det var i 2010 vores mest humlede øl, men langt fra en “humlebombe”.\r\n" +
			"\r\n" +
			"Humlerne giver tilsammen en fløjlsblød, behagelig bitterhed, lige når øllen kommer ind i munden. Efterfølgende kommer et balanceret smagsindtryk af humler og de mørkere malte.\r\n" +
			"Denne øl finder vi er velegnet til kraftigt krydrede kødretter.\r\n" +
			"\r\n" +
			"Find selv en anledning til fejring og nyd et glas ved cirka 8 grader Celsius!\r\n" +
			"\r\n" +
			"Fås på 60 cl flasker";

	public static String jazzClassic = "En behagelig mørk classic type. Let tilgængelig med en balanceret bitterhed, kun krydret med aromahumlen Perle. Kun fra fad.\r\n" +
			"\r\n" +
			"Jazz Classic er brygget specifikt til Fodvarmeriets telt ved Aarhus Jazz Festival 2011.";

	public static String blondie = "Blondie er vores bud på en frisk sommerøl. Der er tale om en klassisk engelsk Pale Ale, krydret med humleblomster fra Fuggles og East Kent Goldings varianterne, to af de kendteste.\r\n" +
			"\r\n" +
			"Den friske øl er mild og blid, og man bliver hurtigt rolig i Blondies selskab. Hun er nu også kun på 5%.\r\n" +
			"\r\n" +
			"Fås kun på fad.";

	public static String forAarsBryg = "Forårsbryg er en lys Dortmunder type. Det er en hilsen til solens første stråler og det spirende forår. Nyd den til fed fisk, æg og rejer eller til lyse oste. Eller nyd den svøbt i et tæppe til solens første stråler.\r\n" +
			"7% alk. Fås få flaske og på fad.";

	public static String indiaPaleAle = "Aarhus Bryghus IPA på 6% alk. er en India Pale Ale, inspireret af de moderne IPA’er fra den amerikanske vestkyst.\r\n" +
			"\r\n" +
			"Den gyldne ale er krydret med 5 forskellige humler:\r\n" +
			"\r\n" +
			"Engelske Fuggles og East Kent Goldings, tysk Cascade og Perle og amerikansk Mosaic.\r\n" +
			"\r\n" +
			"Alle humler er aroma humler. De fleste er tilsat urtkedlen lige inden nedsvaling, mens 20% er tilsat som ”tør-humle” til det filtrerede kolde øl lige før tapning.\r\n" +
			"\r\n" +
			"Nyd denne øl med de aromatiske humle-olier som den er, eller som ledsager til asiatisk mad tilberedt i en wok med ingefær, hvidløg og rød peber.";

	public static String julebryg = "Julebryg fra Aarhus Bryghus.\r\n" +
			"\r\n" +
			"Til Jul skal en øl være lidt kraftigere og lidt mørkere, og gerne ekstra krydret.\r\n" +
			"\r\n" +
			"Julebryg fra Aarhus Bryghus med 6% alkohol er brygget på pilsnermalt, hvedemalt, münchenermalt, karamelmalt og farvemalt. Den er krydret med 3 forskellige aromahumler, som matcher maltene, samt med appelsinsaft, appelsinskalolie og nellike.\r\n" +
			"\r\n" +
			"Aromahumlen giver øllen en svagt parfumeret duft, med mindelser om clementiner. Dette forstærkes af saft og skalolie fra appelsinerne.\r\n" +
			"\r\n" +
			"Den mørke farve skyldes brugen af farvemalt og münchenermalt.\r\n" +
			"\r\n" +
			"Smagen er mild og samtidig fyldig, på trods af den store dosering af aromahumle. Krydringen er diskret, den skal kun anes. Med den store fylde og meget humling, er der ikke behov for at tilsætte så meget kulsyre. Dette er med til at give den milde, behagelige smag.\r\n" +
			"\r\n" +
			"Nyd denne øl som den er til selskabelighed i Julen, til julefrokosten eller i festligt lag som en behagelig, mild tørstslukker. Vi anbefaler IKKE denne Julebryg til andestegen Juleaften. Det er den ikke kraftig og fyldig nok til. Derimod anbefaler vi vores Imperial Stout eller Fregatten Jylland som ledagere til andestegen.\r\n" +
			"\r\n" +
			"Drik Julebryggen ved cirka 8 grader Celsius for at aromaen kan komme til sin ret.\r\n" +
			"\r\n" +
			"Julebryg fås kun som filtreret øl, enten på 20 liters fustage eller på 60 cl flaske.";

	public static String juletoenden = "Selv den fattigste husholdning, måtte til julen brygge sig en beholdning af stærkere art. Denne kaldtes for Juletønden. At lægge JuletøndenJuletønden ind var betegnelsen for et særligt godt traktement.\r\n" +
			"\r\n" +
			"Juletønden fra Aarhus Bryghus er en mørk specialøl brygget på bygmalt og sukker, krydret med humle. Kan med fordel nydes til den tunge julemad.\r\n" +
			"\r\n" +
			"  Aarhus Bryghus Juletønden er med sine 8% alkohol en tung sag. Der er anvendt mørk farvemalt som er ristet på samme måde som kaffe. Dette matches af en kraftig humling med de fineste tyske aromahumler. Münchenermalt og karamelmalt er med til at give krop og fylde, sukkeret trækker den anden vej og hjælper til at øllen ikke bliver for tung og klistret.";

	public static String imperialStout = "Aarhus Bryghus Imperial Stout er inspireret af den engelske Imperial Stout. Imperial Stout er som øltype oprindeligt udviklet til Zarens hof i Skt. Petersborg. I 1800-tallet var denne øltype meget populær. Øllet bryggedes i England og sejledes til Rusland. For at holde til den lange rejse gjorde man øllen ekstra stærk i forhold til de Stout, der solgtes i England. Øllet tappedes ofte på champagneflasker.\r\n" +
			"\r\n" +
			"Det høje alkoholindhold på 8% er med til at sikre holdbarheden af det sorte øl. Der er anvendt uraffineret rørsukker fra Mauritius som ekstra smagsgiver. De fleste engelske ales er brygget med uraffineret rørsukker. Ved at anvende uraffineret sukker, får man smagsnoter fra rørsukkeret med i den færdige øl. Farven er i den lyse ende af skalaen. Til gengæld er der ikke tilsat karamelkulør i denne Stout!\r\n" +
			"\r\n" +
			"Smagen af Aarhus Bryghus Stout er tyk og fed. Selvom det er en kraftig øl, er det ikke humlen, der er dominerende. Det gør den lettere at gå til, end mange andre sorte øl. Der er endvidere en svag sødme fra rørsukkeret, der er balanceret med humlen. Med tiden vil humlebitterheden fortage sig, og sødmen vil dominere.\r\n" +
			"\r\n" +
			"Den unge Stout vil være en god ledsager til et stykke mørkt kød eller en kraftig gryderet. Den vil helt sikkert være oplagt at drikke til Juleanden eller flæskestegen. Dansk Julemad er udviklet gennem århundreder, hvor vi altid har drukket øl til. Derfor egner øl sig bare bedre til det danske køkken.\r\n" +
			"\r\n" +
			"Aarhus Bryghus Stout vil også være en glimrende ledsager til alle oste. Prøv at servere rugbrød til ostene. Rugbrødet og Stouten vil løfte og komplimentere ostene.\r\n" +
			"\r\n" +
			"Serveringstemperaturen for Aarhus Bryghus Stout må gerne være cirka 10 grader. Det gør ikke noget at den yderligere varmes i glasset, det kan den sagtens tåle.\r\n" +
			"\r\n" +
			"Med tiden vil humlebitterheden aftage og sødmen vil dominere. Om cirka 2 år vil Stout være en spændende ledsager til en kraftig chokoladekage!\r\n" +
			"\r\n" +
			"Gem en kasse, glem den og glæd dig! Men vent ikke for længe. ved at gemme den mørkt og køligt er det ikke usandsynligt, at den vil holde sig i årevis.";

	public static String oldStrongAle = "Old Strong Ale er brygget af udvalgte bygmalte, sukker og aromahumle.\r\n" +
			"\r\n" +
			"Old Strong Ale er en øl brygget i samarbejde med Den Gamle By i Aarhus. Den Gamle Bys Bryggerlaug laver deres egen malt og brygger på et lille anlæg i kælderen under bryggeriet i Ålborggården.\r\n" +
			"\r\n" +
			"Malten der fremstilles, tørres over åben ild i en 500 år gammel ”kølle” (udposning af skorstenen med et gulv af porøse mursten). Røgen fra den åbne ild giver malten en let røget smag.\r\n" +
			"\r\n" +
			"En anelse af denne malt anvendes ved brygningen af Aarhus Bryghus Old Strong Ale.\r\n" +
			"\r\n" +
			"Aarhus Bryghus Old Strong Ale er en gylden, majestætisk øl, krydret med klassiske engelske humletyper. Overgæren sammen med humlerne, den store mængde malt, herunder malten fra Den Gamle By, giver en kraft til øllen, som blander sødme, bitterhed og et stræjf af røg til en perfekt harmoni.\r\n" +
			"\r\n" +
			"Opskriften er baseret på gamle håndskrevne noter om gammeltøl og landøl fra Vendsyssel, oprindeligt indsamlet af bryggeren på Vendia i Hjørring.";

	public static String fregattenJylland = "Fregatten Jylland er brygget på bestilling af museet i Ebeltoft af samme navn. Det er en stærk fætter på 8% alkohol, brygget på Fregatten Jyllandpilsnermalt, münchenermalt, karamelmalt og farvemalt, tilsat en sjat æblesaft, og krydret med en ordentlig nævefuld af aromahumle.\r\n" +
			"\r\n" +
			"Smagen kan bedst beskrives som en “winter-warmer”, noget der varmer i en kold tid. Der er sødme fra den stærke øl, en smule syre fra æblerne til at friske op, og bitterhed fra humlen, der hænger længe i munden.\r\n" +
			"\r\n" +
			"Nyd den til en kraftig gryderet, eksempelvis på vildt og svampe, eller drik den som sømandskost med et glas mørk rom!\r\n" +
			"\r\n" +
			"Øllet bør nydes ved cirka 10-12 grader for at få glæde af alle de spændende aromaer. Den vil modne med tiden, idet humlebitterheden vil aftage og den bagvedliggende sødme vil blive mere dominerende. Fregatten Jylland fås filtreret på 60 cl flaske. Holdbarhed 3 år fra tappedato. Alkohol 8% vol.";

	public static String tribute2017 = "Tribute 2017 er en hyldest til Aarhus som Europæisk Kulturhovedstad. Tribute er en kraftig Barleywine på 9% alk som er blevet tørhumlet med Fuggles, Goldings og den amerikanske Mosaic-humle. Dermed har vi tilført ”fedme” til den i forvejen kraftige øl.\r\n" +
			"\r\n" +
			"Aarhus Bryghus Tribute 2017 er tappet på 60 cl flasker efter devisen ”Øl til deling – Øl til nydelse”.";

	public static String blackMonster2017 = "Årets udgave af Black Monster er præget af temaet for Aaarhus som Europæisk Kulturhovedstad: RETHINK.\r\n" +
			"\r\n" +
			"Vi har i år brygget Black Monster med en tilsætning af single estate kaffe fra skråningerne på Mount Kilimanjaro. Efterfølgende er den modnet på romfade. Endelig er den tørhumlet med den samme humle som vi har anvendt i brygget, således at der er tale om en single hop øl, brygget på den tyske humle Polaris. 10%\r\n" +
			"\r\n" +
			"Der er tappet et yderst begrænset oplag af denne eksklusive øl, som fra idag vil kunne købes på bryggeriet. Vi vil herefter begynde distribution udelukkende til specialhandelen og enkelte restauratører i Århus og omegn. Aarhus Bryghus Black Monster er tappet på 60 cl flasker efter devisen ”Øl til deling – Øl til nydelse”.";



}

