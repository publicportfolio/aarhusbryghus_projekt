package application.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;

import application.model.Corporate;
import application.model.Customer;
import application.model.Order;
import application.model.OrderLine;
import application.model.Payment;
import application.model.PaymentMethod;
import application.model.PriceList;
import application.model.PriceListLine;
import application.model.Private;
import application.model.Product;
import application.model.Reservation;
import application.model.TourBooking;
import application.model.evaluators.FieldEvaluator;
import application.model.evaluators.IntegerEvaluator;
import application.model.evaluators.MailEvaluator;
import application.model.evaluators.PriceEvaluator;
import application.model.products.Beer;
import application.model.products.CarbonicAcid;
import application.model.products.Delivery;
import application.model.products.DraftBeerMachine;
import application.model.products.GiftBasket;
import application.model.products.Glass;
import application.model.products.Keg;
import application.model.products.Tour;
import application.model.products.Voucher;
import produktBeskrivelser.ProduktBeskrivelser;
import storage.Storage;

public class Service {
	private Storage storage;
	private static Service service;

	private Service() {
		storage = new Storage();
	}

	public static void shutDownService() {
		service = null;
	}

	public static Service getService() {
		if (service == null) {
			service = new Service();
		}
		return service;
	}

	// ------------------------------------------------------------------------------------------

	public Private createPrivateCustomer(String name, String address, int zipCode, int phone, String mail) {
		Private p = null;

		if (searchCustomerByPhone(phone) == null) {
			p = new Private(name, address, zipCode, phone, mail);
			storage.addCustomer(p);
		}

		return p;
	}

	public Corporate createCorporateCustomer(String name, String address, int zipCode, int phone, String mail, int cvr,
			String contactPerson) {
		Corporate c = null;

		if (searchCustomerByPhone(phone) == null) {
			c = new Corporate(name, address, zipCode, phone, mail, cvr, contactPerson);
			storage.addCustomer(c);
		}

		return c;
	}

	public Order createOrder(LocalDateTime date, PriceList priceList) {
		Order o = new Order(date, priceList);
		storage.addOrder(o);
		return o;
	}

	public Payment createPayment(Order order, PaymentMethod paymentMethod, LocalDateTime date, double payed) {

		Payment p = null;

		
		if (order.getBalance() >= payed) {

			p = new Payment(paymentMethod, date, payed);

			if (!order.getLstPayments().contains(p)) {
				order.addPayment(p);
			}
			order.setBalance(order.getBalance() - p.getPayed());
		} else {
			throw new IllegalArgumentException("Betalingen er for stor");
		}
		
		if (order.getLstOrderlines().size() == 1) {
			updateStock(order);
		}

		return p;
	}

	public TourBooking createTourBooking(LocalDateTime date, OrderLine orderLine) {
		TourBooking t = new TourBooking(date, orderLine);
		orderLine.setTourbooking(t);
		storage.addTourBooking(t);
		return t;
	}

	public Reservation createReservation(LocalDate fromDate, LocalDate toDate, OrderLine orderLine) {
		Reservation r = new Reservation(fromDate, toDate, orderLine);
		orderLine.setReservation(r);
		storage.addReservation(r);
		return r;
	}

	public PriceList createPriceList(String title) {
		PriceList p = new PriceList(title);
		storage.addPriceList(p);
		return p;
	}

	public Product createProduct(String name, String description, int inStock) {
		Product p = new Product(name, description, inStock);
		storage.addProduct(p);
		return p;
	}

	public Voucher createVoucher(String name, String description, int inStock, int stamp) {
		Voucher p = new Voucher(name, description, inStock, stamp);
		storage.addProduct(p);
		return p;
	}

	public Delivery createDelivery(String name, String description, int inStock) {
		Delivery d = new Delivery(name, description, inStock);
		storage.addProduct(d);
		return d;
	}

	public Glass createGlass(String name, String description, int inStock) {
		Glass p = new Glass(name, description, inStock);
		storage.addProduct(p);
		return p;
	}

	public Beer createBeer(String name, String description, int inStock, double cl, double alcohol) {
		Beer p = new Beer(name, description, inStock, cl, alcohol);
		storage.addProduct(p);
		return p;
	}

	public Keg createKeg(String name, String description, int inStock, double liter) {
		Keg p = new Keg(name, description, inStock, liter);
		storage.addProduct(p);
		return p;
	}

	public CarbonicAcid createCarbonicAcid(String name, String description, int inStock, double kg) {
		CarbonicAcid p = new CarbonicAcid(name, description, inStock, kg);
		storage.addProduct(p);
		return p;
	}

	public DraftBeerMachine createDraftBeerMachine(String name, String description, int inStock) {
		DraftBeerMachine p = new DraftBeerMachine(name, description, inStock);
		storage.addProduct(p);
		return p;
	}

	public GiftBasket createGiftBasket(String name, String description, int inStock) {
		GiftBasket p = new GiftBasket(name, description, inStock);
		storage.addProduct(p);
		return p;
	}

	public Tour createTour(String name, String description, int inStock) {
		Tour p = new Tour(name, description, inStock);
		storage.addProduct(p);
		return p;
	}

	// ------------------------------------------------------------------------------------------

	public void addCustomerToOrder(Order order, Customer customer) {
		if (order.getCustomer() == null && !customer.getLstOrder().contains(order)) {
			order.setCustomer(customer);
			customer.addOrder(order);
		}
	}

	public void addBeerToGiftBasket(GiftBasket giftBasket, Beer beer) {
		giftBasket.addBeer(beer);
		beer.removeAmountFromStock(giftBasket.getInStock());
	}

	public void addGlassToGiftBasket(GiftBasket giftBasket, Glass glass) {
		giftBasket.addGlass(glass);
		glass.removeAmountFromStock(giftBasket.getInStock());
	}

	// ------------------------------------------------------------------------------------------

	/**
	 * Metode til at tilføje et antal vare til til lagerbeholdningen
	 *
	 * @param product
	 * @param amount
	 */
	public void addProductToStock(Product product, int amount) {
		product.addAmountToStock(amount);
	}

	// ------------------------------------------------------------------------------------------

	/**
	 * Metode til at sætte normaltotalprisen for et produkt på en ordrelinje
	 *
	 * @param priceList
	 * @param orderLine
	 */
	public void setNormalPrice(PriceList priceList, OrderLine orderLine) {
		for (PriceListLine priceListLine : priceList.getLstPriceListLines()) {
			if (priceListLine.getProduct().equals(orderLine.getProduct())) {
				orderLine.setTotalPrice(priceListLine.getPrice() * orderLine.getQuantity());
			}
		}
		orderLine.getOrder().updateBalanceOnOrder();
	}

	/**
	 * Metode til at trække solgte vare ud af lageret
	 *
	 * @param order
	 *
	 */
	public void updateStock(Order order) {

		for (OrderLine orderLine : order.getLstOrderlines()) {

			orderLine.getProduct().addAmountToStock(-orderLine.getQuantity());

		}
	}

	/**
	 * Metode til at opdatere en privatkunde
	 *
	 * @param customer
	 * @param name
	 * @param address
	 * @param zipcode
	 * @param mail
	 * @param phone
	 */
	public void updatePrivateCustomer(Private customer, String name, String address, int zipcode, String mail,
			int phone) {

		customer.setName(name);
		customer.setAddress(address);
		customer.setZipCode(zipcode);
		customer.setPhone(phone);
		customer.setMail(mail);

	}

	/**
	 * Metode til at opdatere en erhvervskunde
	 *
	 * @param customer
	 * @param name
	 * @param address
	 * @param zipcode
	 * @param mail
	 * @param phone
	 * @param cvr
	 * @param contactPerson
	 */
	public void updateCorporateCustomer(Corporate customer, String name, String address, int zipcode, String mail,
			int phone, int cvr, String contactPerson) {

		customer.setName(name);
		customer.setAddress(address);
		customer.setZipCode(zipcode);
		customer.setPhone(phone);
		customer.setMail(mail);
		customer.setCvr(cvr);
		customer.setContactPerson(contactPerson);

	}

	public void updateBeer(Beer beer, String name, String description, int inStock, double cl, double alcohol) {
		beer.setName(name);
		beer.setDescription(description);
		beer.setInStock(inStock);
		beer.setCl(cl);
		beer.setAlcohol(alcohol);
	}

	public void updateVoucher(Voucher voucher, String name, String description, int inStock, int stamp) {
		voucher.setName(name);
		voucher.setDescription(description);
		voucher.setInStock(inStock);
		voucher.setStamp(stamp);
	}

	public void updateDelivery(Delivery delivery, String name, String description, int inStock) {
		delivery.setName(name);
		delivery.setDescription(description);
		delivery.setInStock(inStock);
	}

	public void updateProduct(Product product, String name, String description, int inStock) {
		product.setName(name);
		product.setDescription(description);
		product.setInStock(inStock);
	}

	public void updateKeg(Keg keg, String name, String description, int inStock, double liter) {
		keg.setName(name);
		keg.setDescription(description);
		keg.setInStock(inStock);
		keg.setLiter(liter);

	}

	public void updateCarbonicAcid(CarbonicAcid carbonicAcid, String name, String description, int inStock, double kg) {
		carbonicAcid.setName(name);
		carbonicAcid.setDescription(description);
		carbonicAcid.setInStock(inStock);
		carbonicAcid.setKg(kg);

	}

	public void updateDraftBeerMachine(DraftBeerMachine draftBeerMachine, String name, String description,
			int inStock) {
		draftBeerMachine.setName(name);
		draftBeerMachine.setDescription(description);
		draftBeerMachine.setInStock(inStock);
	}

	public void updatePriceList(PriceList priceList, String name) {
		priceList.setTitle(name);
	}

	/**
	 * Opdaterer antallet af produkterne på lager i gavekurven, når selve gavekurven
	 * skal øges ellers sænkes i lagerantal
	 *
	 * @param giftBasket
	 * @param amount
	 *            (kan både være positivt eller negativt afhængigt af om man fjerner
	 *            eller tilføjer antal på lager for gavekurven
	 * @return boolean for at finde ud af om det kan lade sig gøre
	 */
	public void updateGiftBasketStock(GiftBasket giftBasket, int amount) {
		if ((giftBasket.getInStock() + (amount)) < 0) {
			throw new IllegalArgumentException(
					"Ikke nok på lager af " + giftBasket.getName() + " til at fjerne mængden");
		}
		HashSet<Beer> beers = new HashSet<>();
		HashSet<Glass> glasses = new HashSet<>();
		for (Beer beer : giftBasket.getLstBeers()) {
			beers.add(beer);
			int counter = Collections.frequency(giftBasket.getLstBeers(), beer);
			if ((beer.getInStock() - (counter * amount) < 0)) {
				throw new IllegalArgumentException(
						"Der er ikke nok på lager af " + beer.getName() + " til at opdatere mængden af gavekurven");
			}
		}
		for (Glass glass : giftBasket.getLstGlasses()) {
			glasses.add(glass);
			int counter = Collections.frequency(giftBasket.getLstGlasses(), glass);
			if ((glass.getInStock() - (counter * amount) < 0)) {
				throw new IllegalArgumentException(
						"Der er ikke nok på lager af " + glass.getName() + " til at opdatere mængden af gavekurven");
			}
		}
		for (Beer b : beers) {
			int counter = Collections.frequency(giftBasket.getLstBeers(), b);
			if (amount < 0) {
				b.addAmountToStock(0 - counter * amount);
			} else {
				b.removeAmountFromStock(counter * amount);
			}
		}
		for (Glass g : glasses) {
			int counter = Collections.frequency(giftBasket.getLstGlasses(), g);
			if (amount < 0) {
				g.addAmountToStock(0 - counter * amount);
			} else {
				g.removeAmountFromStock(counter * amount);
			}
		}
		giftBasket.addAmountToStock(amount);
	}

	/**
	 * Metode til at tjekke om en ordre er lukket og færdigbehandlet
	 *
	 * @param order
	 * @return
	 */
	public boolean orderIsClosedForPayment(Order order) {

		boolean closed = false;

		if (order.getLstPayments().size() > 0 && order.getBalance() == 0) {
			closed = true;
		}

		return closed;
	}

	/**
	 * Metode til at finde ordre imellem to valgte datoer
	 *
	 * @param from
	 * @param to
	 * @return
	 */
	public ArrayList<Order> showOrdersByDate(LocalDate from, LocalDate to) {

		ArrayList<Order> result = new ArrayList<>();

		for (Order order : getOrders()) {

			if ((order.getDate().toLocalDate().isAfter(from) || order.getDate().toLocalDate().isEqual(from))
					&& (order.getDate().toLocalDate().isBefore(to) || order.getDate().toLocalDate().isEqual(to))) {
				result.add(order);
			}
		}

		return result;
	}

	/**
	 * Metode til at finde ud af om denefterspurgte ordre findes
	 *
	 * @param ordernumber
	 * @return
	 */
	public Order searchOrderByOrderNumber(int ordernumber, PriceList priceList) {

		Order resultOrder = null;

		for (Order order : Service.getService().getOrders()) {
			if (order.getOrderNumber() == ordernumber && order.getPriceList().equals(priceList)) {
				resultOrder = order;
				break;
			}
		}

		return resultOrder;
	}

	/**
	 * Metode til at finde alle produkter der indeholder inputtet man filtrere efter
	 * i sit navn.
	 *
	 * @param filter
	 * @param priceList
	 * @return
	 */
	public ArrayList<PriceListLine> getPriceListLinesByFilter(String filter, Order order) {

		ArrayList<PriceListLine> result = new ArrayList<>();

		ArrayList<PriceListLine> listToFilter = new ArrayList<>(getUniquePriceListLine(order));

		for (PriceListLine pll : listToFilter) {
			if (pll.getProductName().toLowerCase().contains(filter.toLowerCase())) {
				result.add(pll);
			}
		}

		return result;
	}

	/**
	 * Metode der søger efter kunde ud fra telefonnummer
	 *
	 * @param phone
	 * @return
	 */
	public Customer searchCustomerByPhone(int phone) {
		Customer foundCustomer = null;

		for (Customer customer : this.getCustomers()) {

			if (customer.getPhone() == phone) {
				foundCustomer = customer;
				break;
			}
		}

		return foundCustomer;

	}

	/**
	 * Metode der finder ud af om et givent produkt kræver en reservation
	 *
	 * @param product
	 * @return
	 */
	public boolean checkIfProductNeedsReservation(Product product) {
		boolean needReservation = false;

		if (product instanceof Keg || product instanceof CarbonicAcid || product instanceof DraftBeerMachine) {
			needReservation = true;
		}

		return needReservation;
	}

	/**
	 * Metode der tjekke om man kan trække amount fra instock uden at det bliver
	 * negativt
	 *
	 * @param product
	 * @param amount
	 * @return
	 */
	public boolean checkIfStockIsAvailable(Product product, int amount) {
		if ((product.getInStock() - amount) < 0) {
			return false;
		}
		return true;
	}

	/**
	 * Metode til at finde alle udlejede produkter produktet er udlejet fromDate til
	 * toDate, eller så lange balancen ikke er 0.
	 *
	 * @return
	 */

	public ArrayList<OrderLine> getLeasedProducts() {

		ArrayList<OrderLine> leasedProductOrders = new ArrayList<>();

		for (Order order : storage.getOrders()) {

			if (order.getBalance() > 0) {

				for (OrderLine orderLine : order.getLstOrderlines()) {

					if (checkIfProductNeedsReservation(orderLine.getProduct())) {
						if (orderLine.getReservation().getFromDate().isBefore(LocalDate.now())
								|| orderLine.getReservation().getFromDate().isEqual(LocalDate.now())) {
							leasedProductOrders.add(orderLine);
						}

					}
				}
			}
		}

		return leasedProductOrders;
	}

	// Bliver kun brugt til at udføre Test
	public ArrayList<OrderLine> getLeasedProductsTester(LocalDate l) {

		ArrayList<OrderLine> leasedProductOrders = new ArrayList<>();

		for (Order order : storage.getOrders()) {

			if (order.getBalance() > 0) {

				for (OrderLine orderLine : order.getLstOrderlines()) {

					if (checkIfProductNeedsReservation(orderLine.getProduct())) {
						if (orderLine.getReservation().getFromDate().isBefore(l)) {
							leasedProductOrders.add(orderLine);
						}

					}
				}
			}
		}

		return leasedProductOrders;
	}	

	/**
	 * Metode til at opdatere en ordrelinje.
	 *
	 * @param orderLine
	 * @param quantity
	 * @param totalPrice
	 * @param reservation
	 * @param fromDate
	 * @param toDate
	 * @param tourBooking
	 * @param tourDate
	 * @return
	 */
	public void updateOrderLine(OrderLine orderLine, int quantity, double totalPrice, Reservation reservation,
			LocalDate fromDate, LocalDate toDate, TourBooking tourBooking, LocalDateTime tourDate) {

		orderLine.setQuantity(quantity);
		orderLine.setTotalPrice(totalPrice);
		if (reservation != null) {
			reservation.setFromDate(fromDate);
			reservation.setToDate(toDate);
		}
		if (tourBooking != null) {
			tourBooking.setDate(tourDate);
		}
		if (orderLine.getOrder().getLstPayments().size() > 0) {
			addProductToStock(orderLine.getProduct(), orderLine.getQuantity());
		}
		
		orderLine.getOrder().updateBalanceOnOrder();
	}

	/**
	 * Metode til at opdatere en total på en ordrelinje.
	 *
	 * @param total
	 * @param orderLine
	 */
	public void updateOrderLineTotal(double total, OrderLine orderLine) {
		orderLine.setTotalPrice(total);
		orderLine.getOrder().updateBalanceOnOrder();
	}

	/**
	 * Metode der updatere balancen på en ordre
	 *
	 * @param order
	 */
	public void updateBalanceOnOrder(Order order) {
		order.updateBalanceOnOrder();
	}

	/**
	 * Metode til at returnere de prislistelinjer som ikke allerede er på ordren.
	 *
	 * @param order
	 * @return
	 */
	public ArrayList<PriceListLine> getUniquePriceListLine(Order order) {

		ArrayList<PriceListLine> result;
		result = new ArrayList<>(order.getPriceList().getLstPriceListLines());

		// Liste med elementer der skal fjernes fra result
		ArrayList<PriceListLine> toRemove = new ArrayList<PriceListLine>();
		// Antallet af elementer i ordrens orderlines
		int count = order.getLstOrderlines().size();

		if (order.getLstOrderlines().size() > 0) {

			for (PriceListLine priceListLine : result) {

				for (OrderLine orderLine : order.getLstOrderlines()) {

					// Hvis denne priceListLines produkt er det samme som orderlines produkt
					if (orderLine.getProduct().equals(priceListLine.getProduct())) {
						// Tilføjer elementet til listen med elementer der skal fjernes
						toRemove.add(priceListLine);
						count--;
					}
				}
				// Er der ikke flere elementer i ordrens ordrelinjer at tjekker
				// breaker loopet.
				if (count == 0) {
					break;
				}
			}
			// Nu sletter vi de elementer som allerede er på ordren,
			// så de ikke bliver sendt med retur i den nye liste
			for (int i = 0; i < toRemove.size(); i++) {
				result.remove(toRemove.get(i));
			}
		}
		return result;
	}

	public PriceList getAllPriceListsAsPriceList() {
		PriceList result = new PriceList("Alle lister");
		for (PriceList p : getPriceLists()) {
			for (PriceListLine pl : p.getLstPriceListLines()) {
				result.addPriceListLine(pl);
			}
		}
		return result;
	}

	// ------------------------------------------------------------------------------------------
	// Remove metoder

	public void removeProduct(Product product) {
		if (product != null) {
			storage.removeProduct(product);
		}
	}

	// Sletter en order og tilhørende tour bookinger og reservationer
	public void removeOrder(Order order) {
		if (order != null) {
			storage.removeOrder(order);

			for (OrderLine ol : order.getLstOrderlines()) {
				if (ol.getReservation() != null) {
					storage.removeReservation(ol.getReservation());
				}
				if (ol.getTourbooking() != null) {
					storage.removeTourBooking(ol.getTourbooking());
				}
				if (ol.getProduct() != null) {
					storage.removeProduct(ol.getProduct());
				}
				if (order.getLstPayments().size() > 0) {
					addProductToStock(ol.getProduct(), ol.getQuantity());
				}
			}
			
		}
	}

	public void removePayment(Order order, Payment payment) {

		if (order.getLstPayments().contains(payment)) {
			order.removePayment(payment);
			order.updateBalanceOnOrder();
			if (order.getLstPayments().size() == 0) {
				//Ligger alle vare på lager igen
				for (OrderLine orderline : order.getLstOrderlines()) {
					addProductToStock(orderline.getProduct(), orderline.getQuantity());
				}
				
				
			}

		}

	}

	public void removePriceListLineFromPriceList(PriceList priceList, PriceListLine priceListLine) {
		priceListLine.getProduct().removePriceListLine(priceListLine);
		priceListLine.setProduct(null);
		priceList.removePriceListLine(priceListLine);
	}

	public void removePriceList(PriceList priceList) {
		for (PriceListLine e : priceList.getLstPriceListLines()) {
			e.getProduct().removePriceListLine(e);
			e.setProduct(null);
		}
		priceList.getLstPriceListLines().clear();
		storage.removePriceList(priceList);
	}

	public void removeOrderLine(Order order, OrderLine orderLine) {

		if (orderLine.getReservation() != null) {
			orderLine.setReservation(null);
		}
		if (orderLine.getTourbooking() != null) {
			orderLine.setTourbooking(null);
		}
		if (order.getLstPayments().size() > 0) {
			addProductToStock(orderLine.getProduct(), orderLine.getQuantity());
		}
		
		orderLine.getProduct().removeOrderLines(orderLine);
		order.removeOrderLine(orderLine);
		order.updateBalanceOnOrder();

	}

	public void removeGiftBasket(GiftBasket giftBasket) {
		storage.removeProduct(giftBasket);
	}

	public void removeGlasFromGiftBasket(Glass glass, GiftBasket giftBasket) {
		giftBasket.removeGlass(glass);
		glass.addAmountToStock(giftBasket.getInStock());
	}

	public void removeBeerFromGiftBasket(Beer beer, GiftBasket giftBasket) {
		giftBasket.removeBeer(beer);
		beer.addAmountToStock(giftBasket.getInStock());
	}

	// ------------------------------------------------------------------------------------------

	public ArrayList<Customer> getCustomers() {
		return storage.getCustomers();
	}

	public ArrayList<Product> getProducts() {
		return storage.getProducts();
	}

	public ArrayList<PriceList> getPriceLists() {
		return storage.getPriceLists();
	}

	public ArrayList<Order> getOrders() {
		return storage.getOrders();
	}

	public ArrayList<TourBooking> getTourBookings() {
		return storage.getTourBookings();
	}

	public ArrayList<Reservation> getReservations() {
		return storage.getReservations();
	}

	public ArrayList<Product> getAllBottleBeers() {
		ArrayList<Product> beers = new ArrayList<>();
		for (Product e : Service.getService().getProducts()) {
			if (e instanceof Beer) {
				Beer beer = (Beer) e;
				if (beer.getCl() != 40) {
					beers.add(beer);
				}
			}
		}
		return beers;
	}

	public ArrayList<Product> getAllDraftBeers() {
		ArrayList<Product> beers = new ArrayList<>();
		for (Product e : Service.getService().getProducts()) {
			if (e instanceof Beer) {
				Beer beer = (Beer) e;
				if (beer.getCl() == 40) {
					beers.add(beer);
				}
			}
		}
		return beers;
	}

	public ArrayList<Product> getAllBeers() {
		ArrayList<Product> beers = new ArrayList<>();
		for (Product e : Service.getService().getProducts()) {
			if (e instanceof Beer) {
				beers.add(e);
			}
		}
		return beers;
	}

	public ArrayList<Product> getAllGlasses() {
		ArrayList<Product> glasses = new ArrayList<>();
		for (Product e : Service.getService().getProducts()) {
			if (e instanceof Glass) {
				glasses.add(e);
			}
		}
		return glasses;
	}

	public ArrayList<Product> getAllCarbonicAcids() {
		ArrayList<Product> carbonicAcids = new ArrayList<>();
		for (Product e : Service.getService().getProducts()) {
			if (e instanceof CarbonicAcid) {
				carbonicAcids.add(e);
			}
		}
		return carbonicAcids;
	}

	public ArrayList<Product> getAllKegs() {
		ArrayList<Product> kegs = new ArrayList<>();
		for (Product e : Service.getService().getProducts()) {
			if (e instanceof Keg) {
				kegs.add(e);
			}
		}
		return kegs;
	}

	public ArrayList<Product> getAllVouchers() {
		ArrayList<Product> vouchers = new ArrayList<>();
		for (Product e : Service.getService().getProducts()) {
			if (e instanceof Voucher) {
				vouchers.add(e);
			}
		}
		return vouchers;
	}

	public ArrayList<Product> getAllGiftBaskets() {
		ArrayList<Product> giftBaskets = new ArrayList<>();
		for (Product e : Service.getService().getProducts()) {
			if (e instanceof GiftBasket) {
				giftBaskets.add(e);
			}
		}
		return giftBaskets;
	}

	public ArrayList<Product> getAllTours() {
		ArrayList<Product> tours = new ArrayList<>();
		for (Product e : Service.getService().getProducts()) {
			if (e instanceof Tour) {
				tours.add(e);
			}
		}
		return tours;
	}

	public ArrayList<Product> getAllDeliveries() {
		ArrayList<Product> deliveries = new ArrayList<>();
		for (Product e : Service.getService().getProducts()) {
			if (e instanceof Delivery) {
				deliveries.add(e);
			}
		}
		return deliveries;
	}

	public ArrayList<Product> getAllDraftBeerMachines() {
		ArrayList<Product> draftBeerMachines = new ArrayList<>();
		for (Product e : Service.getService().getProducts()) {
			if (e instanceof DraftBeerMachine) {
				draftBeerMachines.add(e);
			}
		}
		return draftBeerMachines;
	}

	/**
	 * Metode der returnerer alle produkter der ikke er på priceList parametren
	 *
	 * @param priceList
	 * @return
	 */
	public ArrayList<Product> getProductsNotAddedToPriceList(PriceList priceList) {
		ArrayList<Product> products = new ArrayList<>();
		for (Product e : Service.getService().getProducts()) {
			boolean match = false;
			for (PriceListLine e2 : priceList.getLstPriceListLines()) {
				if (e2.getProduct().equals(e)) {
					match = true;
				}
			}
			if (!match) {
				products.add(e);
			}
		}
		return products;
	}

	// --------------------VALIDATORS-------------------------//

	/**
	 * Metode til at validere om et input er tom
	 *
	 * @param s
	 * @return
	 */
	public boolean isEmpty(String s) {

		FieldEvaluator fe = new FieldEvaluator();

		return fe.isValid(s);
	}

	/**
	 * Metode til at validere om et input er en gyldig double
	 *
	 * @param s
	 * @return
	 */
	public boolean isValidDouble(String s) {

		PriceEvaluator ne = new PriceEvaluator();

		return ne.isValid(s);
	}

	/**
	 * Metode til at validere om et input er en gyldig integer
	 *
	 * @param s
	 * @return
	 */
	public boolean isValidInteger(String s) {

		IntegerEvaluator ie = new IntegerEvaluator();

		return ie.isValid(s);
	}

	/**
	 * Metode til at validere om et input er en gyldig email
	 *
	 * @param s
	 * @return
	 */
	public boolean isValidEmail(String s) {

		MailEvaluator me = new MailEvaluator();

		return me.isValid(s);
	}

	// ------------------------------------------------------------------------------------------

	public void initContent() {
		Voucher voucher = service.createVoucher("Klippekort", "Klippekort, der kan bruges til fadøl", 10, 4);

		Delivery delivery = service.createDelivery("Levering", "Radius af 20km", 100);

		Beer klosterBryg = service.createBeer("Klosterbryg", ProduktBeskrivelser.klosterbrygBeskrivelse, 300, 60, 4.6);
		Beer sweetGeorgiaBrown = service.createBeer("Sweet Georgia Brown", ProduktBeskrivelser.sweetGeogiaBrown, 10, 60,
				4.6);
		Beer extraPilsner = service.createBeer("Extra Pilsner", ProduktBeskrivelser.extraPilsner, 10, 60, 4.6);
		Beer celebration = service.createBeer("Celebration", ProduktBeskrivelser.celebration, 10, 60, 4.6);
		Beer blondie = service.createBeer("Blondie", ProduktBeskrivelser.blondie, 10, 60, 4.6);
		Beer foraarsBryg = service.createBeer("Forårsbryg", ProduktBeskrivelser.forAarsBryg, 250, 60, 4.6);
		Beer indiaPaleAle = service.createBeer("India Pale Ale", ProduktBeskrivelser.indiaPaleAle, 10, 60, 4.6);
		Beer juleBryg = service.createBeer("Julebryg", ProduktBeskrivelser.julebryg, 10, 60, 4.6);
		Beer juleToenden = service.createBeer("JuleTønden", ProduktBeskrivelser.juletoenden, 10, 60, 4.6);
		Beer oldStrongAle = service.createBeer("Old Strong Ale", ProduktBeskrivelser.oldStrongAle, 10, 60, 4.6);
		Beer fregattenJylland = service.createBeer("Fregatten Jylland", ProduktBeskrivelser.fregattenJylland, 10, 60,
				4.6);
		Beer imperialStout = service.createBeer("Imperial Stout", ProduktBeskrivelser.imperialStout, 10, 60, 4.6);
		Beer tribute = service.createBeer("Tribute", ProduktBeskrivelser.tribute2017, 10, 60, 4.6);
		Beer blackMonster = service.createBeer("Black Monster", ProduktBeskrivelser.blackMonster2017, 10, 60, 4.6);

		Beer klosterBrygFad = service.createBeer("Klosterbryg", ProduktBeskrivelser.klosterbrygBeskrivelse, 10, 40,
				4.6);
		Beer jazzClassicFad = service.createBeer("Jazz Classic", ProduktBeskrivelser.jazzClassic, 10, 40, 4.6);
		Beer extraPilsnerFad = service.createBeer("Extra Pilsner", ProduktBeskrivelser.extraPilsner, 10, 40, 4.6);
		Beer celebrationFad = service.createBeer("Celebration", ProduktBeskrivelser.celebration, 10, 40, 4.6);
		Beer blondieFad = service.createBeer("Blondie", ProduktBeskrivelser.blondie, 10, 40, 4.6);
		Beer foraarsBrygFad = service.createBeer("Forårsbryg", ProduktBeskrivelser.forAarsBryg, 10, 40, 4.6);
		Beer indiaPaleAleFad = service.createBeer("India Pale Ale", ProduktBeskrivelser.indiaPaleAle, 10, 40, 4.6);
		Beer juleBrygFad = service.createBeer("Julebryg", ProduktBeskrivelser.julebryg, 10, 40, 4.6);
		Beer imperialStoutFad = service.createBeer("Imperial Stout", ProduktBeskrivelser.imperialStout, 10, 40, 4.6);
		Beer specialFad = service.createBeer("Special", "Specialøl", 10, 40, 4.6);

		Product aebleBrus = service.createProduct("Æblebrus", "Sodavand til dem, der ikke drikker", 10);
		Product cola = service.createProduct("Cola", "Sodavand til dem, der ikke drikker", 10);
		Product nikoline = service.createProduct("Nikoline", "Sodavand til dem, der ikke drikker", 10);
		Product sevenUp = service.createProduct("7-Up", "Sodavand til dem, der ikke drikker", 10);
		Product vand = service.createProduct("Vand", "Vand til den tørstige", 10);
		Product pantFustage = service.createProduct("Pant fustage", "", 10);
		Product pantKulsyre = service.createProduct("Pant kulsyre", "", 10);

		Keg klosterBrygFus = service.createKeg("Klosterbryg", ProduktBeskrivelser.klosterbrygBeskrivelse, 10, 20);
		Keg jazzClassicFus = service.createKeg("Jazz Classic", ProduktBeskrivelser.jazzClassic, 10, 25);
		Keg extraPilsnerFus = service.createKeg("Extra Pilsner", ProduktBeskrivelser.extraPilsner, 10, 25);
		Keg celebrationFus = service.createKeg("Celebration", ProduktBeskrivelser.celebration, 10, 20);
		Keg blondieFus = service.createKeg("Blondie", ProduktBeskrivelser.blondie, 10, 25);
		Keg foraarsBrygFus = service.createKeg("Forårsbryg", ProduktBeskrivelser.forAarsBryg, 10, 20);
		Keg indiaPaleAleFus = service.createKeg("India Pale Ale", ProduktBeskrivelser.indiaPaleAle, 10, 20);
		Keg juleBrygFus = service.createKeg("Julebryg", ProduktBeskrivelser.julebryg, 10, 20);
		Keg imperialStoutFus = service.createKeg("Imperial Stout", ProduktBeskrivelser.imperialStout, 10, 20);

		CarbonicAcid sixKilo = service.createCarbonicAcid("Kulsyre", "Kulsyre er godt til brus", 10, 6);
		service.createCarbonicAcid("Kulsyre", "Kulsyre er godt til brus", 10, 4);
		service.createCarbonicAcid("Kulsyre", "Kulsyre er godt til brus", 10, 10);

		DraftBeerMachine oneTap = service.createDraftBeerMachine("1-hanes fadølsanlæg", "Kan udlejes", 10);
		DraftBeerMachine twoTaps = service.createDraftBeerMachine("2-haners fadølsanlæg", "Kan udlejes", 10);
		DraftBeerMachine moreTaps = service.createDraftBeerMachine("Multi hane fadølsanlæg", "Kan udlejes", 10);

		Glass glass = service.createGlass("Glas", "Et glas", 1000);

		GiftBasket twoBeerTwoGlass = service.createGiftBasket("Gaveæske, 2 øl, 2 glas", "En dejlig gaveæske", 10);
		GiftBasket fourBeer = service.createGiftBasket("Gaveæske, 4 øl", "En dejlig gaveæske", 10);
		GiftBasket sixBeer = service.createGiftBasket("Trækasse 6 øl", "En dejlig gaveæske", 10);
		GiftBasket sixBeerTwoGlass = service.createGiftBasket("Gavekurv, 6 øl, 2 glas", "En dejlig gaveæske", 10);
		GiftBasket sixBeerSixGlass = service.createGiftBasket("Trækasse, 6 øl, 6 glas", "En dejlig gaveæske", 10);
		GiftBasket twelveBeerTree = service.createGiftBasket("Trækasse, 12 øl", "En dejlig gaveæske", 10);
		GiftBasket twelveBeerPap = service.createGiftBasket("Papkasse, 12 øl", "En dejlig gaveæske", 10);

		// --------------------CONNECT BEER AND GLASS TO
		// GIFTBASKETS---------------------------------
		for (int i = 0; i < 2; i++) {
			service.addBeerToGiftBasket(twoBeerTwoGlass, klosterBryg);
			service.addGlassToGiftBasket(twoBeerTwoGlass, glass);
			service.addGlassToGiftBasket(sixBeerTwoGlass, glass);
		}

		for (int i = 0; i < 4; i++) {
			service.addBeerToGiftBasket(fourBeer, foraarsBryg);
		}

		for (int i = 0; i < 6; i++) {
			service.addBeerToGiftBasket(sixBeerTwoGlass, foraarsBryg);
			service.addBeerToGiftBasket(sixBeerSixGlass, foraarsBryg);
			service.addBeerToGiftBasket(sixBeer, foraarsBryg);
			service.addGlassToGiftBasket(sixBeerSixGlass, glass);
		}

		for (int i = 0; i < 12; i++) {
			service.addBeerToGiftBasket(twelveBeerTree, klosterBryg);
			service.addBeerToGiftBasket(twelveBeerPap, klosterBryg);
		}
		// ------------------------------------------------------------------------------------------
		Tour tour = service.createTour("Rundvisning", "En times rundvisning med smagsprøver efterfulgt", 10);
		PriceList fredagsbar = service.createPriceList("Fredagsbar");
		PriceList butik = service.createPriceList("Butik");

		fredagsbar.createPriceListLine(100, voucher);

		fredagsbar.createPriceListLine(50, klosterBryg);
		fredagsbar.createPriceListLine(50, sweetGeorgiaBrown);
		fredagsbar.createPriceListLine(50, extraPilsner);
		fredagsbar.createPriceListLine(50, celebration);
		fredagsbar.createPriceListLine(50, blondie);
		fredagsbar.createPriceListLine(50, foraarsBryg);
		fredagsbar.createPriceListLine(50, indiaPaleAle);
		fredagsbar.createPriceListLine(50, juleBryg);
		fredagsbar.createPriceListLine(50, juleToenden);
		fredagsbar.createPriceListLine(50, oldStrongAle);
		fredagsbar.createPriceListLine(50, fregattenJylland);
		fredagsbar.createPriceListLine(50, imperialStout);
		fredagsbar.createPriceListLine(50, tribute);
		fredagsbar.createPriceListLine(50, blackMonster);

		fredagsbar.createPriceListLine(30, klosterBrygFad);
		fredagsbar.createPriceListLine(30, jazzClassicFad);
		fredagsbar.createPriceListLine(30, extraPilsnerFad);
		fredagsbar.createPriceListLine(30, celebrationFad);
		fredagsbar.createPriceListLine(30, blondieFad);
		fredagsbar.createPriceListLine(30, foraarsBrygFad);
		fredagsbar.createPriceListLine(30, indiaPaleAleFad);
		fredagsbar.createPriceListLine(30, juleBrygFad);
		fredagsbar.createPriceListLine(30, imperialStoutFad);
		fredagsbar.createPriceListLine(30, specialFad);
		fredagsbar.createPriceListLine(15, aebleBrus);
		fredagsbar.createPriceListLine(15, cola);
		fredagsbar.createPriceListLine(15, nikoline);
		fredagsbar.createPriceListLine(15, sevenUp);
		fredagsbar.createPriceListLine(10, vand);

		fredagsbar.createPriceListLine(400, sixKilo);

		fredagsbar.createPriceListLine(100, twoBeerTwoGlass);
		fredagsbar.createPriceListLine(130, fourBeer);
		fredagsbar.createPriceListLine(240, sixBeer);
		fredagsbar.createPriceListLine(250, sixBeerTwoGlass);
		fredagsbar.createPriceListLine(290, sixBeerSixGlass);
		fredagsbar.createPriceListLine(390, twelveBeerTree);
		fredagsbar.createPriceListLine(360, twelveBeerPap);

		butik.createPriceListLine(100, voucher);

		butik.createPriceListLine(36, klosterBryg);
		butik.createPriceListLine(36, sweetGeorgiaBrown);
		butik.createPriceListLine(36, extraPilsner);
		butik.createPriceListLine(36, celebration);
		butik.createPriceListLine(36, blondie);
		butik.createPriceListLine(36, foraarsBryg);
		butik.createPriceListLine(36, indiaPaleAle);
		butik.createPriceListLine(36, juleBryg);
		butik.createPriceListLine(36, juleToenden);
		butik.createPriceListLine(36, oldStrongAle);
		butik.createPriceListLine(36, fregattenJylland);
		butik.createPriceListLine(36, imperialStout);
		butik.createPriceListLine(36, tribute);
		butik.createPriceListLine(50, blackMonster);

		butik.createPriceListLine(775, klosterBrygFus);
		butik.createPriceListLine(625, jazzClassicFus);
		butik.createPriceListLine(575, extraPilsnerFus);
		butik.createPriceListLine(775, celebrationFus);
		butik.createPriceListLine(700, blondieFus);
		butik.createPriceListLine(775, foraarsBrygFus);
		butik.createPriceListLine(775, indiaPaleAleFus);
		butik.createPriceListLine(775, juleBrygFus);
		butik.createPriceListLine(775, imperialStoutFus);
		butik.createPriceListLine(200, pantFustage);
		butik.createPriceListLine(1000, pantKulsyre);

		butik.createPriceListLine(400, sixKilo);

		butik.createPriceListLine(250, oneTap);
		butik.createPriceListLine(400, twoTaps);
		butik.createPriceListLine(500, moreTaps);

		butik.createPriceListLine(15, glass);

		butik.createPriceListLine(100, twoBeerTwoGlass);
		butik.createPriceListLine(130, fourBeer);
		butik.createPriceListLine(240, sixBeer);
		butik.createPriceListLine(250, sixBeerTwoGlass);
		butik.createPriceListLine(290, sixBeerSixGlass);
		butik.createPriceListLine(390, twelveBeerTree);
		butik.createPriceListLine(360, twelveBeerPap);

		butik.createPriceListLine(100, tour);

		butik.createPriceListLine(500, delivery);

		// -------------------------------------------------------------------------------------

		Order o1 = createOrder(LocalDateTime.of(2017, 1, 1, 13, 30), fredagsbar);
		Order o2 = createOrder(LocalDateTime.of(2016, 1, 1, 14, 30), fredagsbar);
		Order o3 = createOrder(LocalDateTime.of(2016, 1, 1, 15, 30), butik);
		Order o4 = createOrder(LocalDateTime.of(2017, 1, 1, 15, 30), butik);
		Order o5 = createOrder(LocalDateTime.now(), butik);
		Order o6 = createOrder(LocalDateTime.now(), butik);
		Order o7 = createOrder(LocalDateTime.now(), butik);
		Order o8 = createOrder(LocalDateTime.now(), fredagsbar);

		OrderLine ol1 = o1.createOrderLine(klosterBrygFad, 4);
		setNormalPrice(fredagsbar, ol1);

		OrderLine ol2 = o2.createOrderLine(voucher, 1);
		setNormalPrice(fredagsbar, ol2);
		OrderLine ol3 = o2.createOrderLine(klosterBryg, 3);
		setNormalPrice(fredagsbar, ol3);

		OrderLine ol4 = o3.createOrderLine(oneTap, 1);
		setNormalPrice(butik, ol4);
		OrderLine ol5 = o3.createOrderLine(imperialStoutFus, 2);
		setNormalPrice(butik, ol5);
		OrderLine ol6 = o3.createOrderLine(sixKilo, 1);
		setNormalPrice(butik, ol6);

		OrderLine ol7 = o4.createOrderLine(oneTap, 2);
		setNormalPrice(butik, ol7);

		OrderLine ol8 = o5.createOrderLine(oneTap, 2);
		setNormalPrice(butik, ol8);
		OrderLine ol9 = o5.createOrderLine(blondieFus, 8);
		setNormalPrice(butik, ol9);
		OrderLine ol10 = o5.createOrderLine(pantFustage, 8);
		setNormalPrice(butik, ol10);
		OrderLine ol11 = o5.createOrderLine(glass, 50);
		setNormalPrice(butik, ol11);

		OrderLine ol12 = o6.createOrderLine(celebration, 2);
		setNormalPrice(butik, ol12);

		OrderLine ol13 = o7.createOrderLine(juleToenden, 4);
		setNormalPrice(butik, ol13);

		OrderLine ol14 = o8.createOrderLine(imperialStoutFad, 5);
		setNormalPrice(fredagsbar, ol14);
		OrderLine ol15 = o8.createOrderLine(voucher, 5);
		setNormalPrice(fredagsbar, ol15);
		OrderLine ol16 = o8.createOrderLine(juleBrygFad, 5);
		setNormalPrice(fredagsbar, ol16);

		o1.setBalance(o1.calculateTotalPrice());
		o2.setBalance(o2.calculateTotalPrice());
		o3.setBalance(o3.calculateTotalPrice());
		o4.setBalance(o4.calculateTotalPrice());
		o5.setBalance(o5.calculateTotalPrice());
		o6.setBalance(o6.calculateTotalPrice());
		o7.setBalance(o7.calculateTotalPrice());
		o8.setBalance(o8.calculateTotalPrice());

		createReservation(LocalDate.now().minusDays(2), LocalDate.now().plusDays(4), ol7);
		createReservation(LocalDate.now().minusDays(3), LocalDate.now().minusDays(2), ol8);
		createReservation(LocalDate.now().minusDays(3), LocalDate.now().plusDays(3), ol9);

		Private c1 = createPrivateCustomer("Hans Henrik", "Søndervænget 20", 8000, 70121416, "hansH@gmail.com");
		Corporate c2 = createCorporateCustomer("Viby katedralskole", "Sandhøjvej", 8260, 88888888, "mail@gmail.com",
				1231412341, "Keld");

		addCustomerToOrder(o5, c2);
		addCustomerToOrder(o4, c1);
		addCustomerToOrder(o1, c1);
		addCustomerToOrder(o3, c2);

		createPayment(o1, PaymentMethod.Kontant, LocalDateTime.of(2017, 1, 1, 13, 31), 120);
		createPayment(o3, PaymentMethod.Regning, LocalDateTime.of(2017, 1, 1, 14, 30), 2200);
		createPayment(o5, PaymentMethod.Kontant, LocalDateTime.now().minusDays(3), 800);
		createPayment(o7, PaymentMethod.Klippekort, LocalDateTime.now(), 30);
		createPayment(o8, PaymentMethod.Klippekort, LocalDateTime.now(), 30);

		Service.getService().createTourBooking(LocalDateTime.now(), ol1);
		Service.getService().createTourBooking(LocalDateTime.now().plusDays(6), ol4);
	}
}
