package application.model;

import java.util.ArrayList;

public class PriceList {

	private String title;
	private ArrayList<PriceListLine> lstPriceListLines;

	public PriceList(String title) {
		this.title = title;
		this.lstPriceListLines = new ArrayList<>();
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public ArrayList<PriceListLine> getLstPriceListLines() {
		return lstPriceListLines;
	}

	public void setLstPriceListLines(ArrayList<PriceListLine> lstPriceListLines) {
		this.lstPriceListLines = lstPriceListLines;
	}

	/**
	 * @param price
	 * @param product
	 */
	public PriceListLine createPriceListLine(double price, Product product) {

		PriceListLine pll = new PriceListLine(price, product);
		product.addPriceListLine(pll);
		this.lstPriceListLines.add(pll);

		return pll;
	}

	public void removePriceListLine(PriceListLine priceListLine) {
		if (this.lstPriceListLines.contains(priceListLine)) {
			this.lstPriceListLines.remove(priceListLine);
		}
	}

	@Override
	public String toString() {

		return this.getTitle();
	}

	// ----
	public void addPriceListLine(PriceListLine priceListLine) {
		if (!this.lstPriceListLines.contains(priceListLine)) {
			this.lstPriceListLines.add(priceListLine);
		}
	}

}
