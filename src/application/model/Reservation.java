package application.model;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class Reservation {
	private LocalDate fromDate;
	private LocalDate toDate;
	private OrderLine orderLine;

	public Reservation(LocalDate fromDate, LocalDate toDate, OrderLine orderLine) {
		this.fromDate = fromDate;
		this.toDate = toDate;
		this.orderLine = orderLine;
	}

	public LocalDate getFromDate() {
		return fromDate;
	}

	public void setFromDate(LocalDate fromDate) {
		this.fromDate = fromDate;
	}

	public LocalDate getToDate() {
		return toDate;
	}

	public void setToDate(LocalDate toDate) {
		this.toDate = toDate;
	}

	public OrderLine getOrderLine() {
		return orderLine;
	}

	public void setOrderLine(OrderLine orderLine) {
		this.orderLine = orderLine;
	}




}
