package application.model;

public class PriceListLine {

	private double price;
	private Product product;

	public PriceListLine(double price, Product product) {
		this.price = price;
		this.product = product;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}



	/* Table view Methods */

	public int getInStock() {
		return this.product.getInStock();
	}

	public String getProductName() {
		return this.product.toString();
	}


}
