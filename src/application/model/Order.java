package application.model;

import java.time.LocalDateTime;
import java.util.ArrayList;

import application.model.products.Delivery;
import application.model.products.Voucher;
import application.service.Service;

public class Order {
	private static int counter = 0;
	private int orderNumber;
	private ArrayList<OrderLine> lstOrderlines;
	private ArrayList<Payment> lstPayments;
	private Customer customer;
	private LocalDateTime date;
	private double balance;
	private boolean delivery;
	private PriceList priceList;

	public Order(LocalDateTime date, PriceList priceList) {
		Order.counter++;
		this.setOrderNumber(counter);
		this.lstOrderlines = new ArrayList<>();
		this.customer = null;
		this.date = date;
		this.balance = 0;
		this.lstPayments = new ArrayList<>();
		this.delivery = false;
		this.priceList = priceList;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public LocalDateTime getDate() {
		return date;
	}

	public void setDate(LocalDateTime date) {
		this.date = date;
	}

	public double getBalance() {

		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public ArrayList<OrderLine> getLstOrderlines() {
		return lstOrderlines;
	}

	public void addOrderLine(OrderLine o) {
		if (!this.lstOrderlines.contains(o)) {
			this.lstOrderlines.add(o);
		}
	}

	public void removeOrderLine(OrderLine o) {
		if (this.lstOrderlines.contains(o)) {
			this.lstOrderlines.remove(o);
		}
	}

	public ArrayList<Payment> getLstPayments() {
		return lstPayments;
	}

	public void addPayment(Payment o) {
		if (!this.lstPayments.contains(o)) {
			this.lstPayments.add(o);
		}
	}

	public void removePayment(Payment o) {
		if (this.lstPayments.contains(o)) {
			this.lstPayments.remove(o);
		}
	}

	public boolean isDelivery() {
		return delivery;
	}

	public void setDelivery(boolean delivery) {
		this.delivery = delivery;
	}

	public double calculateTotalPrice() {
		double total = 0;
		for (OrderLine e : lstOrderlines) {
			total += e.getTotalPrice();
		}
		return total;
	}

	public OrderLine createOrderLine(Product product, int amount) {
		OrderLine orderline = new OrderLine(product, amount, this);
		this.lstOrderlines.add(orderline);
		product.addOrderLine(orderline);
		updateBalanceOnOrder();

		if (product instanceof Delivery) {
			this.delivery = true;
		}
		if (this.getLstPayments().size() > 0) {
			product.addAmountToStock(-amount);
		}
		

		return orderline;
	}

	public int getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(int orderNumber) {
		this.orderNumber = orderNumber;
	}

	/**
	 * Metode til at opdatere balancen på en ordre
	 *
	 * @param price
	 * @param order
	 */
	public void updateBalanceOnOrder() {

		double totalPayments = 0;

		for (Payment payment : this.getLstPayments()) {
			totalPayments += payment.getPayed();
		}

		double newBalance = ((this.getTotalPrice() - totalPayments));
		this.setBalance(newBalance);

		// Tjekker om der er en levering på ordren
		boolean hasDelivery = false;
		for (OrderLine orderLine : this.getLstOrderlines()) {
			if (orderLine.getProduct() instanceof Delivery) {
				hasDelivery = true;
				break;
			}
		}

		this.setDelivery(hasDelivery);
	}



	// Tableview methods

	public double getTotalPrice() {
		return calculateTotalPrice();
	}

	public int getStamps() {
		int result = 0;
		for (OrderLine o : getLstOrderlines()) {
			if (o.getProduct() instanceof Voucher) {
				Voucher v = (Voucher) o.getProduct();
				result += o.getQuantity() * v.getStamp();
			}
		}
		return result;
	}

	public int getUsedStamps() {
		int result = 0;
		for (Payment p : getLstPayments()) {
			if (p.getPaymentMethod() == PaymentMethod.Klippekort) {
				result += Math.ceil((p.getPayed() / 30));
			}
		}
		return result;
	}

	public PriceList getPriceList() {
		return priceList;
	}

	public void setPriceList(PriceList priceList) {
		this.priceList = priceList;
	}

	public boolean getIsFinish() {

		boolean isFinish = false;

		if (balance == 0) {
			isFinish = true;
		}

		return isFinish;
	}

	public String getPriceListTitle() {
		return this.priceList.getTitle();
	}

	public String getDeliveryAsString() {

		return (this.delivery == true ? "Ja" : "Nej");
	}

	public String getFinishAsString() {
		return (this.getIsFinish() == true ? "Ja" : "Nej");
	}

}
