package application.model.evaluators;

public class PriceEvaluator implements Evaluator {

	@Override
	public boolean isValid(String s) {
		try{
			if (!s.matches("[0-9.Ee-]+")){
				return false;
			}
			double tal = Double.parseDouble(s);
			if (tal < 0) {
				return false;
			}
			return true;
		}
		catch(Exception e){
			return false;
		}
	}

}
