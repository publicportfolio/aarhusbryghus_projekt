package application.model.evaluators;

public class MailEvaluator implements Evaluator {

	@Override
	public boolean isValid(String s) {

		if (s.matches("\\b[\\w.%-]+@[-.\\w]+\\.[A-Za-z]{2,4}\\b")) {
			return true;
		}
		return false;
		
	}

}
