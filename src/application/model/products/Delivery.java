package application.model.products;

import application.model.Product;

public class Delivery extends Product {

	public Delivery(String name, String description, int inStock) {
		super(name, description, inStock);
	}

}
