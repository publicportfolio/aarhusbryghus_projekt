package application.model.products;

import java.util.ArrayList;

import application.model.Product;

public class GiftBasket extends Product {

	private ArrayList<Glass> lstGlasses;
	private ArrayList<Beer> lstBeers;

	public GiftBasket(String name, String desciption, int inStock) {
		super(name, desciption,inStock);
		this.lstBeers = new ArrayList<>();
		this.lstGlasses = new ArrayList<>();
	}

	public ArrayList<Glass> getLstGlasses() {
		return lstGlasses;
	}

	public void addGlass(Glass glass) {
		this.lstGlasses.add(glass);
	}

	public void removeGlass(Glass glass) {
		this.lstGlasses.remove(glass);
	}

	public ArrayList<Beer> getLstBeers() {
		return lstBeers;
	}

	public void addBeer(Beer beer) {
		this.lstBeers.add(beer);
	}

	public void removeBeer(Beer beer) {
		this.lstBeers.remove(beer);
	}
}
