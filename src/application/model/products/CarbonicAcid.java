package application.model.products;

import application.model.Product;

public class CarbonicAcid extends Product {

	private double kg;
	

	public CarbonicAcid(String name, String desciption,int inStock, double kg) {
		super(name, desciption,inStock);
		this.kg = kg;
	}

	public double getKg() {
		return kg;
	}

	public void setKg(double kg) {
		this.kg = kg;
	}


	@Override
	public String toString() {
		return super.getName() + ", " + kg + "kg";
	}








}
