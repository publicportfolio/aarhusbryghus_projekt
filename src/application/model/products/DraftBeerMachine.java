package application.model.products;

import application.model.Product;

public class DraftBeerMachine extends Product {
	public DraftBeerMachine(String name, String desciption,int inStock) {
		super(name, desciption,inStock);
	}

	@Override
	public String toString() {
		return super.getName();
	}
}
