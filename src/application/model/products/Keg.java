package application.model.products;

import application.model.Product;

public class Keg extends Product {

	private double liter;

	public Keg(String name, String desciption, int inStock,double liter) {
		super(name, desciption, inStock);
		this.liter = liter;
	}

	public double getLiter() {
		return liter;
	}

	public void setLiter(double liter) {
		this.liter = liter;
	}

	@Override
	public String toString() {
		return super.getName() + " Fustage, " + liter+"L";
	}





}
