package application.model.products;

import application.model.Product;

public class Voucher extends Product {

	private int stamp;


	public Voucher(String name, String desciption, int inStock,int stamp) {
		super(name, desciption, inStock);
		this.stamp = stamp;
	}

	public int getStamp() {
		return stamp;
	}


	public void setStamp(int stamp) {
		this.stamp = stamp;
	}

	@Override
	public String toString() {
		return (super.getName() + " : " + this.stamp + " klip");
	}





}
