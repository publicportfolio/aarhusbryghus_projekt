package application.model;

import java.util.ArrayList;

public class Product {

	private String name;
	private String description;
	private int inStock;
	private ArrayList<OrderLine> lstOrderLines;
	private ArrayList<PriceListLine> lstPriceListLines;

	public Product(String name, String description, int inStock) {
		this.name = name;
		this.description = description;
		this.inStock = inStock;
		this.lstOrderLines = new ArrayList<>();
		this.lstPriceListLines = new ArrayList<>();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getInStock() {
		return inStock;
	}

	public void setInStock(int inStock) {
		this.inStock = inStock;
	}

	public void addAmountToStock(int amount) {
		if (this.inStock + amount < 0) {
			throw new IllegalArgumentException(
					"Kan ikke ændre antallet, da total på lager på " + getName() + " bliver negativt");
		}
		this.inStock += amount;
	}

	public void removeAmountFromStock(int amount) {
		if (this.inStock - amount < 0) {
			throw new IllegalArgumentException("Kan ikke ændre antallet, da total på lager bliver negativt");
		}
		this.inStock -= amount;
	}

	public ArrayList<OrderLine> getLstOrderLines() {
		return lstOrderLines;
	}

	public void setLstOrderLines(ArrayList<OrderLine> lstOrderLines) {
		this.lstOrderLines = lstOrderLines;
	}

	public void addOrderLine(OrderLine orderLine) {
		if (!this.lstOrderLines.contains(orderLine)) {
			this.lstOrderLines.add(orderLine);
		}
	}

	public void removeOrderLines(OrderLine orderLine) {
		if (this.lstOrderLines.contains(orderLine)) {
			this.lstOrderLines.remove(orderLine);
		}
	}

	public ArrayList<PriceListLine> getLstPriceListLines() {
		return lstPriceListLines;
	}

	public void addPriceListLine(PriceListLine priceListLine) {
		if (!this.lstPriceListLines.contains(priceListLine)) {
			this.lstPriceListLines.add(priceListLine);
		}
	}

	public void removePriceListLine(PriceListLine priceListLine) {
		if (this.lstPriceListLines.contains(priceListLine)) {
			this.lstPriceListLines.remove(priceListLine);
		}
	}

	@Override
	public String toString() {
		return name;
	}

	public String getToString() {
		return toString();
	}

	/* TabelViewMethods */

	public int getSold(PriceList pl) {
		int result = 0;
		for (OrderLine o : getLstOrderLines()) {
			if (pl.getTitle() != "Alle lister") {
				if (o.getOrder().getPriceList().equals(pl)) {
					result += o.getQuantity();
				}
			} else {
				result += o.getQuantity();
			}
		}
		return result;
	}
}
