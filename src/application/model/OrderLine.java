package application.model;

import java.time.LocalDate;

public class OrderLine {
	private Product product;
	private int quantity;
	private double totalPrice;
	private TourBooking tourbooking;
	private Reservation reservation;
	private Order order;


	public OrderLine(Product product, int quantity, Order order) {
		this.product = product;
		this.quantity = quantity;
		this.totalPrice = 0;
		this.tourbooking = null;
		this.reservation = null;
		this.order = order;
	}


	public Product getProduct() {
		return product;
	}


	public void setProduct(Product product) {
		this.product = product;
	}


	public int getQuantity() {
		return quantity;
	}


	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}


	public double getTotalPrice() {
		return totalPrice;
	}


	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}


	public TourBooking getTourbooking() {
		return tourbooking;
	}


	public void setTourbooking(TourBooking tourbooking) {
		this.tourbooking = tourbooking;
	}


	public Reservation getReservation() {
		return reservation;
	}


	public void setReservation(Reservation reservation) {
		this.reservation = reservation;
	}


	public Order getOrder() {
		return order;
	}


	public void setOrder(Order order) {
		this.order = order;
	}


	/* Table view Methods */

	public String getProductName() {
		return this.product.toString();
	}

	public double getSinglePrice() {
		return (this.totalPrice/this.quantity);
	}

	public LocalDate getFromDate() {
		return this.reservation.getFromDate();
	}

	public LocalDate getToDate() {
		return this.reservation.getToDate();
	}

	public int getOrderNumber() {
		return this.order.getOrderNumber();
	}


}
