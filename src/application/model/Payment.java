package application.model;

import java.time.LocalDateTime;

public class Payment {
	private PaymentMethod paymentMethod;
	private LocalDateTime date;
	private double payed;

	public Payment(PaymentMethod paymentMethod, LocalDateTime date, double payed) {
		this.paymentMethod = paymentMethod;
		this.date = date;
		this.payed = payed;
	}

	public PaymentMethod getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(PaymentMethod paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public LocalDateTime getDate() {
		return date;
	}

	public void setDate(LocalDateTime date) {
		this.date = date;
	}

	public double getPayed() {
		return payed;
	}

	public void setPayed(double payed) {
		this.payed = payed;
	}

	





}
