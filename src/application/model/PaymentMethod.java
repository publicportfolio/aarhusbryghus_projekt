package application.model;

public enum PaymentMethod {
	Dankort, Mobilepay, Kontant, Klippekort, Regning;

}
