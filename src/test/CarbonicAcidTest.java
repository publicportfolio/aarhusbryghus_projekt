package test;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import application.model.products.CarbonicAcid;

public class CarbonicAcidTest {
	CarbonicAcid object;

	@Before
	public void setUp() throws Exception {
		object = new CarbonicAcid("Kulsyre", "Kulsyre er godt til brus", 10, 6);
	}

	@Test
	public void TC1Constructor() {
		String name = object.getName();
		String description = object.getDescription();
		int inStock = object.getInStock();
		double kg = object.getKg();

		assertEquals("Kulsyre", name);
		assertEquals("Kulsyre er godt til brus", description);
		assertEquals(10, inStock);
		assertEquals(6, kg, 0.0001);
	}

	@Test
	public void toStringTC2() {
		String expected = "Kulsyre, 6.0kg";
		String actual = object.toString();
		assertEquals(expected, actual);
	}

}
