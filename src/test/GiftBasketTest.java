package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import application.model.products.Beer;
import application.model.products.GiftBasket;
import application.model.products.Glass;

public class GiftBasketTest {
	GiftBasket object;
	Beer beer;
	Beer beer2;
	Glass glass;
	Glass glass2;

	@Before
	public void setUp() throws Exception {
		object = new GiftBasket("Gaveaeske, 2 oel, 2 glas", "En dejlig gaveæske", 10);
		beer = new Beer("Klosterbryg", "En dejlig øl", 10, 60, 4.7);
		beer2 = new Beer("Blondie", "En dejlig øl", 10, 60, 4.7);
		glass = new Glass("Øl-glas", "Et glas man kan drikke øl af", 10);
		glass2 = new Glass("WhiskeyGlas", "Et glas man kan drikke whiskey af", 10);
	}

	@Test
	public void TC1Constructor() {
		String name = object.getName();
		String description = object.getDescription();
		int inStock = object.getInStock();
		ArrayList<Beer> beers = object.getLstBeers();
		ArrayList<Glass> glasses = object.getLstGlasses();

		assertEquals("Gaveaeske, 2 oel, 2 glas", name);
		assertEquals("En dejlig gaveæske", description);
		assertEquals(10, inStock);
		assertNotNull(beers);
		assertNotNull(glasses);
	}

	@Test
	public void getLstBeersTC2() {
		ArrayList<Beer> beers = new ArrayList<>();
		beers.add(beer);
		beers.add(beer2);
		object.addBeer(beer);
		object.addBeer(beer2);
		assertEquals(beers, object.getLstBeers());
	}

	@Test
	public void getLstGlassesTC3() {
		ArrayList<Glass> glasses = new ArrayList<>();
		glasses.add(glass);
		glasses.add(glass2);
		object.addGlass(glass);
		object.addGlass(glass2);
		assertEquals(glasses, object.getLstGlasses());
	}

	@Test
	public void getLstGlassesTC4() {
		ArrayList<Glass> glasses = new ArrayList<>();
		glasses.add(glass);
		glasses.add(glass2);
		object.addGlass(glass);
		object.addGlass(glass);
		object.addGlass(glass2);
		object.addGlass(glass2);
		object.removeGlass(glass);
		object.removeGlass(glass2);
		assertEquals(glasses, object.getLstGlasses());
	}

	@Test
	public void getLstBeersTC5() {
		ArrayList<Beer> beers = new ArrayList<>();
		beers.add(beer);
		beers.add(beer2);
		object.addBeer(beer);
		object.addBeer(beer2);
		object.addBeer(beer);
		object.addBeer(beer2);
		object.removeBeer(beer);
		object.removeBeer(beer2);
		assertEquals(beers, object.getLstBeers());
	}

}
