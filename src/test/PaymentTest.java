package test;

import static org.junit.Assert.assertEquals;

import java.time.LocalDateTime;

import org.junit.Before;
import org.junit.Test;

import application.model.Payment;
import application.model.PaymentMethod;

public class PaymentTest {
	private Payment object;

	@Before
	public void setUp() throws Exception {
		object = new Payment(PaymentMethod.Dankort, LocalDateTime.of(2001, 10, 10, 10, 10), 20);
	}

	@Test
	public void TC1Constructor() {
		assertEquals(PaymentMethod.Dankort, object.getPaymentMethod());
		assertEquals(LocalDateTime.of(2001, 10, 10, 10, 10), object.getDate());
		assertEquals(20, object.getPayed(), 0.1);
	}

	@Test
	public void getPaymentMethodTC2() {
		object.setPaymentMethod(PaymentMethod.Mobilepay);
		assertEquals(PaymentMethod.Mobilepay, object.getPaymentMethod());

	}

}
