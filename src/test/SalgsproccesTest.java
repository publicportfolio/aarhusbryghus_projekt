package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

import application.model.Corporate;
import application.model.Order;
import application.model.OrderLine;
import application.model.Payment;
import application.model.PaymentMethod;
import application.model.PriceList;
import application.model.PriceListLine;
import application.model.Reservation;
import application.model.TourBooking;
import application.service.Service;

public class SalgsproccesTest {
	PriceList butik;
	Service service;
	PriceListLine pllCelebrationFlaske;
	PriceListLine pllFadølsanlæg1Hane;
	PriceListLine pllTour;
	PriceListLine pllDelivery;

	@Before
	public void setUp() throws Exception {
		Service.shutDownService();
		service = Service.getService();
		service.initContent();
		butik = service.getPriceLists().get(1);
		pllCelebrationFlaske = butik.getLstPriceListLines().get(4);
		pllFadølsanlæg1Hane = butik.getLstPriceListLines().get(27);
		pllTour = butik.getLstPriceListLines().get(38);
		pllDelivery = butik.getLstPriceListLines().get(39);
	}

	@Test
	public void TCSalgsprocess() {
		//		-------------- Test på ordre-oprettelse ----------------
		Order order = service.createOrder(LocalDateTime.of(2017, 10, 10, 10, 10), butik);
		assertEquals(butik,order.getPriceList());
		assertEquals("Butik",order.getPriceList().getTitle());
		assertEquals(LocalDateTime.of(2017, 10, 10, 10, 10),order.getDate());




		//		-------------- Tjekker om der er nok på lager af ønsket produkt ----------------
		assertTrue(service.checkIfStockIsAvailable(pllCelebrationFlaske.getProduct(), 2));

		//		-------------- Tjekker om produkt skal have en reservation ----------------
		assertFalse(service.checkIfProductNeedsReservation(pllCelebrationFlaske.getProduct()));

		//		-------------- Test på associering mellem order og orderlineBeer og indholdet ----------------
		OrderLine orderLineBeer = order.createOrderLine(pllCelebrationFlaske.getProduct(), 2);

		assertTrue(order.getLstOrderlines().contains(orderLineBeer));
		assertEquals(order, orderLineBeer.getOrder());
		assertEquals(2,orderLineBeer.getQuantity());

		//		-------------- Test på associering mellem produkt og orderlineBeer ----------------
		assertTrue(pllCelebrationFlaske.getProduct().getLstOrderLines().contains(orderLineBeer));
		assertEquals(pllCelebrationFlaske.getProduct(),orderLineBeer.getProduct());

		//		-------------- Opdaterer prisen på orderLineBeer ----------------

		service.setNormalPrice(butik, orderLineBeer);

		assertEquals(72, orderLineBeer.getTotalPrice(),0.0001);
		//		-------------- Tjekker samlet beløb på balance----------------
		assertEquals(72, order.getBalance(),0.0001);




		//		-------------- Tjekker om der er nok på lager af ønsket produkt ----------------
		assertTrue(service.checkIfStockIsAvailable(pllFadølsanlæg1Hane.getProduct(), 1));

		//		-------------- Tjekker om produkt skal have en reservation ----------------
		assertTrue(service.checkIfProductNeedsReservation(pllFadølsanlæg1Hane.getProduct()));

		//		-------------- Laver en ny orderline med fadølsanlæg på order o tester associering ----------------
		OrderLine orderLineAnlæg = order.createOrderLine(pllFadølsanlæg1Hane.getProduct(), 1);
		assertTrue(order.getLstOrderlines().contains(orderLineAnlæg));
		assertEquals(order, orderLineAnlæg.getOrder());

		//		-------------- Laver en ny kunde til ordren og tester associeringerne og indholdet----------------
		Corporate kunde = service.createCorporateCustomer("Simon Kempel", "Havvej 82", 8000, 33333333, "test@test.dk",1011121314,"Lars Brygger");
		service.addCustomerToOrder(order, kunde);
		assertEquals(kunde,order.getCustomer());
		assertTrue(kunde.getLstOrder().contains(order));
		assertEquals("Simon Kempel",kunde.getName());
		assertEquals("Havvej 82",kunde.getAddress());
		assertEquals(8000,kunde.getZipCode());
		assertEquals(33333333,kunde.getPhone());
		assertEquals("test@test.dk",kunde.getMail());
		assertEquals(1011121314,kunde.getCvr());
		assertEquals("Lars Brygger",kunde.getContactPerson());

		//		-------------- Laver en ny reservering til orderLineAnlæg og tester associeringerne/indholdet----------------
		Reservation reservation = service.createReservation(LocalDate.of(2017, 10, 10), LocalDate.of(2017, 10, 20), orderLineAnlæg);
		assertEquals(orderLineAnlæg,reservation.getOrderLine());
		assertEquals(reservation, orderLineAnlæg.getReservation());
		assertEquals(LocalDate.of(2017, 10, 10),reservation.getFromDate());
		assertEquals(LocalDate.of(2017, 10, 20),reservation.getToDate());

		//		-------------- Opdaterer prisen på orderLineAnlæg ----------------
		service.setNormalPrice(butik, orderLineAnlæg);
		assertEquals(250, orderLineAnlæg.getTotalPrice(),0.0001);

		//		-------------- Tjekker samlet beløb på order ----------------
		assertEquals(322, order.getBalance(),0.0001);


		//		-------------- Tilføjer levering til ordren og tester associeringer/indhold----------------
		OrderLine orderLineLevering = order.createOrderLine(pllDelivery.getProduct(), 1);
		assertTrue(order.getLstOrderlines().contains(orderLineLevering));
		assertEquals(order, orderLineLevering.getOrder());

		//		-------------- Opdaterer prisen på orderLineLevering ----------------
		service.setNormalPrice(butik, orderLineLevering);
		assertEquals(500, orderLineLevering.getTotalPrice(),0.0001);

		//		-------------- Tjekker samlet beløb på order ----------------
		assertEquals(822, order.getBalance(),0.0001);

		//		-------------- Tjekker om ordren er blevet sat til levering ----------------
		assertTrue(order.isDelivery());



		//		-------------- Tjekker om der er nok på lager af ønsket produkt ----------------
		assertTrue(service.checkIfStockIsAvailable(pllTour.getProduct(), 2));

		//		-------------- Tjekker om produkt skal have en reservation ----------------
		assertFalse(service.checkIfProductNeedsReservation(pllTour.getProduct()));

		//		-------------- Laver en ny orderline med tour på order og tester associeringerne----------------
		OrderLine orderLineTour = order.createOrderLine(pllTour.getProduct(), 2);
		assertTrue(order.getLstOrderlines().contains(orderLineTour));
		assertEquals(order, orderLineTour.getOrder());

		//		-------------- Laver en ny tourbooking til orderLineTour og tester associeringerne/indholdet----------------
		TourBooking tourbooking = service.createTourBooking(LocalDateTime.of(2017, 10, 15, 13, 00), orderLineTour);
		assertEquals(tourbooking,orderLineTour.getTourbooking());
		assertEquals(orderLineTour,tourbooking.getOrderLine());
		assertEquals(LocalTime.of(13, 00), tourbooking.getTime());

		//		-------------- Opdaterer prisen på orderLineTour ----------------
		service.setNormalPrice(butik, orderLineTour);
		assertEquals(200, orderLineTour.getTotalPrice(),0.0001);

		//		-------------- Tjekker samlet beløb på order ----------------
		assertEquals(1022, order.getBalance(),0.0001);




		//		-------------- Laver en betaling på ordren og tjekker på associeringerne og indholdet ----------------
		Payment betaling = service.createPayment(order,PaymentMethod.Dankort, LocalDateTime.of(2017, 10, 10, 10, 30), 1022);
		assertTrue(order.getLstPayments().contains(betaling));
		assertEquals(PaymentMethod.Dankort,betaling.getPaymentMethod());
		assertEquals(LocalDateTime.of(2017, 10, 10, 10, 30),betaling.getDate());
		assertEquals(1022,betaling.getPayed(),0.0001);




		//		-------------- Tjekker om order er afsluttet og betalt ----------------
		assertTrue(order.getIsFinish());

	}

}
