package test;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import application.model.PriceListLine;
import application.model.Product;

public class PriceListLineTest {

	private PriceListLine object;
	private Product product;

	@Before
	public void setUp() throws Exception {
		product = new Product("", "", 10);
		object = new PriceListLine(20, product);
	}

	@Test
	public void TC1Contructor() {
		assertEquals(product, object.getProduct());
		assertEquals(20.0, object.getPrice(), 0.1);
	}

	@Test
	public void setProductTC2() {
		Product p1 = new Product("hej", "", 20);
		object.setProduct(p1);

		assertEquals(p1, object.getProduct());
		assertEquals("hej", object.getProductName());
	}

	@Test
	public void setProductNameTC3() {
		Product p1 = new Product("hej", "", 20);
		object.setProduct(p1);

		assertEquals(20.0, object.getInStock(), 0.1);
		assertEquals("hej", object.getProductName());
	}

}
