package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import application.model.Corporate;
import application.model.Customer;
import application.model.Order;
import application.model.OrderLine;
import application.model.Payment;
import application.model.PaymentMethod;
import application.model.PriceList;
import application.model.PriceListLine;
import application.model.Private;
import application.model.Product;
import application.model.Reservation;
import application.model.TourBooking;
import application.model.products.Beer;
import application.model.products.CarbonicAcid;
import application.model.products.Delivery;
import application.model.products.DraftBeerMachine;
import application.model.products.GiftBasket;
import application.model.products.Glass;
import application.model.products.Keg;
import application.model.products.Tour;
import application.model.products.Voucher;
import application.service.Service;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class ServiceTest {

	private Service service;
	private PriceList butik;

	@Before
	public void setUp() throws Exception {
		Service.shutDownService();
		service = Service.getService();

		butik = new PriceList("Butik");
	}


	@Test
	public void TC1Constructor() {
		assertEquals(Service.getService(),service);
	}


	@Test
	public void createPrivateCustomerTC2() {

		Private p = service.createPrivateCustomer("Bo", "skovvej 22", 8000, 98989898, "bo@test.dk");

		String name = p.getName();
		String address = p.getAddress();
		int zipcode = p.getZipCode();
		int phone = p.getPhone();
		String mail = p.getMail();

		boolean exists = service.getCustomers().contains(p);

		assertEquals("Bo",name);
		assertEquals("skovvej 22",address);
		assertEquals(8000,zipcode);
		assertEquals(98989898,phone);
		assertEquals("bo@test.dk",mail);
		assertEquals(true, exists);
	}

	@Test
	public void createCorporateCustomerTC3() {

		Corporate c = service.createCorporateCustomer("Byens murer", "skovvej 22", 8000, 70707070, "bo@test.dk",1234567890,"Bo hansen");

		String name = c.getName();
		String address = c.getAddress();
		int zipcode = c.getZipCode();
		int phone = c.getPhone();
		String mail = c.getMail();
		int cvr = c.getCvr();
		String contact = c.getContactPerson();

		boolean exists = service.getCustomers().contains(c);

		assertEquals("Byens murer",name);
		assertEquals("skovvej 22",address);
		assertEquals(8000,zipcode);
		assertEquals(70707070,phone);
		assertEquals("bo@test.dk",mail);
		assertEquals(1234567890,cvr);
		assertEquals("Bo hansen",contact);
		assertEquals(true, exists);
	}


	@Test
	public void createOrderTC4() {

		Order o = service.createOrder(LocalDateTime.of(2017,12,12,13,00),butik);

		LocalDateTime expectedDate = o.getDate();

		boolean exists = service.getOrders().contains(o);

		assertEquals(true, exists);
		assertEquals(LocalDateTime.of(2017,12,12,13,00), expectedDate);
	}


	@Test
	public void createPaymentTC5() {
		Order o = service.createOrder(LocalDateTime.now(), butik);
		o.setBalance(100);

		try {
			service.createPayment(o,PaymentMethod.Dankort, LocalDateTime.now(), 200);
			fail();
		} catch (RuntimeException e) {
			assertEquals("Betalingen er for stor",e.getMessage());
		}
	}

	@Test
	public void createPaymentTC6() {
		Order o = service.createOrder(LocalDateTime.now(), butik);
		o.setBalance(100);
		Payment payment = service.createPayment(o,PaymentMethod.Dankort, LocalDateTime.of(2017,12,12,13,00), 100);

		PaymentMethod method = payment.getPaymentMethod();
		LocalDateTime date = payment.getDate();
		double payed = payment.getPayed();

		assertEquals(PaymentMethod.Dankort, method);
		assertEquals(LocalDateTime.of(2017,12,12,13,00), date);
		assertEquals(100, payed,0.1);
	}

	@Test
	public void createTourBookingTC7() {
		Tour tour = new Tour("rundvisning","",10);
		Order o = service.createOrder(LocalDateTime.of(2017,12,12,13,00),butik);
		OrderLine ol = o.createOrderLine(tour, 10);

		TourBooking tourBooking = service.createTourBooking(LocalDateTime.of(2017,12,12,12,00), ol);

		LocalDateTime date = tourBooking.getDate();
		boolean exists = ol.getTourbooking().equals(tourBooking);
		boolean exists1 = service.getTourBookings().contains(tourBooking);

		assertEquals(LocalDateTime.of(2017,12,12,12,00),date);
		assertEquals(true,exists);
		assertEquals(true,exists1);
	}


	@Test
	public void createReservationTC8() {
		DraftBeerMachine product = new DraftBeerMachine("fad","Plastik krus til en god pris.",10);
		Order o = service.createOrder(LocalDateTime.of(2017,12,12,13,00),butik);
		OrderLine ol = o.createOrderLine(product, 10);

		Reservation res = service.createReservation(LocalDate.of(2017,12,12),LocalDate.of(2017,12,13), ol);

		LocalDate fromDate = res.getFromDate();
		LocalDate toDate = res.getToDate();
		boolean exists = ol.getReservation().equals(res);

		assertEquals(LocalDate.of(2017,12,12),fromDate);
		assertEquals(LocalDate.of(2017,12,13),toDate);
		assertEquals(true,exists);
	}

	@Test
	public void createPriceListTC10() {
		PriceList fredagsbar = service.createPriceList("fredagsbar");
		String title = fredagsbar.getTitle();

		boolean exists = service.getPriceLists().contains(fredagsbar);

		assertEquals("fredagsbar",title);
		assertEquals(true,exists);
	}

	@Test
	public void createProductTC11() {
		Product p = service.createProduct("t-shirt", "Large", 10);

		String name = p.getName();
		String description = p.getDescription();
		int stock = p.getInStock();

		boolean exists = service.getProducts().contains(p);

		assertEquals("t-shirt",name);
		assertEquals("Large",description);
		assertEquals(10,stock);
		assertEquals(true,exists);
	}
	@Test
	public void createVoucherTC12() {
		Voucher v = service.createVoucher("Klippekort", "4 klip", 10,100);

		String name = v.getName();
		String description = v.getDescription();
		int stock = v.getInStock();
		int stamp = v.getStamp();

		boolean exists = service.getProducts().contains(v);

		assertEquals("Klippekort",name);
		assertEquals("4 klip",description);
		assertEquals(10,stock);
		assertEquals(100,stamp);
		assertEquals(true,exists);
	}

	@Test
	public void createGlassTC13() {
		Glass g = service.createGlass("Stort glas", "Meget stort glas", 15);

		String name = g.getName();
		String description = g.getDescription();
		int stock = g.getInStock();


		boolean exists = service.getProducts().contains(g);

		assertEquals("Stort glas",name);
		assertEquals("Meget stort glas",description);
		assertEquals(15,stock);
		assertEquals(true,exists);
	}

	@Test
	public void createBeerTC14() {
		Beer b = service.createBeer("Klosterbryg", "god øl", 15,60,4.6);

		String name = b.getName();
		String description = b.getDescription();
		int stock = b.getInStock();
		double cl = b.getCl();
		double alc = b.getAlcohol();

		boolean exists = service.getProducts().contains(b);

		assertEquals("Klosterbryg",name);
		assertEquals("god øl",description);
		assertEquals(15,stock);
		assertEquals(60,cl,0.1);
		assertEquals(4.6,alc,0.1);
		assertEquals(true,exists);
	}

	@Test
	public void createKegTC15() {
		Keg b = service.createKeg("Klosterbryg fustage", "20l fustage", 15,20);

		String name = b.getName();
		String description = b.getDescription();
		int stock = b.getInStock();
		double liter = b.getLiter();


		boolean exists = service.getProducts().contains(b);

		assertEquals("Klosterbryg fustage",name);
		assertEquals("20l fustage",description);
		assertEquals(15,stock);
		assertEquals(20,liter,0.1);
		assertEquals(true,exists);
	}


	@Test
	public void createCarbonicAcidTC16() {
		CarbonicAcid ca = service.createCarbonicAcid("Kulsyre", "4kg", 15,4);

		String name = ca.getName();
		String description = ca.getDescription();
		int stock = ca.getInStock();
		double kg = ca.getKg();


		boolean exists = service.getProducts().contains(ca);

		assertEquals("Kulsyre",name);
		assertEquals("4kg",description);
		assertEquals(15,stock);
		assertEquals(4,kg,0.1);
		assertEquals(true,exists);
	}


	@Test
	public void createDraftBeerMachineTC17() {
		DraftBeerMachine dbm = service.createDraftBeerMachine("fadølsanlæg", "2 haner", 15);

		String name = dbm.getName();
		String description = dbm.getDescription();
		int stock = dbm.getInStock();


		boolean exists = service.getProducts().contains(dbm);

		assertEquals("fadølsanlæg",name);
		assertEquals("2 haner",description);
		assertEquals(15,stock);
		assertEquals(true,exists);
	}

	@Test
	public void createGiftBasketTC18() {
		GiftBasket gb = service.createGiftBasket("Gavekurv", "2 øl og 2 glas", 15);

		String name = gb.getName();
		String description = gb.getDescription();
		int stock = gb.getInStock();

		boolean exists = service.getProducts().contains(gb);

		assertEquals("Gavekurv",name);
		assertEquals("2 øl og 2 glas",description);
		assertEquals(15,stock);
		assertEquals(true,exists);
	}


	@Test
	public void createTourTC19() {
		Tour tour = service.createTour("Rundvisning", "julerundvisning", 25);

		String name = tour.getName();
		String description = tour.getDescription();
		int stock = tour.getInStock();

		boolean exists = service.getProducts().contains(tour);

		assertEquals("Rundvisning",name);
		assertEquals("julerundvisning",description);
		assertEquals(25,stock);
		assertEquals(true,exists);
	}


	@Test
	public void addCustomerToOrderTC20() {
		Private p = new Private("Bo","Svanevej 44",2323,98989898,"bo@test.dk");
		Order o = new Order(LocalDateTime.now(), butik);

		service.addCustomerToOrder(o, p);

		boolean exists = o.getCustomer().equals(p);
		boolean exists2 = p.getLstOrder().contains(o);

		assertEquals(true,exists);
		assertEquals(true,exists2);
	}

	@Test
	public void addBeerToGiftBasketTC21() {
		Beer b = new Beer("Klosterbryg", "god", 2, 40, 4.6);
		GiftBasket g = new GiftBasket("Gavekurv", "2 øl", 1);

		service.addBeerToGiftBasket(g, b);

		int stockBeer = b.getInStock();
		boolean exists = g.getLstBeers().contains(b);

		assertEquals(true,exists);
		assertEquals(1,stockBeer);
	}

	@Test
	public void addGlassToGiftBasketTC22() {
		Glass glas = new Glass("Glas", "god", 2);
		GiftBasket g = new GiftBasket("Gavekurv", "2 glas", 1);

		service.addGlassToGiftBasket(g,glas);
		service.addGlassToGiftBasket(g,glas);

		int glassStock = glas.getInStock();
		boolean exists = g.getLstGlasses().contains(glas);

		assertEquals(true,exists);
		assertEquals(0,glassStock);
	}

	@Test
	public void addProductToStockTC23() {
		Glass glas = new Glass("Glas", "god", 2);

		service.addProductToStock(glas, 3);

		assertEquals(5,glas.getInStock());
	}

	@Test
	public void setNormalPriceTC24() {
		Glass glas = new Glass("Glas", "god", 4);

		butik.createPriceListLine(20, glas);
		Order o = new Order(LocalDateTime.now(), butik);
		OrderLine ol = o.createOrderLine(glas, 2);

		service.setNormalPrice(butik, ol);

		assertEquals(40,ol.getTotalPrice(),0.1);
	}

	@Test
	public void updateStockTC25() {
		Glass glas = new Glass("Glas", "god", 10);
		butik.createPriceListLine(20, glas);
		Order o = new Order(LocalDateTime.now(), butik);
		o.createOrderLine(glas, 2);

		service.updateStock(o);

		assertEquals(8,glas.getInStock());
	}


	@Test
	public void updateBalanceOnOrderTC26() {
		Order o = new Order(LocalDateTime.now(), butik);

		o.setBalance(60);

		Payment p = service.createPayment(o,PaymentMethod.Dankort, LocalDateTime.now(), 10);

		boolean exists = o.getLstPayments().contains(p);

		assertEquals(true,exists);
	}

	@Test
	public void updatePrivateCustomerTC27() {

		Private p = new Private("Bo", "svanevej 33", 2222, 78787878, "bo@test.dk");

		service.updatePrivateCustomer(p, "Bo Hansen", "Svanvej 33", 2222, "bo@bomail.dk", 23232323);

		String name = p.getName();
		String address = p.getAddress();
		int zip = p.getZipCode();
		String mail = p.getMail();
		int phone = p.getPhone();

		assertEquals("Bo Hansen",name);
		assertEquals("Svanvej 33",address);
		assertEquals(2222,zip);
		assertEquals("bo@bomail.dk",mail);
		assertEquals(23232323,phone);

	}

	@Test
	public void updateCorporateCustomerTC28() {

		Corporate c = new Corporate("Bo", "svanevej 33", 2222, 78787878, "bo@test.dk",1234567890,"Bo Nielsen");

		service.updateCorporateCustomer(c, "Bo Hansen", "Svanvej 33", 2222, "bo@bomail.dk", 23232323,12345,"Nielsen");

		String name = c.getName();
		String address = c.getAddress();
		int zip = c.getZipCode();
		String mail = c.getMail();
		int phone = c.getPhone();
		int cvr = c.getCvr();
		String contact = c.getContactPerson();


		assertEquals("Bo Hansen",name);
		assertEquals("Svanvej 33",address);
		assertEquals(2222,zip);
		assertEquals("bo@bomail.dk",mail);
		assertEquals(23232323,phone);
		assertEquals(12345,cvr);
		assertEquals("Nielsen",contact);
	}

	@Test
	public void updateBeerTC29() {

		Beer b = new Beer("Klosterbryg", "god øl", 3, 60, 4.6);

		service.updateBeer(b, "Klosterbryg 2.0", "meget god øl", 5, 40, 5.2);

		String name = b.getName();
		String description = b.getDescription();
		int inStock = b.getInStock();
		double cl = b.getCl();
		double alc = b.getAlcohol();


		assertEquals("Klosterbryg 2.0",name);
		assertEquals("meget god øl",description);
		assertEquals(5,inStock);
		assertEquals(40,cl,0.1);
		assertEquals(5.2,alc,0.1);
	}

	@Test
	public void updateVoucherTC30() {

		Voucher b = new Voucher("klippekort", "4 klip", 10, 4);

		service.updateVoucher(b, "klippekort", "5 klip", 5, 5);

		String name = b.getName();
		String description = b.getDescription();
		int inStock = b.getInStock();
		int stamp = b.getStamp();

		assertEquals("klippekort",name);
		assertEquals("5 klip",description);
		assertEquals(5,inStock);
		assertEquals(5,stamp);
	}

	@Test
	public void updateProductTC31() {

		Product b = new Product("t-shirt", "Large", 10);

		service.updateProduct(b, "rød t-shirt", "Large", 5);

		String name = b.getName();
		String description = b.getDescription();
		int inStock = b.getInStock();

		assertEquals("rød t-shirt",name);
		assertEquals("Large",description);
		assertEquals(5,inStock);
	}

	@Test
	public void updateKegTC32() {

		Keg b = new Keg("Klosterbryg", "fustage", 10,20);

		service.updateKeg(b, "Klosterbryg 2.0", "fustage", 5,40);

		String name = b.getName();
		String description = b.getDescription();
		int inStock = b.getInStock();
		double liter = b.getLiter();

		assertEquals("Klosterbryg 2.0",name);
		assertEquals("fustage",description);
		assertEquals(5,inStock);
		assertEquals(40,liter,0.1);
	}


	@Test
	public void updateCarbonicAcidTC33() {

		CarbonicAcid b = new CarbonicAcid("Kulsyre", "20kg", 10,20);

		service.updateCarbonicAcid(b, "Kulsyre", "4kg", 5,4);

		String name = b.getName();
		String description = b.getDescription();
		int inStock = b.getInStock();
		double kg = b.getKg();

		assertEquals("Kulsyre",name);
		assertEquals("4kg",description);
		assertEquals(5,inStock);
		assertEquals(4,kg,0.1);
	}


	@Test
	public void updateDraftBeerMachineTC34() {

		DraftBeerMachine b = new DraftBeerMachine("fadølsanlæg", "2-haner", 10);

		service.updateDraftBeerMachine(b, "fadølsanlæg", "4-haner", 5);

		String name = b.getName();
		String description = b.getDescription();
		int inStock = b.getInStock();

		assertEquals("fadølsanlæg",name);
		assertEquals("4-haner",description);
		assertEquals(5,inStock);
	}

	@Test
	public void updatePriceListTC35() {

		service.updatePriceList(butik,"ny butiks liste");

		String title = butik.getTitle();

		assertEquals("ny butiks liste",title);
	}


	@Test
	public void orderIsClosedForPaymentTC37() {
		Product p = service.createProduct("T-shirt", "Large", 10);
		Order o = service.createOrder(LocalDateTime.now(),butik);
		butik.createPriceListLine(20,p);
		OrderLine ol = o.createOrderLine(p, 2);
		service.setNormalPrice(butik, ol);

		boolean isClosedBefore = service.orderIsClosedForPayment(o);

		service.createPayment(o,PaymentMethod.Dankort, LocalDateTime.now(), 20);

		boolean isClosedMiddle = service.orderIsClosedForPayment(o);

		service.createPayment(o,PaymentMethod.Dankort, LocalDateTime.now(), 20);

		boolean isClosedAfter = service.orderIsClosedForPayment(o);

		assertEquals(false,isClosedBefore);
		assertEquals(false,isClosedMiddle);
		assertEquals(true,isClosedAfter);
	}



	@Test
	public void showOrdersByDateTC37() {
		Order o = service.createOrder(LocalDateTime.of(2017, 10, 10, 13, 00),butik);
		Order o2 = service.createOrder(LocalDateTime.of(2017, 10, 11, 13, 00),butik);
		Order o3 = service.createOrder(LocalDateTime.of(2017, 10, 12, 13, 00),butik);

		ArrayList<Order> orders = new ArrayList<>();

		orders.add(o);
		orders.add(o2);
		orders.add(o3);

		assertEquals(orders,service.showOrdersByDate(LocalDate.of(2017, 10, 10), LocalDate.of(2017, 10, 12)));
	}


	@Test
	public void searchOrderByOrderNumberTC38() {
		PriceList pl = service.createPriceList("");
		Order o = service.createOrder(LocalDateTime.now(), pl);

		assertEquals(o, service.searchOrderByOrderNumber(o.getOrderNumber(), pl));
		assertNull(service.searchOrderByOrderNumber(50, pl));
	}
	@Test
	public void getPriceListLinesByFilterTC39() {
		PriceList pl = service.createPriceList("test butik");
		Order o = service.createOrder(LocalDateTime.of(2017, 10, 10, 13, 00),pl);
		Product p1 = service.createProduct("test1", "", 20);
		Product p2 = service.createProduct("test2", "", 20);
		Product p3 = service.createProduct("test3", "", 20);
	
		PriceListLine pll1 = pl.createPriceListLine(20, p1);
		PriceListLine pll2 = pl.createPriceListLine(20, p2);
		PriceListLine pll3 = pl.createPriceListLine(20, p3);
		o.createOrderLine(pll1.getProduct(), 2);

		ArrayList<PriceListLine> tester = new ArrayList<>();
		tester.add(pll1);
		assertEquals(0, service.getPriceListLinesByFilter("test1", o).size());
		tester.clear();

		tester.add(pll2);
		assertEquals(tester.get(0), service.getPriceListLinesByFilter("tEsT2", o).get(0));
		tester.clear();

		tester.add(pll3);
		assertEquals(tester.get(0), service.getPriceListLinesByFilter("tEsT3", o).get(0));
		tester.clear();

		tester.add(pll1);
		assertEquals(0, service.getPriceListLinesByFilter("sdgfasdgasdg", o).size());
		tester.clear();
	}

	@Test
	public void searchCustomerByPhoneTC40() {
		Customer c = service.createCorporateCustomer("", "", 0, 88888888, "", 12, "");

		assertEquals(null, service.searchCustomerByPhone(88888887));
		assertEquals(c, service.searchCustomerByPhone(88888888));

	}

	@Test
	public void checkIfProductNeedsReservationTC41() {
		Product p = service.createProduct("", "", 20);
		Keg k = service.createKeg("", "", 20, 20);
		CarbonicAcid ca = service.createCarbonicAcid("", "", 20, 20);
		DraftBeerMachine dbm = service.createDraftBeerMachine("", "", 20);

		assertEquals(false, service.checkIfProductNeedsReservation(p));
		assertEquals(true, service.checkIfProductNeedsReservation(k));
		assertEquals(true, service.checkIfProductNeedsReservation(ca));
		assertEquals(true, service.checkIfProductNeedsReservation(dbm));

	}


	@Test
	public void checkIfStockIsAvailableTC42() {
		Product p = service.createProduct("", "", 20);

		assertEquals(true, service.checkIfStockIsAvailable(p, 20));
		assertEquals(true, service.checkIfStockIsAvailable(p, 19));
		assertEquals(false, service.checkIfStockIsAvailable(p, 21));

	}

	@Test
	public void updateOrderLineTC43() {
		PriceList pl = service.createPriceList("");
		Order o = service.createOrder(LocalDateTime.now(), pl);
		Product p = service.createProduct("", "", 0);
		OrderLine ol = o.createOrderLine(p, 20);
		Reservation r = service.createReservation(LocalDate.now(), LocalDate.now(), ol);
		TourBooking t = service.createTourBooking(LocalDateTime.now(), ol);

		service.updateOrderLine(ol, 40, 40, r, LocalDate.ofYearDay(30, 30), LocalDate.ofYearDay(31, 31), t,
				LocalDateTime.of(32, 12, 30, 10, 10));
		assertEquals(40, ol.getQuantity());
		assertEquals(40, ol.getTotalPrice(), 0.1);
		assertNotNull(ol.getReservation());
		assertEquals(LocalDate.ofYearDay(30, 30), ol.getFromDate());
		assertEquals(LocalDate.ofYearDay(31, 31), ol.getToDate());
		assertNotNull(ol.getTourbooking());
		// assertEquals(LocalDateTime.of(32, 12, 30, 10, 10),
		// ol.getTourbooking().getDate());
	}


	@Test
	public void updateOrderLineTotalTC44() {
		PriceList pl = service.createPriceList("");
		Order o = service.createOrder(LocalDateTime.now(), pl);
		Product p = service.createProduct("", "", 0);

		OrderLine ol = o.createOrderLine(p, 20);

		service.updateOrderLineTotal(20, ol);

		assertEquals(20, ol.getTotalPrice(), 0.1);
	}

	@Test
	public void getAllPrceListsAsPriceListTC45() {
		PriceList pl = service.createPriceList("");
		PriceList pl2 = service.createPriceList("");
		Product p = service.createProduct("", "", 20);
		PriceListLine pll1 = pl.createPriceListLine(20, p);
		pl2.addPriceListLine(pll1);
		PriceListLine pll2 = pl2.createPriceListLine(20, p);

		ArrayList<PriceListLine> tester = new ArrayList<>();
		tester.add(pll1);
		tester.add(pll2);

		service.getAllPriceListsAsPriceList();

		for (int i = 0; i < service.getAllPriceListsAsPriceList().getLstPriceListLines().size(); i++) {
			assertEquals(tester.get(i), service.getAllPriceListsAsPriceList().getLstPriceListLines().get(i));
		}
	}

	@Test
	public void removeProductTC46() {
		Product p = service.createProduct("", "", 20);

		service.removeProduct(null);

		service.removeProduct(p);

		assertEquals(0, service.getProducts().size());
		assertEquals(0, service.getProducts().size());
	}

	@Test
	public void removePaymentTC47() {
		PriceList pl = service.createPriceList("");
		Product p = service.createProduct("", "", 20);
		PriceListLine pll = pl.createPriceListLine(20, p);
		Order o = service.createOrder(LocalDateTime.now(), pl);
		OrderLine ol = o.createOrderLine(p, 10);
		ol.getProduct().addPriceListLine(pll);
		ol.setTotalPrice(200);
		o.setBalance(200);
		Order o2 = service.createOrder(LocalDateTime.now(), pl);

		Payment pm = service.createPayment(o, PaymentMethod.Dankort, LocalDateTime.now(), 20);
		
		service.removePayment(o2, pm);
		assertEquals(0, o2.getLstOrderlines().size());

		service.removePayment(o, pm);
		assertEquals(0, o.getLstPayments().size());
		assertEquals(200, o.getBalance(), 0.1);
	}

	@Test
	public void removePriceListLineTC48() {
		PriceList pl = service.createPriceList("");
		Product p = service.createProduct("", "", 0);
		PriceListLine pll = pl.createPriceListLine(20, p);

		service.removePriceListLineFromPriceList(pl, pll);

		assertEquals(0, pl.getLstPriceListLines().size());
		assertEquals(0, p.getLstPriceListLines().size());
	}

	@Test
	public void removegiftBasketTC49() {
		GiftBasket gb = service.createGiftBasket("", "", 20);

		service.removeGiftBasket(gb);

		assertEquals(0, service.getAllGiftBaskets().size());
		assertEquals(0, service.getProducts().size());
	}

	@Test
	public void removePriceListTC50() {
		PriceList pl = service.createPriceList("");
		Product p = service.createProduct("", "", 0);
		pl.createPriceListLine(20, p);

		service.removePriceList(pl);

		assertEquals(0, service.getPriceLists().size());
		assertEquals(0, p.getLstPriceListLines().size());
	}


	@Test
	public void removeOrderLineTC51() {
		PriceList pl = service.createPriceList("");
		Order o = service.createOrder(LocalDateTime.now(), pl);
		Product p = service.createProduct("", "", 0);
		OrderLine ol = o.createOrderLine(p, 3);

		service.removeOrderLine(o, ol);

		assertEquals(0, o.getLstOrderlines().size());
		assertEquals(0, p.getLstOrderLines().size());

		OrderLine ol1 = o.createOrderLine(p, 3);

		service.createReservation(LocalDate.now(), LocalDate.now(), ol1);
		service.createTourBooking(LocalDateTime.now(), ol1);

		service.removeOrderLine(o, ol1);

		assertEquals(0, o.getLstOrderlines().size());
		assertEquals(0, p.getLstOrderLines().size());
	}


	@Test
	public void getAllMethodsForProductsTC52() {
		service.createProduct("", "", 10);
		Product p2 = service.createBeer("", "", 21, 60, 12);
		Product p2a = service.createBeer("", "", 10, 40, 12);
		Product p3 = service.createCarbonicAcid("", "", 20, 20);
		Product p4 = service.createDraftBeerMachine("", "", 20);
		Product p5 = service.createGiftBasket("", "", 20);
		Product p6 = service.createGlass("", "", 12);
		Product p7 = service.createKeg("", "", 20, 29);
		Product p8 = service.createTour("", "", 20);
		Product p9 = service.createVoucher("", "", 20, 20);

		ArrayList<Product> beers = new ArrayList<>();
		beers.add(p2);
		beers.add(p2a);

		for (int i = 0; i < service.getAllBeers().size(); i++) {
			assertEquals(beers.get(i), service.getAllBeers().get(i));
		}

		assertEquals(p2, service.getAllBottleBeers().get(0));
		assertEquals(p2a, service.getAllDraftBeers().get(0));
		assertEquals(p3, service.getAllCarbonicAcids().get(0));
		assertEquals(p4, service.getAllDraftBeerMachines().get(0));
		assertEquals(p5, service.getAllGiftBaskets().get(0));
		assertEquals(p6, service.getAllGlasses().get(0));
		assertEquals(p7, service.getAllKegs().get(0));
		assertEquals(p8, service.getAllTours().get(0));
		assertEquals(p9, service.getAllVouchers().get(0));
	}

	@Test
	public void getProductsNotAddedToPriceListTC53() {
		Product p1 = service.createProduct("", "", 20);
		Product p2 = service.createProduct("", "", 20);
		Product p3 = service.createProduct("", "", 20);
		Product p4 = service.createProduct("", "", 20);

		PriceList pll = service.createPriceList("");
		pll.createPriceListLine(20, p1);
		pll.createPriceListLine(20, p1);
		pll.createPriceListLine(20, p1);
		pll.createPriceListLine(20, p2);

		ArrayList<Product> tester = new ArrayList<>();
		tester.add(p3);
		tester.add(p4);

		for (int i = 0; i < service.getProductsNotAddedToPriceList(pll).size(); i++) {
			assertEquals(tester.get(i), service.getProductsNotAddedToPriceList(pll).get(i));
		}
	}

	@Test
	public void isValidEmptyTC54() {
		String empty = "f";
		assertEquals(true, service.isEmpty(empty));
	}

	@Test
	public void isValidDoubleTC55() {
		String price = "123.12412";
		assertEquals(true, service.isValidDouble(price));
	}

	@Test
	public void isValidIntegerTC56() {
		String number = "1231415";
		assertEquals(true, service.isValidInteger(number));
	}
	@Test
	public void isValidEmailTC57() {
		String mail = "hej_12HS@gmail.com";
		assertEquals(true, service.isValidEmail(mail));
	}


	@Test
	public void createDeliveryTC58() {
		Delivery d = service.createDelivery("Levering", "Radius af 20km", 100);

		String name = d.getName();
		String description = d.getDescription();
		int inStock = d.getInStock();

		assertEquals("Levering", name);
		assertEquals("Radius af 20km", description);
		assertEquals(100, inStock);
	}


	@Test
	public void updateDeliveryTC59() {
		Delivery d = service.createDelivery("Levering", "Radius af 20km", 100);

		String name = d.getName();
		String description = d.getDescription();
		int inStock = d.getInStock();

		assertEquals("Levering", name);
		assertEquals("Radius af 20km", description);
		assertEquals(100, inStock);
	}
	
	
	@Test
	public void getUniquePriceListLineTC60() {
	
		PriceList pl = service.createPriceList("test butik");
		Order o = service.createOrder(LocalDateTime.of(2017, 10, 10, 13, 00),pl);
		Product p1 = service.createProduct("test1", "", 20);
		Product p2 = service.createProduct("test2", "", 20);
		Product p3 = service.createProduct("test3", "", 20);
	
		PriceListLine pll1 = pl.createPriceListLine(20, p1);
		PriceListLine pll2 = pl.createPriceListLine(20, p2);
		PriceListLine pll3 = pl.createPriceListLine(20, p3);

		ArrayList<PriceListLine> tester = new ArrayList<>();
		tester.add(pll1);
		assertEquals(tester.get(0), service.getPriceListLinesByFilter("test1", o).get(0));
		tester.clear();

		tester.add(pll2);
		assertEquals(tester.get(0), service.getPriceListLinesByFilter("tEsT2", o).get(0));
		tester.clear();

		tester.add(pll3);
		assertEquals(tester.get(0), service.getPriceListLinesByFilter("tEsT3", o).get(0));
		tester.clear();

		tester.add(pll1);
		assertEquals(0, service.getPriceListLinesByFilter("sdgfasdgasdg", o).size());
		tester.clear();
	}
}
