package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.time.LocalDate;
import java.time.LocalDateTime;

import org.junit.Before;
import org.junit.Test;

import application.model.Order;
import application.model.OrderLine;
import application.model.PriceList;
import application.model.Product;
import application.model.Reservation;
import application.model.TourBooking;

public class OrderLineTest {
	private OrderLine object;
	private Product product;
	private Order order;
	private PriceList priceList;

	@Before
	public void setUp() throws Exception {
		product = new Product("", "", 20);
		order = new Order(LocalDateTime.now(), priceList);
		object = new OrderLine(product, 23, order);
	}

	@Test
	public void TC1Constructor() {
		assertEquals(order, object.getOrder());
		assertEquals(product, object.getProduct());
		assertEquals(23, object.getQuantity());
		assertEquals(null, object.getReservation());
		assertEquals(null, object.getTourbooking());
		assertEquals(0, object.getTotalPrice(), 0.1);

	}

	@Test
	public void getProductTC2() {
		Product p = new Product("", "", 12);
		object.setProduct(p);

		assertEquals(p, object.getProduct());
	}

	@Test
	public void getReservationTC3() {
		Reservation r = new Reservation(LocalDate.now(), LocalDate.now(), object);
		object.setReservation(r);

		assertNotNull(object.getReservation());
	}

	@Test
	public void getTourBookingTC4() {
		TourBooking t = new TourBooking(LocalDateTime.now(), object);
		object.setTourbooking(t);

		assertNotNull(object.getTourbooking());
	}

	@Test
	public void getOrderTC5() {
		Order o = new Order(LocalDateTime.now(), priceList);
		object.setOrder(o);

		assertEquals(o, object.getOrder());
	}

	@Test
	public void getSinglePriceTC6() {
		object.setTotalPrice(23);
		assertEquals(1, object.getSinglePrice(), 0.1);
	}

	@Test
	public void getProductNameTC7() {

		assertEquals("", object.getProductName());
	}
}
