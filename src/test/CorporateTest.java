package test;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import application.model.Corporate;

public class CorporateTest {
	Corporate object;

	@Before
	public void setUp() throws Exception {
		object = new Corporate("Viby katedralskole", "Sandhøjvej", 8260, 88888888, "mail@gmail.com", 1231412341,
				"Keld");
	}

	@Test
	public void TC1Constructor() {
		String navn = object.getName();
		String address = object.getAddress();
		int zipCode = object.getZipCode();
		int phone = object.getPhone();
		String mail = object.getMail();
		int cvr = object.getCvr();
		String contactPerson = object.getContactPerson();

		assertEquals("Viby katedralskole", navn);
		assertEquals("Sandhøjvej", address);
		assertEquals(8260, zipCode);
		assertEquals(88888888, phone);
		assertEquals("mail@gmail.com", mail);
		assertEquals(1231412341, cvr);
		assertEquals("Keld", contactPerson);
	}

	@Test
	public void toStringTC2() {
		String expected = "Navn: Viby katedralskole\n" + "Adresse: Sandhøjvej\n" + "Postnr.: 8260\n"
				+ "Tlf.: 88888888\n" + "Email: mail@gmail.com\n" + "CVR: 1231412341\n" + "Kontakt: Keld";
		String actual = object.toString();
		assertEquals(expected, actual);
	}

}
