package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.time.LocalDateTime;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import application.model.Customer;
import application.model.Order;
import application.model.OrderLine;
import application.model.Payment;
import application.model.PaymentMethod;
import application.model.PriceList;
import application.model.Product;
import application.model.products.Beer;
import application.model.products.Voucher;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class OrderTest {
	Order object;
	PriceList butik;
	Product product;
	Product product2;
	Payment payment;
	Payment payment2;
	OrderLine orderLine;
	OrderLine orderLine2;

	@Before
	public void setUp() throws Exception {
		product = new Beer("Fadøl", "En god fadøl", 10, 60, 4.7);
		product2 = new Product("Sodavand", "Smager godt", 10);
		butik = new PriceList("Butik");
		butik.createPriceListLine(30, product);
		butik.createPriceListLine(20, product2);
		object = new Order(LocalDateTime.of(2017, 10, 10, 10, 10), butik);
		payment = new Payment(PaymentMethod.Klippekort, LocalDateTime.of(2017, 10, 10, 10, 00), 60);
		payment2 = new Payment(PaymentMethod.Dankort, LocalDateTime.of(2017, 10, 10, 10, 00), 20);
		orderLine = new OrderLine(product, 1, object);
		orderLine2 = new OrderLine(product2, 1, object);
	}

	@Test
	public void TC1Constructor() {
		int orderNumber = object.getOrderNumber();
		ArrayList<OrderLine> lstOrderLines = object.getLstOrderlines();
		ArrayList<Payment> lstPayments = object.getLstPayments();
		Customer customer = object.getCustomer();
		LocalDateTime date = object.getDate();
		double balance = object.getBalance();
		boolean delivery = object.isDelivery();
		PriceList priceList = object.getPriceList();
		assertEquals(0, lstOrderLines.size());
		assertEquals(0, lstPayments.size());
		assertEquals(null, customer);
		assertEquals(LocalDateTime.of(2017, 10, 10, 10, 10), date);
		assertEquals(0, balance, 0.0001);
		assertFalse(delivery);
		assertEquals(butik, priceList);
	}

	@Test
	public void getLstOrderLinesTC2() {
		ArrayList<OrderLine> lstOrderLines = new ArrayList<>();
		lstOrderLines.add(orderLine);
		lstOrderLines.add(orderLine2);
		object.addOrderLine(orderLine);
		object.addOrderLine(orderLine);
		object.addOrderLine(orderLine2);
		object.addOrderLine(orderLine2);
		assertEquals(lstOrderLines, object.getLstOrderlines());
	}

	@Test
	public void getLstOrderLinesTC3() {
		ArrayList<OrderLine> lstOrderLines = new ArrayList<>();
		object.addOrderLine(orderLine);
		object.addOrderLine(orderLine2);
		object.removeOrderLine(orderLine);
		object.removeOrderLine(orderLine2);
		assertEquals(lstOrderLines, object.getLstOrderlines());
	}

	@Test
	public void getLstOrderLinesTC4() {
		ArrayList<OrderLine> lstOrderLines = new ArrayList<>();
		object.removeOrderLine(orderLine);
		assertEquals(lstOrderLines, object.getLstOrderlines());
	}

	@Test
	public void calculateTotalPriceTC5() {
		object.addOrderLine(orderLine);
		object.addOrderLine(orderLine2);
		orderLine.setTotalPrice(30);
		orderLine2.setTotalPrice(20);
		assertEquals(50, object.calculateTotalPrice(), 0.0001);
	}

	@Test
	public void createOrderLineTC6() {
		OrderLine orderLine = object.createOrderLine(product, 1);
		assertEquals(product, orderLine.getProduct());
		assertEquals(1, orderLine.getQuantity());
	}

	@Test
	public void getStampsTC7() {
		object.createOrderLine(new Voucher("Klippekort", "4Klip alt, kan bruges til fadøl", 10, 4), 3);
		object.createOrderLine(product, 3);
		assertEquals(12, object.getStamps());
	}

	@Test
	public void getUsedStampsTC8() {
		object.addPayment(payment);
		object.addPayment(payment);
		object.addPayment(payment2);
		object.addPayment(payment2);
		assertEquals(2, object.getUsedStamps());
	}

	@Test
	public void getisFinishTC9() {
		assertTrue(object.getIsFinish());
	}

	@Test
	public void getIsFinishTC10() {
		object.setBalance(1);
		assertFalse(object.getIsFinish());
	}
	
	@Test
	public void updateBalanceOnOrderTC11() {
		orderLine.setTotalPrice(33);
		orderLine2.setTotalPrice(99);	
		object.addOrderLine(orderLine);
		object.addOrderLine(orderLine2);
		object.updateBalanceOnOrder();

		assertFalse(object.getIsFinish());
	}

}
