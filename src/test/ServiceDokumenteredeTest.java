package test;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.time.LocalDateTime;

import org.junit.Before;
import org.junit.Test;

import application.model.Order;
import application.model.OrderLine;
import application.model.PriceList;
import application.model.Product;
import application.model.Reservation;
import application.model.TourBooking;
import application.model.products.Beer;
import application.model.products.GiftBasket;
import application.model.products.Glass;
import application.model.products.Keg;
import application.service.Service;

public class ServiceDokumenteredeTest {

	private GiftBasket gb;
	private Beer b;
	private Glass g;
	private PriceList pl;
	private Order o;
	private Product p;
	private Keg k;
	private OrderLine ol;
	private final LocalDate now = LocalDate.of(2017, 10, 25);
	private Service service;

	@Before
	public void setUp() throws Exception {
		Service.shutDownService();
		service = Service.getService();
		gb = service.createGiftBasket("Gavekurv", "", 10);
		g = service.createGlass("Glas", "", 20);
		b = service.createBeer("Foraarsbryg", "", 20, 60, 4.7);

		pl = service.createPriceList("tester");
		p = service.createProduct("Test produkt", "", 10);
		k = service.createKeg("Test produkt 2", "", 10, 25);
		o = service.createOrder(LocalDateTime.of(2017, 10, 24, 13, 0), pl);
	}

	@Test
	public void updateGiftBasketStockTC1() {
		for (int i = 0; i < 1; i++) {
			service.addBeerToGiftBasket(gb, b);
			service.addGlassToGiftBasket(gb, g);
		}

		service.updateGiftBasketStock(gb, 2);

		assertEquals(12, gb.getInStock());
		assertEquals(8, g.getInStock());
		assertEquals(8, b.getInStock());
	}

	@Test
	public void updateGiftBasketStockTC2() {
		for (int i = 0; i < 1; i++) {
			service.addBeerToGiftBasket(gb, b);
			service.addGlassToGiftBasket(gb, g);
		}

		service.updateGiftBasketStock(gb, -10);

		assertEquals(0, gb.getInStock());
		assertEquals(20, g.getInStock());
		assertEquals(20, b.getInStock());
	}

	@Test
	public void updateGiftBasketStockTC3() {
		for (int i = 0; i < 1; i++) {
			service.addBeerToGiftBasket(gb, b);
			service.addGlassToGiftBasket(gb, g);
		}

		service.updateGiftBasketStock(gb, -9);

		assertEquals(1, gb.getInStock());
		assertEquals(19, g.getInStock());
		assertEquals(19, b.getInStock());
	}

	@Test
	public void updateGiftBasketStockTC4() {
		for (int i = 0; i < 1; i++) {
			service.addBeerToGiftBasket(gb, b);
			service.addGlassToGiftBasket(gb, g);
		}

		service.updateGiftBasketStock(gb, 10);

		assertEquals(20, gb.getInStock());
		assertEquals(0, g.getInStock());
		assertEquals(0, b.getInStock());
	}

	@Test
	public void updateGiftBasketStockTC5() {
		for (int i = 0; i < 1; i++) {
			service.addBeerToGiftBasket(gb, b);
			service.addGlassToGiftBasket(gb, g);
		}

		service.updateGiftBasketStock(gb, 9);

		assertEquals(19, gb.getInStock());
		assertEquals(1, g.getInStock());
		assertEquals(1, b.getInStock());
	}

	@Test
	public void updateGiftBasketStockTC6() {
		for (int i = 0; i < 1; i++) {
			service.addGlassToGiftBasket(gb, g);
		}

		service.updateGiftBasketStock(gb, -10);

		assertEquals(0, gb.getInStock());
		assertEquals(20, g.getInStock());
	}

	@Test
	public void updateGiftBasketStockTC7() {
		for (int i = 0; i < 1; i++) {
			service.addGlassToGiftBasket(gb, g);
		}

		service.updateGiftBasketStock(gb, -9);

		assertEquals(1, gb.getInStock());
		assertEquals(19, g.getInStock());
	}

	@Test
	public void updateGiftBasketStockTC8() {
		for (int i = 0; i < 1; i++) {
			service.addGlassToGiftBasket(gb, g);
		}

		service.updateGiftBasketStock(gb, 10);

		assertEquals(20, gb.getInStock());
		assertEquals(0, g.getInStock());
	}

	@Test
	public void updateGiftBasketStockTC9() {
		for (int i = 0; i < 1; i++) {
			service.addGlassToGiftBasket(gb, g);
		}

		service.updateGiftBasketStock(gb, 9);

		assertEquals(19, gb.getInStock());
		assertEquals(1, g.getInStock());
	}

	@Test
	public void updateGiftBasketStockTC10() {
		for (int i = 0; i < 1; i++) {
			service.addBeerToGiftBasket(gb, b);
		}

		service.updateGiftBasketStock(gb, -10);

		assertEquals(0, gb.getInStock());
		assertEquals(20, b.getInStock());
	}

	@Test
	public void updateGiftBasketStockTC11() {
		for (int i = 0; i < 1; i++) {
			service.addBeerToGiftBasket(gb, b);
		}

		service.updateGiftBasketStock(gb, -9);

		assertEquals(1, gb.getInStock());
		assertEquals(19, b.getInStock());
	}

	@Test
	public void updateGiftBasketStockTC12() {
		for (int i = 0; i < 1; i++) {
			service.addBeerToGiftBasket(gb, b);
		}

		service.updateGiftBasketStock(gb, 10);

		assertEquals(20, gb.getInStock());
		assertEquals(0, b.getInStock());
	}

	@Test
	public void updateGiftBasketStockTC13() {
		for (int i = 0; i < 1; i++) {
			service.addBeerToGiftBasket(gb, b);
		}

		service.updateGiftBasketStock(gb, 9);

		assertEquals(19, gb.getInStock());
		assertEquals(1, b.getInStock());
	}

	@Test
	public void updateGiftBasketStockTC14() {
		service.updateGiftBasketStock(gb, 10);

		assertEquals(20, gb.getInStock());
	}

	@Test
	public void updateGiftBasketStockTC15() {
		service.updateGiftBasketStock(gb, 999999);

		assertEquals(1000009, gb.getInStock());
	}

	@Test
	public void updateGiftBasketStockTC16() {
		for (int i = 0; i < 1; i++) {
			service.addBeerToGiftBasket(gb, b);
			service.addGlassToGiftBasket(gb, g);
		}

		try {
			service.updateGiftBasketStock(gb, -11);
		} catch (IllegalArgumentException e) {
			assertEquals("Ikke nok på lager af " + gb.getName() + " til at fjerne mængden", e.getMessage());
		}
	}

	@Test
	public void updateGiftBasketStockTC17() {
		for (int i = 0; i < 1; i++) {
			service.addBeerToGiftBasket(gb, b);
			service.addGlassToGiftBasket(gb, g);
		}

		try {
			service.updateGiftBasketStock(gb, 11);
		} catch (IllegalArgumentException e) {
			assertEquals("Der er ikke nok på lager af " + b.getName() + " til at opdatere mængden af gavekurven",
					e.getMessage());
		}
	}

	@Test
	public void updateGiftBasketStockTC18() {
		for (int i = 0; i < 1; i++) {
			service.addGlassToGiftBasket(gb, g);
		}

		try {
			service.updateGiftBasketStock(gb, 11);
		} catch (IllegalArgumentException e) {
			assertEquals("Der er ikke nok på lager af " + g.getName() + " til at opdatere mængden af gavekurven",
					e.getMessage());
		}
	}

	@Test
	public void updateGiftBasketStockTC19() {
		for (int i = 0; i < 1; i++) {
			service.addBeerToGiftBasket(gb, b);
		}

		try {
			service.updateGiftBasketStock(gb, 11);
		} catch (IllegalArgumentException e) {
			assertEquals("Der er ikke nok på lager af " + b.getName() + " til at opdatere mængden af gavekurven",
					e.getMessage());
		}
	}

	@Test
	public void updateGiftBasketStockTC20() {
		try {
			service.updateGiftBasketStock(gb, -11);
		} catch (IllegalArgumentException e) {
			assertEquals("Ikke nok på lager af " + gb.getName() + " til at fjerne mængden", e.getMessage());
		}
	}

	// ----------------------------------------------------------------------------

	@Test
	public void getLeasedProductsTC1() {
		service.createReservation(LocalDate.of(2017, 10, 14), null, o.createOrderLine(k, 1));
		o.setBalance(100);
		assertEquals(1, service.getLeasedProductsTester(now).size());
	}

	@Test
	public void getLeasedProductsTC2() {
		service.createReservation(LocalDate.of(2017, 10, 24), null, o.createOrderLine(k, 1));
		o.setBalance(100);
		assertEquals(1, service.getLeasedProductsTester(now).size());
	}

	@Test
	public void getLeasedProductsTC3() {
		o.setBalance(0);
		service.createReservation(LocalDate.of(2017, 10, 24), null, o.createOrderLine(k, 1));
		assertEquals(0, service.getLeasedProductsTester(now).size());
	}

	@Test
	public void getLeasedProductsTC4() {
		o.setBalance(-1);
		service.createReservation(LocalDate.of(2017, 10, 24), null, o.createOrderLine(k, 1));

		assertEquals(0, service.getLeasedProductsTester(now).size());
	}

	@Test
	public void getLeasedProductsTC5() {
		o.setBalance(1);
		service.createReservation(LocalDate.of(2017, 10, 24), null, o.createOrderLine(p, 1));

		assertEquals(0, service.getLeasedProductsTester(now).size());
	}

	@Test
	public void getLeasedProductsTC6() {
		o.setBalance(1);
		service.createReservation(LocalDate.of(2017, 10, 25), null, o.createOrderLine(k, 1));

		assertEquals(0, service.getLeasedProductsTester(now).size());
	}

	@Test
	public void getLeasedProductsTC7() {
		o.setBalance(1);
		service.createReservation(LocalDate.of(2017, 10, 26), null, o.createOrderLine(k, 1));

		assertEquals(0, service.getLeasedProductsTester(now).size());
	}

	@Test
	public void getLeasedProductsTC8() {
		o.setBalance(1);
		service.createReservation(LocalDate.of(2017, 10, 25), null, o.createOrderLine(p, 1));

		assertEquals(0, service.getLeasedProductsTester(now).size());
	}

	@Test
	public void getLeasedProductsTC9() {
		o.setBalance(1);
		service.createReservation(LocalDate.of(2017, 10, 26), null, o.createOrderLine(p, 1));

		assertEquals(0, service.getLeasedProductsTester(now).size());
	}

	@Test
	public void removeOrderTC1() {
		ol = o.createOrderLine(p, 2);
		TourBooking t = service.createTourBooking(LocalDateTime.of(2017, 10, 24, 13, 0), ol);
		Reservation r = service.createReservation(LocalDate.of(2017, 10, 24), LocalDate.of(2017, 10, 25), ol);

		service.removeOrder(o);

		assertEquals(0, service.getOrders().size());
		assertEquals(0, service.getReservations().size());
		assertEquals(0, service.getTourBookings().size());
		for (Order o : service.getOrders()) {
			assertEquals(0, o.getLstOrderlines().size());
		}
	}

	@Test
	public void removeOrderTC2() {
		ol = o.createOrderLine(p, 2);
		Reservation r = service.createReservation(LocalDate.of(2017, 10, 24), LocalDate.of(2017, 10, 25), ol);

		service.removeOrder(o);

		assertEquals(0, service.getOrders().size());
		assertEquals(0, service.getReservations().size());
		for (Order o : service.getOrders()) {
			assertEquals(0, o.getLstOrderlines().size());
		}
	}

	@Test
	public void removeOrderTC3() {
		ol = o.createOrderLine(p, 2);

		service.removeOrder(o);

		assertEquals(0, service.getOrders().size());
		for (Order o : service.getOrders()) {
			assertEquals(0, o.getLstOrderlines().size());
		}
	}

	@Test
	public void removeOrderTC4() {
		ol = o.createOrderLine(p, 2);
		TourBooking t = service.createTourBooking(LocalDateTime.of(2017, 10, 24, 13, 0), ol);

		service.removeOrder(o);

		assertEquals(0, service.getOrders().size());
		assertEquals(0, service.getTourBookings().size());
		for (Order o : service.getOrders()) {
			assertEquals(0, o.getLstOrderlines().size());
		}
	}

	@Test
	public void removeOrderTC5() {
		service.removeOrder(o);

		assertEquals(0, service.getOrders().size());
	}
}
