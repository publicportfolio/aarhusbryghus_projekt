package test;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import application.model.products.Tour;

public class TourTest {
	Tour object;

	@Before
	public void setUp() throws Exception {
		object = new Tour ("Rundvisning", "En times rundvisning med smagsprøver efterfulgt", 10);
	}

	@Test
	public void TC1Constructor() {
		String name = object.getName();
		String description = object.getDescription();
		int inStock = object.getInStock();
		assertEquals("Rundvisning",name);
		assertEquals("En times rundvisning med smagsprøver efterfulgt",description);
		assertEquals(10,inStock);
	}

}
