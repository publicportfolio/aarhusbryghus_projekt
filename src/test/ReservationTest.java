package test;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.time.LocalDateTime;

import org.junit.Before;
import org.junit.Test;

import application.model.Order;
import application.model.OrderLine;
import application.model.PriceList;
import application.model.PriceListLine;
import application.model.Reservation;
import application.model.TourBooking;
import application.model.products.Tour;

public class ReservationTest {
	
	private Reservation reservation;
	private OrderLine orderLine;
	private OrderLine orderLine2;
	
	@Before
	public void setUp() throws Exception {
		Tour tour = new Tour("Rundvisning", "En times rundvisning med smagsprøver efterfulgt", 10);
		PriceList butik = new PriceList("Butik");
		PriceListLine pll = butik.createPriceListLine(100, tour);
		Order o1 = new Order(LocalDateTime.of(2017, 1, 1, 13, 30), butik);
		orderLine = o1.createOrderLine(pll.getProduct(), 10);
		orderLine2 = o1.createOrderLine(pll.getProduct(), 15);
		
		reservation = new Reservation(LocalDate.of(2017, 1, 1), LocalDate.of(2017, 2, 1), orderLine);
	}

	@Test
	public void TC1Constructor() {
		LocalDate from = reservation.getFromDate();
		LocalDate to = reservation.getToDate();
		OrderLine ol = reservation.getOrderLine();
		
		assertEquals(LocalDate.of(2017, 1, 1),from);	
		assertEquals(LocalDate.of(2017, 2, 1),to);
		assertEquals(orderLine,ol);
	}
	
	@Test
	public void setFromDateTC2() {
		reservation.setFromDate(LocalDate.of(2017, 12, 12));
		LocalDate expected = LocalDate.of(2017, 12, 12);
		LocalDate actual = reservation.getFromDate();
	
		assertEquals(expected,actual);	
	}
	
	@Test
	public void setToDateTC3() {
		reservation.setToDate(LocalDate.of(2018, 1, 1));
		LocalDate expected = LocalDate.of(2018, 1, 1);
		LocalDate actual = reservation.getToDate();
	
		assertEquals(expected,actual);	
	}
	
	@Test
	public void setOrderDateTC4() {
		reservation.setOrderLine(orderLine2);
		OrderLine expected = orderLine2;
		OrderLine actual = reservation.getOrderLine();
	
		assertEquals(expected,actual);	
	}
}
