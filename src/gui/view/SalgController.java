package gui.view;

import java.io.IOException;
import java.time.LocalDateTime;

import javax.security.auth.callback.Callback;

import application.model.Order;
import application.model.OrderLine;
import application.model.PriceList;
import application.model.PriceListLine;
import application.model.Product;
import application.model.evaluators.IntegerEvaluator;
import application.service.Service;
import gui.MainApp;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class SalgController {

	private static Pane pane;
	private static Stage stage;
	private static PriceList aktuelPrisliste;
	private static Order aktuelOrder;
	
	@FXML
    private Label lblOverskrift;
	
	@FXML
    private Label lblTotalPris;

	@FXML
	private Label lblOrderNumber;
	
	@FXML
	private Label lblAfsluttet;
	
	@FXML
	private Label lblRestbeloeb;
	
	@FXML
	private Label lblRestbetaling;
	
	@FXML
	private Label lblKundeInfo;
	
	@FXML
	private Label lblError;
	
	@FXML
	private Button btnTilfoej;
	
	@FXML
    private TableView<PriceListLine> tblProdukter;

    @FXML
    private TableColumn<PriceListLine, String> columnNavn;

    @FXML
    private TableColumn<PriceListLine, Integer> columnBeholdning;

    @FXML
    private TableColumn<PriceListLine, Double> columnPris;

    @FXML
    private TableView<OrderLine> tblOrdreLinjer;

    @FXML
    private TableColumn<OrderLine, String> columnOrdreNavn;

    @FXML
    private TableColumn<OrderLine, Integer> columnAntal;

    @FXML
    private TableColumn<OrderLine, Double> columnStkPris;

    @FXML
    private TableColumn<OrderLine, Double> columnTotal;

    @FXML
    private ComboBox<String> cbVarerKategorier;

    @FXML
    private HBox hbKunde;
    
    @FXML
    private Button btnBetal;
    
    @FXML
    private Button btnRet;
    
    @FXML
    private Button btnSlet;
    
    @FXML
    private Button btnNyOrdre;
    
    @FXML
    private Button btnSletOrdre;
    
    @FXML
    private TextField txfSoeg;

    @FXML
    private TextField txfFiltrering;
    
    @FXML
    private Button btnSoeg;
    
    @FXML
    private Button btnNulstil;

	
	public static void initWindow(PriceList priceList, Order aktuelorder) {
		
		aktuelPrisliste = priceList;
		aktuelOrder = aktuelorder;
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/SalgsView.fxml"));
			pane = (Pane) loader.load();
			stage = new Stage();
			stage.setTitle("Nem-Bryg");
			stage.setScene(new Scene(pane));
			stage.getIcons().add(new Image("/resources/images/icon.png"));
			stage.show();
			
			 stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
		          public void handle(WindowEvent we) {
		              onClose();
		          }
		      }); 		
		}
		catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}
	
	
	 @FXML
	    void initialize() {
		 		 
		 if (aktuelOrder == null) {
			 aktuelOrder = Service.getService().createOrder(LocalDateTime.now(),aktuelPrisliste);			
		}
		 
		 btnBetal.setDisable(true);
		 lblOverskrift.setText(aktuelPrisliste.getTitle());
		 lblOrderNumber.setText(aktuelOrder.getOrderNumber()+"");
		 
		 /* Produkt tabellen */
		 columnNavn.setCellValueFactory(new PropertyValueFactory<PriceListLine, String>("ProductName"));
		 columnBeholdning.setCellValueFactory(new PropertyValueFactory<PriceListLine, Integer>("InStock"));	
		 columnPris.setCellValueFactory(new PropertyValueFactory<PriceListLine, Double>("price"));
		 getUniqePriceListLines();
		 
		 /* Ordre tabellen */
		 columnOrdreNavn.setCellValueFactory(new PropertyValueFactory<OrderLine, String>("ProductName"));
		 columnAntal.setCellValueFactory(new PropertyValueFactory<OrderLine, Integer>("quantity"));	
		 columnStkPris.setCellValueFactory(new PropertyValueFactory<OrderLine, Double>("SinglePrice"));
		 columnTotal.setCellValueFactory(new PropertyValueFactory<OrderLine, Double>("totalPrice"));   
	  
		 updateOrderRightSide();
		 getUniqePriceListLines();
		    
	 }
	 
	 public static void onClose() {
		 if (aktuelOrder != null) {		
		 if (aktuelOrder.getLstOrderlines().size() == 0) {
			Service.getService().removeOrder(aktuelOrder);
		}}
	 }
		 
	 public void callAddProductWindow() {
		 
		 if (tblProdukter.getSelectionModel().getSelectedItem() != null) {
			 TilføjProduktController.initWindow(this,aktuelOrder, tblProdukter.getSelectionModel().getSelectedItem());
		}
	}
	
	// opdatere inholdet i højreside, alt efter order situation 
	public void updateOrderRightSide() {

	tblOrdreLinjer.getItems().clear();
	tblOrdreLinjer.getItems().setAll(aktuelOrder.getLstOrderlines());
	lblTotalPris.setText(aktuelOrder.calculateTotalPrice()+ ",-");
	lblOrderNumber.setText(aktuelOrder.getOrderNumber()+"");
	lblRestbetaling.setVisible(false);
	lblRestbeloeb.setText("");
	lblError.setText("");
	txfSoeg.setText("");
	
	if (tblOrdreLinjer.getItems().size() > 0) {
		btnBetal.setDisable(false);
		lblRestbeloeb.setText(aktuelOrder.getBalance()+" ,-");
		lblRestbetaling.setVisible(true);
	}
	
	if (aktuelOrder.getCustomer() != null) {
		hbKunde.setVisible(true);
		lblKundeInfo.setText("");
		
		lblKundeInfo.setText(aktuelOrder.getCustomer().toString());
		
	}
	else {
		hbKunde.setVisible(false);
	}
	
	if (Service.getService().orderIsClosedForPayment(aktuelOrder)) {
		lblAfsluttet.setVisible(true);
		btnBetal.setDisable(true);
		lblRestbeloeb.setText("");
		lblRestbetaling.setVisible(false);
		btnRet.setDisable(true);
		btnSlet.setDisable(true);
		btnTilfoej.setDisable(true);
	}
	else {
		lblAfsluttet.setVisible(false);
	}	
	
 }
 
 	@FXML
 	void updateSingleOrderLine() {
 		if (tblOrdreLinjer.getSelectionModel().getSelectedItem() != null) {
 		OpdatereOrdreLinjeController.initWindow(this, tblOrdreLinjer.getSelectionModel().getSelectedItem());
 		}
 	}
 	
 	@FXML
 	void callPaymentWindow() {
 		
 		if (aktuelOrder.getLstOrderlines().size() > 0) {
 		
 		BetalingsController.initWindow(aktuelOrder,this);
 
 		}
 	}
 	
 	@FXML
 	void searchOrder() {
 			
 		if (Service.getService().isValidInteger(txfSoeg.getText())) {
 			if (aktuelOrder != null) {
						
 			if (Integer.parseInt(txfSoeg.getText()) == aktuelOrder.getOrderNumber()) {
 	 			lblError.setText("Du har allerede denne ordre åben.");
 			}
 			}

 			if (Service.getService().searchOrderByOrderNumber(Integer.parseInt(txfSoeg.getText()),aktuelPrisliste) != null) {
 			onClose();
 			aktuelOrder = Service.getService().searchOrderByOrderNumber(Integer.parseInt(txfSoeg.getText()),aktuelPrisliste);
 			
 			if (aktuelOrder != null) {
 			updateOrderRightSide();
			getUniqePriceListLines();
			lblOrderNumber.setText(aktuelOrder.getOrderNumber()+"");
			lblError.setText("");
			btnSletOrdre.setDisable(false);
 			}
 		}
 			else {	
	 			lblError.setText("Der findes ikke en ordre på denne prisliste med ordrenr:'" + txfSoeg.getText() + "'");
	 		}	
 		}
 		else {
			lblError.setText("Indtast et gyldigt ordrenummer.");
		}
 	}
 	
 	@FXML
 	void createNewOrder() {
 		
 		Order newOrder = Service.getService().createOrder(LocalDateTime.now(), aktuelPrisliste);
 		aktuelOrder = newOrder;
 		updateOrderRightSide();
 		btnTilfoej.setDisable(false);
 		btnSletOrdre.setDisable(false);
 		txfFiltrering.setDisable(false);
 		btnNulstil.setDisable(false);
 		getUniqePriceListLines();
 	}
 	
 	@FXML
 	void detectOrderLineSelection() {
 		
 		if (tblOrdreLinjer.getSelectionModel().getSelectedItem() != null) {
		
			if (!Service.getService().orderIsClosedForPayment(aktuelOrder)) {
				btnSlet.setDisable(false);
				btnRet.setDisable(false);
			}
		}
 		else {
 			btnSlet.setDisable(true);
			btnRet.setDisable(true);
		}

 	}
 	
 	@FXML
 	void deleteOrderLine() {
 		
 		if (tblOrdreLinjer.getSelectionModel().getSelectedItem() != null) {
			Service.getService().removeOrderLine(aktuelOrder, tblOrdreLinjer.getSelectionModel().getSelectedItem());
			updateOrderRightSide();
 		} 
		getUniqePriceListLines();
 		
 	}
 	
 	@FXML
 	void deleteOrdre() {
 		
 		Service.getService().removeOrder(aktuelOrder);
 		aktuelOrder = null;
 		tblOrdreLinjer.getItems().clear();
 		lblOrderNumber.setText("");
 		lblRestbeloeb.setText("");
 		lblTotalPris.setText("");
 		lblKundeInfo.setText("");
 		lblRestbetaling.setVisible(false);
 		hbKunde.setVisible(false);
 		btnSletOrdre.setDisable(true);
 		btnTilfoej.setDisable(true);
 		txfFiltrering.setText("");
 		txfFiltrering.setDisable(true);
 		btnNulstil.setDisable(true);
 		btnBetal.setDisable(true);
 		btnRet.setDisable(true);
 		btnSlet.setDisable(true);
 	}
 	
 	@FXML
 	void onFilterChange() {	
 		tblProdukter.getItems().clear();
 		tblProdukter.getItems().addAll(Service.getService().getPriceListLinesByFilter(txfFiltrering.getText(), aktuelOrder));	
 	}
 	
 	@FXML
 	void clearFilter() {
 		txfFiltrering.setText("");
 		getUniqePriceListLines();
 	}
 	
 	@FXML
 	void getUniqePriceListLines() {	
 		tblProdukter.getItems().clear();
 		tblProdukter.getItems().addAll(Service.getService().getUniquePriceListLine(aktuelOrder));
 		
 	}
 
 	
 
}
