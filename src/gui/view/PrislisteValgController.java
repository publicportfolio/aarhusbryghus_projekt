package gui.view;

import java.io.IOException;

import application.model.PriceList;
import application.service.Service;
import gui.MainApp;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class PrislisteValgController {

	private static Pane pane;
	private static Stage stage;

	@FXML
	private ComboBox<PriceList> cbPrislister;

	@FXML
	private Button btnVaelg;
	
	@FXML
	private Label lblError;

	public static void initWindow() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/PrislisteOversigt.fxml"));
			pane = (Pane) loader.load();
			stage = new Stage();
			stage.setTitle("Nem-Bryg");
			stage.setScene(new Scene(pane));
			stage.getIcons().add(new Image("/resources/images/icon.png"));
			stage.show();
		} catch (IOException e) {
			System.out.println(e.getMessage());

		}
	}

	@FXML
	public void callSaleWindow() {
		lblError.setText("");
		
		if (cbPrislister.getSelectionModel().getSelectedItem() != null) {
			PriceList valgtPrisliste = cbPrislister.getSelectionModel().getSelectedItem();
			SalgController.initWindow(valgtPrisliste, null);
		}
		else {
			lblError.setText("Du skal vælge en prisliste");
		}
		
	}

	@FXML
	void initialize() {
		cbPrislister.setPromptText("Vælg prisliste");
		cbPrislister.getItems().addAll(Service.getService().getPriceLists());
	}

}
