package gui.view;

import java.io.IOException;
import java.util.ArrayList;

import application.model.PriceList;
import application.model.PriceListLine;
import application.model.Product;
import application.model.products.GiftBasket;
import application.service.Service;
import gui.MainApp;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class LagerStyringController {
	private static Pane pane;
	private static Stage stage;

	public static void initWindow() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/LagerStyring.fxml"));
			pane = (Pane) loader.load();
			stage = new Stage();
			stage.setTitle("Kasseapparatet - Nem-Bryg");
			stage.setScene(new Scene(pane));
			stage.setResizable(false);
			stage.getIcons().add(new Image("/resources/images/icon.png"));
			stage.show();
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}

	@FXML
	private ComboBox<String> cbType;

	@FXML
	private ComboBox<PriceList> cbProduktLister;


	@FXML
	private TextArea txaBeskrivelseAfProdukt;

	@FXML
	private Label lblError;

	@FXML
	private Label lblError2;

	@FXML
	private Label lblError3;

	@FXML
	private Label lblError4;

	@FXML
	private Label lblError5;

	@FXML
	private Label lblNavn;

	@FXML
	private TextField txfAngivPris;

	@FXML
	private TextField txfTilføjTilLager;

	// ------------------------ Buttons ------------------------------------
	@FXML
	private Button btnTilføjTilLager;

	@FXML
	private Button btnTilføjProduktTilProduktListe;

	@FXML
	private Button btnFjernProduktFraProduktListe;

	@FXML
	private Button btnOpdaterProduktPåPrisListe;

	// ------------------------ LEFT TABLE(Produkttable)------------------------------------
	@FXML
	private TableView<Product> tblProducts;

	@FXML
	private TableColumn<Product, String> tblProductColumnNavn;

	@FXML
	private TableColumn<Product, Integer> tblProductColumnInStock;

	private ObservableList<Product> dataTblProducts = FXCollections.observableArrayList();

	// ------------------------ RIGHT TABLE(Prislistetable)------------------------------------
	@FXML
	private TableView<PriceListLine> tblPriceListLines;

	@FXML
	private TableColumn<PriceListLine, Product> tblPriceListColumnNavn;

	@FXML
	private TableColumn<PriceListLine, Double> tblPriceListColumnPris;

	private ObservableList<PriceListLine> dataTblPriceListLines = FXCollections.observableArrayList();

	@FXML
	void initialize() {
		tblPriceListColumnNavn.setCellValueFactory(new PropertyValueFactory<PriceListLine, Product>("ProductName"));
		tblPriceListColumnPris.setCellValueFactory(new PropertyValueFactory<PriceListLine, Double>("price"));

		tblProductColumnNavn.setCellValueFactory(new PropertyValueFactory<Product, String>("ToString"));
		tblProductColumnInStock.setCellValueFactory(new PropertyValueFactory<Product, Integer>("inStock"));

		fillContent();
	}

	public void fillContent() {
		fillCBType();
		fillCBProductListTitles();
		fillTblProducts();
	}

	public void fillCBType() {
		cbType.getItems().add("Alle produkter");
		cbType.getItems().add("Ikke tilføjede");
		cbType.getItems().add("Alle øl");
		cbType.getItems().add("Fadøl");
		cbType.getItems().add("Flaskeøl");
		cbType.getItems().add("Glas");
		cbType.getItems().add("Kulsyre");
		cbType.getItems().add("Fustage");
		cbType.getItems().add("Klippekort");
		cbType.getItems().add("Gaveæske");
		cbType.getItems().add("Rundvisning");
		cbType.getItems().add("Fadølsanlæg");
		cbType.getItems().add("Leveringer");
		cbType.setValue("Alle produkter");
	}

	public void fillCBProductListTitles() {
		cbProduktLister.getItems().setAll(Service.getService().getPriceLists());
	}

	public void fillTblPriceListLines() {
		cbProduktListerAction();
	}

	public void fillTblProducts() {
		cbTypeAction();
	}


	//-------------------------ACTIONS--------------------------------------


	public void tblProductsOnMouseClickedAction() {
		clearTextFields();
		clearLblError();

		btnFjernProduktFraProduktListe.setVisible(false);
		btnOpdaterProduktPåPrisListe.setVisible(false);
		btnTilføjProduktTilProduktListe.setVisible(true);
		Product product = null;
		PriceList priceList = null;
		product = tblProducts.getSelectionModel().getSelectedItem();
		priceList = cbProduktLister.getValue();

		if(product == null) {
			return;
		}
		txaBeskrivelseAfProdukt.setText(product.getDescription());
		lblNavn.setText(product.toString());

		if(priceList == null) {
			return;
		}

		for(PriceListLine e : priceList.getLstPriceListLines()) {
			if (e.getProduct().equals(product)) {
				btnTilføjProduktTilProduktListe.setDisable(true);
				return;
			}
		}
		btnTilføjProduktTilProduktListe.setDisable(false);
	}

	public void tblPriceListLinesMouseClickedAction() {
		clearTextFields();
		clearLblError();
		PriceListLine priceListLine = null;
		priceListLine = tblPriceListLines.getSelectionModel().getSelectedItem();

		if(priceListLine == null) {
			return;
		}

		txfAngivPris.setText(priceListLine.getPrice()+"");
		lblNavn.setText(priceListLine.getProductName());
		btnFjernProduktFraProduktListe.setDisable(false);
		btnOpdaterProduktPåPrisListe.setDisable(false);
		btnFjernProduktFraProduktListe.setVisible(true);
		btnOpdaterProduktPåPrisListe.setVisible(true);
		btnTilføjProduktTilProduktListe.setVisible(false);
	}

	public void btnFjernProduktFraProduktListeAction() {
		PriceListLine priceListLine = null;
		PriceList priceList = null;
		priceList = cbProduktLister.getValue();
		priceListLine = tblPriceListLines.getSelectionModel().getSelectedItem();

		if (priceListLine == null || priceList == null) {
			lblError.setText("Dobbelttjek at du har valgt en prisliste og et produkt");
			return;
		}

		Service.getService().removePriceListLineFromPriceList(priceList, priceListLine);
		dataTblPriceListLines.remove(priceListLine);
		tblPriceListLines.getItems().setAll(dataTblPriceListLines);
		clearTextFields();

		btnFjernProduktFraProduktListe.setDisable(true);
		btnOpdaterProduktPåPrisListe.setDisable(true);
	}

	public void btnTilføjProduktTilProduktListeAction() {
		Product product = null;
		PriceList priceList = null;
		priceList = cbProduktLister.getSelectionModel().getSelectedItem();
		product = tblProducts.getSelectionModel().getSelectedItem();
		if(priceList == null || product == null) {
			lblError5.setText("Dobbelttjek at du har valgt en prisliste og et produkt");
			return;
		}
		else {
			for(PriceListLine e : priceList.getLstPriceListLines()) {
				if (e.getProduct().equals(product)) {
					lblError5.setText("Du kan ikke tilføje det samme produkt til listen");
					return;
				}
			}
		}
		if(!Service.getService().isValidDouble(txfAngivPris.getText().trim())) {
			lblError5.setText("Hver sikker på at du har skrevet et positivt tal i feltet");
			return;
		}
		priceList.createPriceListLine(Double.parseDouble(txfAngivPris.getText().trim()), product);
		btnTilføjProduktTilProduktListe.setDisable(true);
		clearLblError();
		clearTextFields();
		fillTblPriceListLines();
	}

	@FXML
	public void btnOpdaterPrisAction() {
		PriceListLine priceListLine = null;
		priceListLine = tblPriceListLines.getSelectionModel().getSelectedItem();
		if(priceListLine == null) {
			lblError.setText("Ingen produkt valgt");
			return;
		}
		if(!Service.getService().isValidDouble(txfAngivPris.getText().trim())) {
			lblError.setText("Hver sikker på at du har skrevet et positivt tal i feltet");
			return;
		}
		priceListLine.setPrice(Double.parseDouble(txfAngivPris.getText().trim()));
		btnOpdaterProduktPåPrisListe.setDisable(true);
		btnFjernProduktFraProduktListe.setDisable(true);
		fillTblPriceListLines();
		clearTextFields();
	}

	@FXML
	public void btnOpretNytProduktListeAction() {
		OpretRedigerProduktListeController.initWindow(null,this);
	}

	@FXML
	public void btnRedigerProduktListeAction() {
		clearLblError();
		if(cbProduktLister.getValue()==null) {
			lblError2.setText("Vælg en liste før du kan redigere den");
			return;
		}
		OpretRedigerProduktListeController.initWindow(cbProduktLister.getValue(),this);
	}

	@FXML
	public void btnSletProduktListeAction() {
		clearLblError();
		PriceList priceList = null;
		try{
			priceList = cbProduktLister.getSelectionModel().getSelectedItem();
		}
		catch(NullPointerException e) {
			lblError2.setText("Der er ingen lister at slette");
			return;
		}

		if (priceList == null) {
			lblError2.setText("Vælg en liste før du kan slette den");
			return;
		}
		Service.getService().removePriceList(priceList);
		dataTblPriceListLines.clear();
		tblPriceListLines.getItems().clear();
		cbProduktLister.getItems().clear();
		cbProduktLister.getItems().setAll(Service.getService().getPriceLists());
	}


	@FXML
	public void btnRedigerProduktAction() {
		if(tblProducts.getSelectionModel().getSelectedItem()==null) {
			lblError3.setText("Vælg et produkt før du kan redigere den");
			return;
		}
		OpretRedigerProduktController.initWindow(this, tblProducts.getSelectionModel().getSelectedItem());
	}

	@FXML
	public void btnSletProduktAction() {
		Product product = tblProducts.getSelectionModel().getSelectedItem();
		if(product == null) {
			lblError3.setText("Vælg et produkt før du kan slette det");
			return;
		}
		Service.getService().removeProduct(product);
		cbTypeAction();
		clearTextFields();
	}

	public void btnTilføjProduktAction() {
		OpretRedigerProduktController.initWindow(this, null);
	}

	public void btnTilføjTilLagerAction() {
		Product product = tblProducts.getSelectionModel().getSelectedItem();
		int	antal;
		if(product == null) {
			lblError4.setText("Intet produkt valgt");
			return;
		}

		try{
			antal = Integer.parseInt(txfTilføjTilLager.getText().trim());
		}
		catch(NumberFormatException e) {
			lblError4.setText("Fejl ved antallet, der skal tilføjes");
			return;
		}

		if(product instanceof GiftBasket) {
			GiftBasket giftBasket = (GiftBasket)product;
			try {
				Service.getService().updateGiftBasketStock(giftBasket, antal);
			}
			catch(RuntimeException e) {
				lblError4.setText(e.getMessage());
			}
		}
		else {
			try {
				Service.getService().addProductToStock(product, antal);
			}
			catch(RuntimeException e) {
				lblError4.setText(e.getMessage());
			}
		}
		fillTblProducts();
		txfTilføjTilLager.clear();
	}

	public void cbProduktListerAction() {
		dataTblPriceListLines.clear();
		tblPriceListLines.getItems().clear();
		if(cbProduktLister.getItems()==null||cbProduktLister.getValue()==null) {
			return;
		}

		for (PriceListLine e : cbProduktLister.getValue().getLstPriceListLines()) {
			dataTblPriceListLines.add(e);
		}
		tblPriceListLines.getItems().setAll(dataTblPriceListLines);
		cbTypeAction();
	}

	public void cbTypeAction() {
		dataTblProducts.clear();
		String type = cbType.getValue();
		ArrayList<Product>products = null;
		switch(type) {
		case "Alle produkter":
			products = Service.getService().getProducts();
			break;
		case "Ikke tilføjede":
			if(cbProduktLister.getValue()==null) {
				tblProducts.setItems(null);
				return;
			}
			products = Service.getService().getProductsNotAddedToPriceList(cbProduktLister.getValue());
			break;
		case "Alle Øl":
			products = Service.getService().getAllBeers();
			break;
		case "Fadøl":
			products = Service.getService().getAllDraftBeers();
			break;
		case "Flaskeøl":
			products = Service.getService().getAllBottleBeers();
			break;
		case "Glas":
			products = Service.getService().getAllGlasses();
			break;
		case "Kulsyre":
			products = Service.getService().getAllCarbonicAcids();
			break;
		case "Fustage":
			products = Service.getService().getAllKegs();
			break;
		case "Klippekort":
			products = Service.getService().getAllVouchers();
			break;
		case "Gaveæske":
			products = Service.getService().getAllGiftBaskets();
			break;
		case "Rundvisning":
			products = Service.getService().getAllTours();
			break;
		case "Fadølsanlæg":
			products = Service.getService().getAllDraftBeerMachines();
			break;
		case "Leveringer":
			products = Service.getService().getAllDeliveries();
			break;
		default:
			products = Service.getService().getAllBeers();
			break;
		}
		for (Product e : products) {
			dataTblProducts.add(e);
		}
		tblProducts.setItems(dataTblProducts);
	}

	public void clearLblError() {
		lblError.setText("");
		lblError2.setText("");
		lblError3.setText("");
		lblError4.setText("");
		lblError5.setText("");
	}

	public void clearTextFields() {
		txfAngivPris.clear();
		lblNavn.setText("");
		txaBeskrivelseAfProdukt.clear();
	}
}




