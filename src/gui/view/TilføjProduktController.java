package gui.view;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import javax.security.auth.callback.Callback;

import application.model.Customer;
import application.model.Order;
import application.model.OrderLine;
import application.model.PriceList;
import application.model.PriceListLine;
import application.model.Product;
import application.model.Reservation;
import application.model.TourBooking;
import application.model.evaluators.IntegerEvaluator;
import application.model.evaluators.PriceEvaluator;
import application.model.products.Beer;
import application.model.products.DraftBeerMachine;
import application.model.products.Tour;
import application.service.Service;
import gui.MainApp;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class TilføjProduktController {

	private static Pane pane;
	private static Stage stage;
	private static Order aktuelOrder;
	private static PriceListLine aktuelPrisLinje;
	private static SalgController parentController;
	
	  @FXML
	    private Label lblProduktNavn;

	  @FXML
	    private Label lblError;
	  
	    @FXML
	    private TextField txfAntal;

	    @FXML
	    private TextField txfStkPris;

	    @FXML
	    private TextField txfTotal;

	    @FXML
	    private Button btnGem;
	
	    @FXML
	    private HBox hbReservation;

	    @FXML
	    private DatePicker txfFromDate;

	    @FXML
	    private DatePicker txfToDate;

	    @FXML
	    private Label lblSecondOverskrift;

	    @FXML
	    private DatePicker txfTourDate;
	    
	    @FXML
	    private HBox hbTourBooking;
	    
	    @FXML
	    private ComboBox<String> cbClock;
	    
	
	public static void initWindow(SalgController parent,Order order, PriceListLine priceListLine) {
		
		aktuelOrder = order;
		aktuelPrisLinje = priceListLine;
		parentController = parent;
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/TilføjProduktView.fxml"));
			pane = (Pane) loader.load();
			stage = new Stage();
			stage.setTitle("Nem-Bryg");
			stage.setScene(new Scene(pane));
			stage.getIcons().add(new Image("/resources/images/icon.png"));
			stage.show();
			
		}
		catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}
	
	
	 @FXML
	 void initialize() {
		 	
		 		lblProduktNavn.setText(aktuelPrisLinje.getProductName());
		 		txfAntal.setText("1");
		 		txfStkPris.setText(aktuelPrisLinje.getPrice()+"");
		 		txfTotal.setEditable(false);

		 		hbReservation.setVisible(false);
	 			hbTourBooking.setVisible(false);
	 			lblSecondOverskrift.setText("");
		 		
	 			cbClock.getItems().add("10:00");
	 			cbClock.getItems().add("11:00");
	 			cbClock.getItems().add("12:00");
	 			cbClock.getItems().add("13:00");
		 		
		 		cbClock.getSelectionModel().select(0);
		 		
		 		if (Service.getService().checkIfProductNeedsReservation(aktuelPrisLinje.getProduct())) {
		 			hbReservation.setVisible(true);
		 			lblSecondOverskrift.setText("Reservation");
		 			txfFromDate.setValue(LocalDate.now());
		 			txfToDate.setValue(LocalDate.now().plusDays(1));

				}
		 		else if (aktuelPrisLinje.getProduct() instanceof Tour) {
		 			hbTourBooking.setVisible(true);
		 			txfTourDate.setValue(LocalDate.now());
		 			lblSecondOverskrift.setText("Rundvisning");
				}	
		 		
		 		//Listener
		 		txfFromDate.onActionProperty().addListener(event -> updateTotal());
		 		
		 		updateTotal();
			 }
	 
	 
	 /*
	  * Udregner total prisen for det valgte antal,
	  * */
	 @FXML
	 private void updateTotal() {
		
		 
		 if (!Service.getService().isValidInteger(txfAntal.getText())) {
			lblError.setText("Indtast et gyldigt antal");
			btnGem.setDisable(true);
		 }
		 else if (!Service.getService().isValidDouble(txfStkPris.getText())) {
			 lblError.setText("Indtast en gyldig pris");
			 btnGem.setDisable(true);
		 }
		 else {
					try {
						double total = Double.parseDouble(txfAntal.getText())*Double.parseDouble(txfStkPris.getText()); 
						
						txfTotal.setText(total+"");
						if (!Service.getService().checkIfStockIsAvailable(aktuelPrisLinje.getProduct(), Integer.parseInt(txfAntal.getText()))) {							
							btnGem.setDisable(true);	
							lblError.setText("Det ønskede antal er ikke på lager.");
						}
						else {
							btnGem.setDisable(false);
						lblError.setText("");
						}					
		} catch (Exception e) {
			txfTotal.setText("fejl");
			btnGem.setDisable(true);
		}
		 }			
	 }
	
	 
	 /**
	  * Opretter en ny ordrelinje, med det angivne produkt, antal samt pris
	  * */
	 @FXML
	 private void addOrderLine() {
			
		
		 if (Service.getService().checkIfProductNeedsReservation(aktuelPrisLinje.getProduct())) {	
				if (txfFromDate.getValue() == null || txfToDate.getValue() == null) {
					if (txfFromDate.getValue() == null) {
						txfFromDate.setValue(LocalDate.now());
					}
					if (txfToDate.getValue() == null) {
						txfToDate.setValue(LocalDate.now());
					}
				}		 
				else if (txfFromDate.getValue().isAfter(txfToDate.getValue()) || txfFromDate.getValue().isBefore(LocalDate.now())) {
				lblError.setText("Fra dato, skal være før til dato, og ikke før dd.");
			}
			 else {	
			
			 if (aktuelOrder.getCustomer() != null) {
							
				 OrderLine ol = aktuelOrder.createOrderLine(aktuelPrisLinje.getProduct(), Integer.parseInt(txfAntal.getText()));
				 
				 Service.getService().updateOrderLineTotal(Double.parseDouble(txfTotal.getText()), ol);
				 
				 Service.getService().createReservation(txfFromDate.getValue(), txfToDate.getValue(), ol);	 			 
				 
				 stage.close();
			}
			else {
				KundeController.initWindow(aktuelOrder, stage,null,this);
			}
			 
			stage.close();	 
		}}
		 else if (aktuelPrisLinje.getProduct() instanceof Tour) {
				if (txfTourDate.getValue() == null) {
					txfTourDate.setValue(LocalDate.now());
				}
				else if (txfTourDate.getValue().isBefore(LocalDate.now())) {
					lblError.setText("Ugyldig dato - må ikke være før dd.");
				}
				 else {
					 if (aktuelOrder.getCustomer() != null) {
					 OrderLine ol = aktuelOrder.createOrderLine(aktuelPrisLinje.getProduct(), Integer.parseInt(txfAntal.getText()));
						
					 Service.getService().updateOrderLineTotal(Double.parseDouble(txfTotal.getText()), ol);
					 Service.getService().createTourBooking(txfTourDate.getValue().atTime(LocalTime.parse(cbClock.getValue())), ol);
					 Service.getService().updateBalanceOnOrder(aktuelOrder);  
											 			
					}
					else {
						KundeController.initWindow(aktuelOrder, stage,null,this);
					}
						stage.close();
				 }
				
		}
		 else {
					 
		 OrderLine ol = aktuelOrder.createOrderLine(aktuelPrisLinje.getProduct(), Integer.parseInt(txfAntal.getText()));
		 Service.getService().updateOrderLineTotal(Double.parseDouble(txfTotal.getText()),ol);
			
		 Service.getService().updateBalanceOnOrder(aktuelOrder);  			
			 
		 stage.close();		 
		 }
		
		 /* opdatere tableview med ordrelinjer */
		 parentController.updateOrderRightSide();
	 	parentController.getUniqePriceListLines();
	 	parentController.clearFilter();
		 }
	 
	 
	 public void closeCurrentStageFromCustomerStage() {
	 
	 		addOrderLine();
	 		parentController.updateOrderRightSide();
	
	 }		
}
