package gui.view;

import java.io.IOException;

import application.model.Corporate;
import application.model.Customer;
import application.model.Order;
import application.model.Private;
import application.model.evaluators.FieldEvaluator;
import application.model.evaluators.IntegerEvaluator;
import application.model.evaluators.MailEvaluator;
import application.model.evaluators.PriceEvaluator;
import application.service.Service;
import gui.MainApp;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class KundeController {
	private static Pane pane;
	private static Stage stage;
	private static Order aktuelOrder;
	private static Customer aktuelKunde;
	private static Stage parentStage;
	
	// De to parents der bliver benyttet
	private static BetalingsController betaling;
	private static TilføjProduktController tilfoej;
	
	
	 	@FXML
	    private TextField txfSoegKunde;

	    @FXML
	    private Button btnSoegKunde;

	    @FXML
	    private TextField txfNavn;

	    @FXML
	    private TextField txfAdresse;

	    @FXML
	    private TextField txfPostnummer;

	    @FXML
	    private TextField txfEmail;
	    
	    @FXML
	    private TextField txfTelefon;

	    @FXML
	    private CheckBox chErhverv;

	    @FXML
	    private HBox hbErhvervsKunde;

	    @FXML
	    private TextField txfKontaktPerson;

	    @FXML
	    private TextField txfCVR;

	    @FXML
	    private Button btnGemKunde;
	    
	    @FXML
	    private Label lblError;
	
	
public static void initWindow(Order order, Stage parentstage, BetalingsController betalingsParent,TilføjProduktController tilfoejParent) {
		
		aktuelOrder = order;
		parentStage = parentstage;
		betaling = betalingsParent;
		tilfoej = tilfoejParent;
		
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/KundeView.fxml"));
			pane = (Pane) loader.load();
			stage = new Stage();
			stage.setTitle("Kasseapparatet - Nem-Bryg");
			stage.setScene(new Scene(pane));
			stage.setResizable(false);
			stage.getIcons().add(new Image("/resources/images/icon.png"));
			stage.show();
		} catch (IOException e) {
			System.out.println(e.getMessage());

		}
	}

@FXML
   void initialize() {
	aktuelKunde = null;
	
}

@FXML
void searchForCustomer() {
	lblError.setText("");
	IntegerEvaluator iEvaluator = new IntegerEvaluator();
	if (iEvaluator.isValid(txfSoegKunde.getText())) {
		
	aktuelKunde = Service.getService().searchCustomerByPhone(Integer.parseInt(txfSoegKunde.getText()));
	
	if (aktuelKunde != null) {	
		
		chErhverv.setDisable(true);
		btnGemKunde.setText("Gem");
		
		txfNavn.setText(aktuelKunde.getName());
		txfAdresse.setText(aktuelKunde.getAddress());
		txfPostnummer.setText(aktuelKunde.getZipCode()+"");
		txfEmail.setText(aktuelKunde.getMail());
		txfTelefon.setText(aktuelKunde.getPhone()+"");
		
		if (aktuelKunde instanceof Corporate) {
			chErhverv.setSelected(true);
			hbErhvervsKunde.setVisible(true);
			txfCVR.setText(((Corporate)aktuelKunde).getCvr()+"");
			txfKontaktPerson.setText(((Corporate)aktuelKunde).getContactPerson());
		}
	}
	else {
		lblError.setText("Der er ingen kunde med det telefon nummer");
		chErhverv.setDisable(false);
		chErhverv.setSelected(false);
		
		txfNavn.setText("");
		txfAdresse.setText("");
		txfPostnummer.setText("");
		txfEmail.setText("");
		txfTelefon.setText("");
		txfCVR.setText("");
		txfKontaktPerson.setText("");
	}
}
	else {
		lblError.setText("Ugyldigt telefonnummer");
	}	
}

@FXML
void showAndHideCorporateFields() {
	if (chErhverv.isSelected()) {
		hbErhvervsKunde.setVisible(true);
	}
	else {
		hbErhvervsKunde.setVisible(false);
	}
}

@FXML
void saveCustomerToOrder() {
	if (checkUserInput()) {
		

	if (aktuelKunde == null) {
		//Opretter erhvervskunde
		if (chErhverv.isSelected()) {
		try {
			Corporate c = Service.getService().createCorporateCustomer(txfNavn.getText(), 
					txfAdresse.getText(), Integer.parseInt(txfPostnummer.getText()), Integer.parseInt(txfTelefon.getText()), txfEmail.getText(), Integer.parseInt(txfCVR.getText()), txfKontaktPerson.getText());
			Service.getService().addCustomerToOrder(aktuelOrder, c);
			
			succeedClose();
		
		} catch (Exception e) {
			lblError.setText("De indtastede data er ikke korrekte");
		}
		
		}
		
		//Opretter privatkunde
		else {
			try {
				Private c = Service.getService().createPrivateCustomer(txfNavn.getText(), 
						txfAdresse.getText(), Integer.parseInt(txfPostnummer.getText()), Integer.parseInt(txfTelefon.getText()), txfEmail.getText());
				Service.getService().addCustomerToOrder(aktuelOrder, c);
				
				succeedClose();
			}  catch (Exception e) { 
				lblError.setText("De indtastede data er ikke korrekte");
			}
		
		}
			}
	
	//Opdatere kundens oplysninger, og kobler ham sammen med ordren
	else {
		
		if (chErhverv.isSelected()) {
			
			try {
				Service.getService().updateCorporateCustomer((Corporate)aktuelKunde, txfNavn.getText(), txfAdresse.getText(),Integer.parseInt(txfPostnummer.getText()), txfEmail.getText(), Integer.parseInt(txfTelefon.getText()),Integer.parseInt(txfCVR.getText()),txfKontaktPerson.getText());
				
				Service.getService().addCustomerToOrder(aktuelOrder, aktuelKunde);

				succeedClose();
			} catch (Exception e) {
				
				lblError.setText("De indtastede data er ikke korrekte");
			}
			
				
				
		}
		else {
				try {
					
					Service.getService().updatePrivateCustomer((Private)aktuelKunde, txfNavn.getText(), txfAdresse.getText(),Integer.parseInt(txfPostnummer.getText()), txfEmail.getText(), Integer.parseInt(txfTelefon.getText()));
					
					Service.getService().addCustomerToOrder(aktuelOrder, aktuelKunde);
					
					succeedClose();
				} catch (Exception e) {
				
					lblError.setText("De indtastede data er ikke korrekte");
				}
				
		}
	}
}
}

public void succeedClose() {
		
	
		if (betaling != null) {				
			betaling.addPaymentToOrder();
		}
		else {
	
			tilfoej.closeCurrentStageFromCustomerStage();	
		}
	
	stage.close();
}

public boolean checkUserInput() {
	
	if (!Service.getService().isEmpty(txfNavn.getText())) {
		lblError.setText("Der er ikke indtastet et korrekt navn.");
		return false;
	}
	else if (!Service.getService().isEmpty(txfAdresse.getText())) {
		lblError.setText("Der er ikke indtastet en korrekt adresse.");
		return false;
	}
	else if (!Service.getService().isValidInteger(txfPostnummer.getText())) {
		lblError.setText("Der er ikke indtastet et korrekt postnummer.");
		return false;
	}
	else if (!Service.getService().isValidInteger(txfTelefon.getText())) {
		lblError.setText("Der er ikke indtastet et korrekt telefonnummer.");
		return false;
	}	
	else if (!Service.getService().isValidEmail(txfEmail.getText())) {
		lblError.setText("Der er ikke indtastet en gyldig email.");
		return false;
	}
	if (chErhverv.isSelected()) {
		if (!Service.getService().isEmpty(txfKontaktPerson.getText())) {
			lblError.setText("Der er ikke indtastet en korrekt kontakt person.");
			return false;
		}
		else if (!Service.getService().isValidInteger(txfCVR.getText())) {
			lblError.setText("Der er ikke indtastet et korrekt CVR-nummer.");
			return false;
		}
	}
	return true;
	
}

}




