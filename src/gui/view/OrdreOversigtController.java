package gui.view;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;

import application.model.Order;
import application.service.Service;
import gui.MainApp;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class OrdreOversigtController {

	private static Pane pane;
	private static Stage stage;

	@FXML
	private TableView<Order> tblOrdreOversigt;

	@FXML
	private TableColumn<Order, Integer> columnOrdreNummer;

	@FXML
	private TableColumn<Order, LocalDateTime> columnOrdreDate;

	@FXML
	private TableColumn<Order, String> columnOrdreLevering;

	@FXML
	private TableColumn<Order, String> columnOrdreAfsluttet;

	@FXML
	private TableColumn<Order, Double> columnOrdreTotal;

	@FXML
	private TableColumn<Order, String> columnPrisliste;

	@FXML
	private Button btnOrdreDetaljer;

	@FXML
	private DatePicker txfFra;

	@FXML
	private DatePicker txfTil;

	@FXML
	private Button btnFiltrer;

	public static void initWindow() {

		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/OrdreOversigt.fxml"));
			pane = (Pane) loader.load();
			stage = new Stage();
			stage.setTitle("Kasseapparatet - Nem-Bryg");
			stage.setScene(new Scene(pane));
			stage.setResizable(false);
			stage.getIcons().add(new Image("/resources/images/icon.png"));
			stage.show();
		} catch (IOException e) {
			System.out.println(e.getMessage());

		}
	}

	@FXML
	void initialize() {

		/* Oversigts tabellen */
		columnOrdreNummer.setCellValueFactory(new PropertyValueFactory<Order, Integer>("orderNumber"));
		columnOrdreDate.setCellValueFactory(new PropertyValueFactory<Order, LocalDateTime>("date"));
		columnOrdreLevering.setCellValueFactory(new PropertyValueFactory<Order, String>("DeliveryAsString"));
		columnOrdreAfsluttet.setCellValueFactory(new PropertyValueFactory<Order, String>("FinishAsString"));
		columnPrisliste.setCellValueFactory(new PropertyValueFactory<Order, String>("PriceListTitle"));
		columnOrdreTotal.setCellValueFactory(new PropertyValueFactory<Order, Double>("TotalPrice"));

		txfFra.setValue(LocalDate.now().minusYears(17));
		txfTil.setValue(LocalDate.now().plusDays(1));
		tblOrdreOversigt.getItems().setAll(Service.getService().getOrders());
	}

	@FXML
	void openOrder() {

		if (tblOrdreOversigt.getSelectionModel().getSelectedItem() != null) {

			Order order = tblOrdreOversigt.getSelectionModel().getSelectedItem();

			tblOrdreOversigt.getSelectionModel().clearSelection();

			SalgController.initWindow(order.getPriceList(), order);

			showSpecifiedOrders();
		}
	}

	@FXML
	void showSpecifiedOrders() {
		if (txfFra.getValue() == null) {
			txfFra.setValue(LocalDate.now().minusYears(17));
		}
		if (txfTil.getValue() == null) {
			txfTil.setValue(LocalDate.now().plusDays(1));
		}
		tblOrdreOversigt.getItems().clear();
		tblOrdreOversigt.getItems().setAll(Service.getService().showOrdersByDate(txfFra.getValue(), txfTil.getValue()));
	}

}
