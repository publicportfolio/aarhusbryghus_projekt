package gui.view;

import java.io.IOException;

import application.model.Product;
import application.model.products.Beer;
import application.model.products.GiftBasket;
import application.model.products.Glass;
import application.service.Service;
import gui.MainApp;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class RedigerGiftBasketController {
	private static Pane pane;
	private static Stage stage;
	private static GiftBasket giftBasket;
	private static OpretRedigerProduktController parentController;

	public static void initWindow(GiftBasket gb, OpretRedigerProduktController pc) {
		giftBasket = gb;
		parentController = pc;
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/RedigerGiftBasket.fxml"));
			pane = (Pane) loader.load();
			stage = new Stage();
			stage.setTitle("Kasseapparatet - Nem-Bryg");
			stage.setScene(new Scene(pane));
			stage.setResizable(false);
			stage.getIcons().add(new Image("/resources/images/icon.png"));
			stage.show();
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}

	@FXML
	private ListView<Product> lvwAlleFlaskeØl;

	@FXML
	private ListView<Product> lvwKurvensØl;

	@FXML
	private ListView<Product> lvwKurvensGlas;

	@FXML
	private ListView<Product> lvwAlleGlas;

	@FXML
	private Label lblErrorGlas;

	@FXML
	private Label lblErrorØl;



	@FXML
	void initialize() {
		updateLists();
	}

	public void updateLists() {
		lvwAlleFlaskeØl.getItems().setAll(Service.getService().getAllBottleBeers());
		lvwAlleGlas.getItems().setAll(Service.getService().getAllGlasses());
		lvwKurvensØl.getItems().setAll(giftBasket.getLstBeers());
		lvwKurvensGlas.getItems().setAll(giftBasket.getLstGlasses());
	}

	@FXML
	public void btnFjernGlasFraKurv() {
		clearLblError();
		Glass glass = (Glass) lvwKurvensGlas.getSelectionModel().getSelectedItem();
		if(glass==null) {
			lblErrorGlas.setText("Vælg venligst et glas fra kurven");
			return;
		}
		Service.getService().removeGlasFromGiftBasket(glass, giftBasket);
		updateLists();
		if(giftBasket.getLstGlasses().contains(glass)) {
			lvwKurvensGlas.getSelectionModel().select(glass);
		}
		parentController.updateParentController();
	}

	@FXML
	public void btnFjernØlFraKurv() {
		clearLblError();
		Beer beer = (Beer) lvwKurvensØl.getSelectionModel().getSelectedItem();
		if(beer==null) {
			lblErrorØl.setText("Vælg venligst en øl fra kurvens øl");
			return;
		}
		Service.getService().removeBeerFromGiftBasket(beer, giftBasket);
		updateLists();
		if(giftBasket.getLstBeers().contains(beer)) {
			lvwKurvensØl.getSelectionModel().select(beer);
		}
		parentController.updateParentController();
	}

	@FXML
	public void btnTilføjGlasTilKurv() {
		clearLblError();
		Glass glass = (Glass) lvwAlleGlas.getSelectionModel().getSelectedItem();
		if(glass==null) {
			lblErrorGlas.setText("Vælg venligst et glas fra alle glas listen");
			return;
		}
		if(!Service.getService().checkIfStockIsAvailable(glass, giftBasket.getInStock())){
			lblErrorGlas.setText("Der er ikke nok "+glass.getName()+" på lager til at tilføje til kurven");
			return;
		}
		Service.getService().addGlassToGiftBasket(giftBasket, glass);
		updateLists();
		lvwAlleGlas.getSelectionModel().select(glass);
		parentController.updateParentController();
	}

	@FXML
	public void btnTilføjØlTilKurv() {
		clearLblError();
		Beer beer = (Beer) lvwAlleFlaskeØl.getSelectionModel().getSelectedItem();
		if(beer==null) {
			lblErrorØl.setText("Vælg venligst en øl fra alle flaske øl listen");
			return;
		}
		if(!Service.getService().checkIfStockIsAvailable(beer, giftBasket.getInStock())){
			lblErrorØl.setText("Der er ikke nok "+beer.getName()+" på lager til at tilføje til kurven");
			return;
		}
		Service.getService().addBeerToGiftBasket(giftBasket, beer);
		updateLists();
		lvwAlleFlaskeØl.getSelectionModel().select(beer);
		parentController.updateParentController();
	}

	public void btnOkAction() {
		stage.close();
	}

	public void clearLblError(){
		lblErrorØl.setText("");
		lblErrorGlas.setText("");
	}
}
