package gui.view;

import java.io.IOException;

import application.model.PriceList;
import application.service.Service;
import gui.MainApp;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class OpretRedigerProduktListeController {
	private static Pane pane;
	private static Stage stage;
	private static PriceList priceList;
	private static LagerStyringController parentController;

	public static void initWindow(PriceList pl, LagerStyringController parent) {
		priceList = pl;
		parentController = parent;
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/OpretRedigerProduktListe.fxml"));
			pane = (Pane) loader.load();
			stage = new Stage();
			stage.setTitle("Kasseapparatet - Nem-Bryg");
			stage.setScene(new Scene(pane));
			stage.setResizable(false);
			stage.getIcons().add(new Image("/resources/images/icon.png"));
			stage.show();
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}

	@FXML
	private Label lblTitel;

	@FXML
	private Label lblError;

	@FXML
	private TextField txfNavn;

	@FXML
	private Button btnTilføj;

	@FXML
	private Button btnAnnuller;

	@FXML
	private Button btnOpdater;


	@FXML
	public void initialize() {
		updateSituation();
	}

	public void updateSituation() {
		if (priceList!=null) {
			lblTitel.setText("Opdater liste");
			btnTilføj.setVisible(false);
			btnOpdater.setVisible(true);
			txfNavn.setText(priceList.getTitle());
		}
	}

	public void updateParrentController() {
		parentController.fillCBProductListTitles();
	}

	@FXML
	public void btnAnnullerAction() {
		priceList = null;
		stage.close();
	}

	@FXML
	public void btnOpdaterAction() {
		String text = txfNavn.getText().trim();
		if (text.isEmpty()) {
			lblError.setText("Fejl, navn må ikke være tom");
			return;
		}
		Service.getService().updatePriceList(priceList, txfNavn.getText().trim());
		updateParrentController();
		btnAnnullerAction();
	}

	@FXML
	public void btnTilføjAction() {
		String text = txfNavn.getText().trim();
		if (text.isEmpty()) {
			lblError.setText("Fejl, navn må ikke være tom");
			return;
		}
		Service.getService().createPriceList(txfNavn.getText().trim());
		updateParrentController();
		btnAnnullerAction();
	}
}
