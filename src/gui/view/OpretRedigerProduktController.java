package gui.view;

import java.io.IOException;

import application.model.Product;
import application.model.products.Beer;
import application.model.products.CarbonicAcid;
import application.model.products.Delivery;
import application.model.products.DraftBeerMachine;
import application.model.products.GiftBasket;
import application.model.products.Glass;
import application.model.products.Keg;
import application.model.products.Tour;
import application.model.products.Voucher;
import application.service.Service;
import gui.MainApp;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class OpretRedigerProduktController {
	private static Pane pane;
	private static Stage stage;
	private static Product product;
	private static LagerStyringController parentController;

	public static void initWindow(LagerStyringController controller, Product p) {
		product = p;
		parentController = controller;

		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/OpretRedigerProdukt.fxml"));
			pane = (Pane) loader.load();
			stage = new Stage();
			stage.setTitle("Kasseapparatet - Nem-Bryg");
			stage.setScene(new Scene(pane));
			stage.setResizable(false);
			stage.getIcons().add(new Image("/resources/images/icon.png"));
			stage.show();
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}

	@FXML
	private ComboBox<String> cbProdukter;

	@FXML
	private ComboBox<Integer> cbAntalKlip;

	@FXML
	private Label lblTitel;

	@FXML
	private Label lblError;

	@FXML
	private Button btnTilbage;

	@FXML
	private Button btnOpretOpdater;

	@FXML
	private Button btnRedigerKurv;

	@FXML
	private AnchorPane apaneFustage;

	@FXML
	private AnchorPane apaneBeer;

	@FXML
	private AnchorPane apaneKlippekort;

	@FXML
	private AnchorPane apaneKulsyre;

	@FXML
	private AnchorPane apaneGiftBasket;

	@FXML
	private TextArea txaBeskrivelse;

	@FXML
	private TextField txfNavn;

	@FXML
	private TextField txfInStock;

	@FXML
	private TextField txfCl;

	@FXML
	private TextField txfAlkoholProcent;

	@FXML
	private TextField txfAntalLiter;

	@FXML
	private TextField txfAntalKg;

	@FXML
	void initialize() {
		fillCBProdukter();
		guiSetupForUpdatingProduct();
	}

	public void guiSetupForUpdatingProduct() {
		if (product!=null) {
			lblTitel.setText("Rediger produkt");
			hideAllExtraPanes();
			if(product instanceof Beer) {
				Beer beer = (Beer)product;
				cbProdukter.setValue("Øl");
				txfCl.setText(beer.getCl()+"");
				txfAlkoholProcent.setText(beer.getAlcohol()+"");
				apaneBeer.setVisible(true);
				btnOpretOpdater.setText("Opdater øl");
				btnOpretOpdater.setOnAction(event->updateBeerAction());
			}
			else if(product instanceof Voucher) {
				Voucher voucher = (Voucher)product;
				cbProdukter.setValue("Klippekort");
				fillCBAntalKlip();
				cbAntalKlip.setValue(voucher.getStamp());
				apaneKlippekort.setVisible(true);
				btnOpretOpdater.setText("Opdater klippekort");
				btnOpretOpdater.setOnAction(event->updateVoucherAction());
			}
			else if(product instanceof Glass) {
				cbProdukter.setValue("Glas");
				btnOpretOpdater.setText("Opdater glas");
				btnOpretOpdater.setOnAction(event->updateProductAction());
			}
			else if(product instanceof Tour) {
				cbProdukter.setValue("Rundvisning");
				btnOpretOpdater.setText("Opdater rundvisning");
				btnOpretOpdater.setOnAction(event->updateProductAction());
			}
			else if(product instanceof Keg) {
				Keg keg = (Keg)product;
				cbProdukter.setValue("Fustage");
				txfAntalLiter.setText(keg.getLiter()+"");
				apaneFustage.setVisible(true);
				btnOpretOpdater.setText("Opdater fustage");
				btnOpretOpdater.setOnAction(event->updateKegAction());
			}
			else if(product instanceof CarbonicAcid) {
				CarbonicAcid carbonicAcid = (CarbonicAcid)product;
				cbProdukter.setValue("Kulsyre");
				txfAntalKg.setText(carbonicAcid.getKg()+"");
				apaneKulsyre.setVisible(true);
				btnOpretOpdater.setText("Opdater kulsyre");
				btnOpretOpdater.setOnAction(event->updateCarbonicAcidAction());
			}
			else if(product instanceof DraftBeerMachine) {
				cbProdukter.setValue("Fadølsanlæg");
				btnOpretOpdater.setText("Opdater fadølsanlæg");
				btnOpretOpdater.setOnAction(event->updateDraftBeerMachineAction());
			}
			else if(product instanceof GiftBasket) {
				cbProdukter.setValue("Gavekurv");
				apaneGiftBasket.setVisible(true);
				btnOpretOpdater.setText("Opdater gavekurv");
				btnOpretOpdater.setOnAction(event->updateGiftBasketAction());
			}
			else if(product instanceof Delivery) {
				cbProdukter.setValue("Levering");
				btnOpretOpdater.setText("Opdater levering");
				btnOpretOpdater.setOnAction(event->updateDeliveryAction());
			}
			else {
				btnOpretOpdater.setText("Opdater diverse");
				btnOpretOpdater.setOnAction(event->updateProductAction());
			}
			txfNavn.setText(product.getName());
			txaBeskrivelse.setText(product.getDescription());
			txfInStock.setText(product.getInStock()+"");
			cbProdukter.setDisable(true);
		}
	}

	public void fillCBProdukter() {
		cbProdukter.getItems().add("Diverse");
		cbProdukter.getItems().add("Øl");
		cbProdukter.getItems().add("Klippekort");
		cbProdukter.getItems().add("Glas");
		cbProdukter.getItems().add("Fustage");
		cbProdukter.getItems().add("Kulsyre");
		cbProdukter.getItems().add("Fadølsanlæg");
		cbProdukter.getItems().add("Gavekurv");
		cbProdukter.getItems().add("Rundvisning");
		cbProdukter.getItems().add("Levering");
		cbProdukter.setValue("Diverse");
	}

	public void fillCBAntalKlip() {
		int TOTALKLIP = 100;
		for(int i = 1; i<=TOTALKLIP;i++) {
			cbAntalKlip.getItems().add(i);
		}
		cbAntalKlip.setValue(1);
	}

	@FXML
	public void btnTilbageAction() {
		closeWindow();
	}

	@FXML
	public void btnRedigerGiftBasketAction() {
		GiftBasket giftBasket = (GiftBasket)product;
		RedigerGiftBasketController.initWindow(giftBasket,this);
	}

	@FXML
	public void cbProdukterAction() {
		clearAllTextFields();
		hideAllExtraPanes();
		String chosen = cbProdukter.getValue();
		if(chosen.equals("Diverse")) {
			btnOpretOpdater.setOnAction(event->createProductAction());
			btnOpretOpdater.setText("Opret diverse");
		}
		else if (chosen.equals("Øl")) {
			apaneBeer.setVisible(true);
			btnOpretOpdater.setOnAction(event->createBeerAction());
			btnOpretOpdater.setText("Opret øl");
		}
		else if (chosen.equals("Glas")) {
			btnOpretOpdater.setOnAction(event->createGlassAction());
			btnOpretOpdater.setText("Opret glas");
		}
		else if (chosen.equals("Klippekort")) {
			apaneKlippekort.setVisible(true);
			fillCBAntalKlip();
			btnOpretOpdater.setOnAction(event->createVoucherAction());
			btnOpretOpdater.setText("Opret klippekort");
		}
		else if (chosen.equals("Fustage")) {
			apaneFustage.setVisible(true);
			btnOpretOpdater.setOnAction(event->createKegAction());
			btnOpretOpdater.setText("Opret fustage");
		}
		else if (chosen.equals("Kulsyre")) {
			apaneKulsyre.setVisible(true);
			btnOpretOpdater.setOnAction(event->createCarbonicAcidAction());
			btnOpretOpdater.setText("Opret kulsyre");
		}
		else if (chosen.equals("Fadølsanlæg")) {
			btnOpretOpdater.setOnAction(event->createDraftBeerMachine());
			btnOpretOpdater.setText("Opret Fadølsanlæg");
		}
		else if (chosen.equals("Rundvisning")) {
			btnOpretOpdater.setOnAction(event->createTourAction());
			btnOpretOpdater.setText("Opret rundvisning");
		}
		else if (chosen.equals("Gavekurv")) {
			btnOpretOpdater.setOnAction(event->createGiftBasketAction());
			btnOpretOpdater.setText("Opret gavekurv");
		}
		else if (chosen.equals("Levering")) {
			btnOpretOpdater.setOnAction(event->createDeliveryAction());
			btnOpretOpdater.setText("Opret levering");
		}
	}

	public void createProductAction() {
		if(evaluateCommonFields()) {
			return;
		}
		String navn = txfNavn.getText().trim();
		String beskrivelse = txaBeskrivelse.getText().trim();
		int inStock = Integer.parseInt(txfInStock.getText());
		Service.getService().createProduct(navn, beskrivelse, inStock);
		updateParentController();
		closeWindow();
	}

	public void createDeliveryAction() {
		if(evaluateCommonFields()) {
			return;
		}
		String navn = txfNavn.getText().trim();
		String beskrivelse = txaBeskrivelse.getText().trim();
		int inStock = Integer.parseInt(txfInStock.getText());
		Service.getService().createDelivery(navn, beskrivelse, inStock);
		updateParentController();
		closeWindow();
	}

	public void createBeerAction() {
		if(evaluateCommonFields()||evaluateBeerFields()) {
			return;
		}
		String navn = txfNavn.getText().trim();
		String beskrivelse = txaBeskrivelse.getText().trim();
		int inStock = Integer.parseInt(txfInStock.getText().trim());
		double cl = Double.parseDouble(txfCl.getText().trim());
		double alkoholprocent = Double.parseDouble(txfAlkoholProcent.getText().trim());
		Service.getService().createBeer(navn, beskrivelse, inStock, cl,alkoholprocent);
		updateParentController();
		closeWindow();
	}

	public void createGlassAction() {
		if(evaluateCommonFields()) {
			return;
		}
		String navn = txfNavn.getText().trim();
		String beskrivelse = txaBeskrivelse.getText().trim();
		int inStock = Integer.parseInt(txfInStock.getText().trim());
		Service.getService().createGlass(navn, beskrivelse, inStock);
		updateParentController();
		closeWindow();
	}

	public void createVoucherAction() {
		if(evaluateCommonFields()) {
			return;
		}
		String navn = txfNavn.getText().trim();
		String beskrivelse = txaBeskrivelse.getText().trim();
		int inStock = Integer.parseInt(txfInStock.getText().trim());
		int stamp = cbAntalKlip.getValue();
		Service.getService().createVoucher(navn, beskrivelse, inStock, stamp);
		updateParentController();
		closeWindow();
	}

	public void createKegAction() {
		if(evaluateCommonFields() || evaluateKegFields()) {
			return;
		}
		String navn = txfNavn.getText().trim();
		String beskrivelse = txaBeskrivelse.getText().trim();
		int inStock = Integer.parseInt(txfInStock.getText().trim());
		double liter = Double.parseDouble(txfAntalLiter.getText().trim());
		Service.getService().createKeg(navn, beskrivelse, inStock, liter);
		updateParentController();
		closeWindow();
	}

	public void createCarbonicAcidAction() {
		if(evaluateCommonFields()||evaluateCarbonicAcidFields()) {
			return;
		}
		String navn = txfNavn.getText().trim();
		String beskrivelse = txaBeskrivelse.getText().trim();
		int inStock = Integer.parseInt(txfInStock.getText().trim());
		double kg = Double.parseDouble(txfAntalKg.getText().trim());
		Service.getService().createCarbonicAcid(navn, beskrivelse, inStock, kg);
		updateParentController();
		closeWindow();
	}

	public void createDraftBeerMachine() {
		if(evaluateCommonFields()) {
			return;
		}
		String navn = txfNavn.getText().trim();
		String beskrivelse = txaBeskrivelse.getText().trim();
		int inStock = Integer.parseInt(txfInStock.getText().trim());
		Service.getService().createDraftBeerMachine(navn, beskrivelse, inStock);
		updateParentController();
		closeWindow();
	}

	public void createTourAction() {
		if(evaluateCommonFields()) {
			return;
		}
		String navn = txfNavn.getText().trim();
		String beskrivelse = txaBeskrivelse.getText().trim();
		int inStock = Integer.parseInt(txfInStock.getText().trim());
		Service.getService().createTour(navn, beskrivelse, inStock);
		updateParentController();
		closeWindow();
	}

	public void createGiftBasketAction() {
		if(evaluateCommonFields()) {
			return;
		}
		String navn = txfNavn.getText().trim();
		String beskrivelse = txaBeskrivelse.getText().trim();
		int inStock = Integer.parseInt(txfInStock.getText().trim());
		GiftBasket giftBasket = Service.getService().createGiftBasket(navn,beskrivelse,inStock);
		RedigerGiftBasketController.initWindow(giftBasket, this);
		updateParentController();
		closeWindow();
	}

	public void updateBeerAction() {
		if(evaluateCommonFields()||evaluateBeerFields()) {
			return;
		}
		Beer beer = (Beer)product;
		String name = txfNavn.getText().trim();
		String description = txaBeskrivelse.getText().trim();
		int inStock = Integer.parseInt(txfInStock.getText().trim());
		double cl = Double.parseDouble(txfCl.getText().trim());
		double alcohol = Double.parseDouble(txfAlkoholProcent.getText().trim());
		Service.getService().updateBeer(beer, name, description, inStock, cl, alcohol);
		updateParentController();
		closeWindow();
	}

	public void updateVoucherAction() {
		if(evaluateCommonFields()) {
			return;
		}
		Voucher voucher = (Voucher)product;
		String name = txfNavn.getText().trim();
		String description = txaBeskrivelse.getText().trim();
		int inStock = Integer.parseInt(txfInStock.getText().trim());
		int stamp = cbAntalKlip.getValue();
		Service.getService().updateVoucher(voucher, name, description, inStock, stamp);
		updateParentController();
		closeWindow();
	}

	public void updateProductAction() {
		if(evaluateCommonFields()) {
			return;
		}
		String name = txfNavn.getText().trim();
		String description = txaBeskrivelse.getText().trim();
		int inStock = Integer.parseInt(txfInStock.getText().trim());
		Service.getService().updateProduct(product, name, description, inStock);
		updateParentController();
		closeWindow();
	}

	public void updateDeliveryAction() {
		if(evaluateCommonFields()) {
			return;
		}
		String name = txfNavn.getText().trim();
		String description = txaBeskrivelse.getText().trim();
		int inStock = Integer.parseInt(txfInStock.getText().trim());
		Service.getService().updateDelivery((Delivery)product, name, description, inStock);
		updateParentController();
		closeWindow();
	}

	public void updateKegAction() {
		if(evaluateCommonFields() || evaluateKegFields()) {
			return;
		}
		Keg keg = (Keg) product;
		String name = txfNavn.getText().trim();
		String description = txaBeskrivelse.getText().trim();
		int inStock = Integer.parseInt(txfInStock.getText().trim());
		double liter = Double.parseDouble(txfAntalLiter.getText().trim());
		Service.getService().updateKeg(keg,name,description,inStock,liter);
		updateParentController();
		closeWindow();
	}

	public void updateCarbonicAcidAction(){
		if(evaluateCommonFields()||evaluateCarbonicAcidFields()) {
			return;
		}
		CarbonicAcid carbonicAcid = (CarbonicAcid)product;
		String name = txfNavn.getText().trim();
		String description = txaBeskrivelse.getText().trim();
		int inStock = Integer.parseInt(txfInStock.getText().trim());
		double kg = Double.parseDouble(txfAntalKg.getText().trim());
		Service.getService().updateCarbonicAcid(carbonicAcid, name, description, inStock, kg);
		updateParentController();
		closeWindow();
	}

	public void updateDraftBeerMachineAction() {
		if(evaluateCommonFields()) {
			return;
		}
		DraftBeerMachine draftBeerMachine = (DraftBeerMachine)product;
		String name = txfNavn.getText().trim();
		String description = txaBeskrivelse.getText().trim();
		int inStock = Integer.parseInt(txfInStock.getText().trim());
		Service.getService().updateDraftBeerMachine(draftBeerMachine, name, description, inStock);
		updateParentController();
		closeWindow();
	}

	public void updateGiftBasketAction() {
		if(evaluateCommonFields()) {
			return;
		}
		int inStock = Integer.parseInt(txfInStock.getText().trim());
		int stockDifference = inStock-product.getInStock();
		GiftBasket giftBasket = (GiftBasket)product;
		try{
			Service.getService().updateGiftBasketStock(giftBasket, stockDifference);
		}
		catch(RuntimeException e) {
			lblError.setText(e.getMessage());
			return;
		}
		String name = txfNavn.getText().trim();
		String description = txaBeskrivelse.getText().trim();
		Service.getService().updateProduct(product, name, description, product.getInStock());
		updateParentController();
		closeWindow();
	}

	public void updateParentController() {
		parentController.fillTblProducts();
		parentController.tblProductsOnMouseClickedAction();
	}

	public void clearLblError() {
		lblError.setText("");
	}

	public void clearAllTextFields() {
		txaBeskrivelse.clear();
		txfNavn.clear();
		txfInStock.clear();
		txfCl.clear();
		txfAlkoholProcent.clear();
		txfAntalLiter.clear();
		txfAntalKg.clear();
	}

	public void hideAllExtraPanes() {
		apaneFustage.setVisible(false);
		apaneBeer.setVisible(false);
		apaneKlippekort.setVisible(false);
		apaneKulsyre.setVisible(false);
	}

	public void closeWindow() {
		product = null;
		stage.close();
	}

	public boolean evaluateCommonFields() {
		if(!Service.getService().isEmpty(txfNavn.getText().trim())){
			lblError.setText("Fejl ved navn, må ikke være tomt");
			return true;
		}
		if(!Service.getService().isValidInteger(txfInStock.getText().trim())){
			lblError.setText("Fejl ved antal på lager");
			return true;
		}
		return false;
	}

	public boolean evaluateBeerFields() {
		if(!Service.getService().isValidDouble(txfCl.getText().trim())){
			lblError.setText("Fejl ved centiliter");
			return true;
		}
		if(!Service.getService().isValidDouble(txfAlkoholProcent.getText().trim())){
			lblError.setText("Fejl ved alkoholprocent");
			return true;
		}
		return false;
	}

	public boolean evaluateKegFields() {
		if(!Service.getService().isValidDouble(txfAntalLiter.getText().trim())){
			lblError.setText("Fejl ved liter");
			return true;
		}
		return false;
	}

	public boolean evaluateCarbonicAcidFields() {
		if(!Service.getService().isValidDouble(txfAntalKg.getText().trim())){
			lblError.setText("Fejl ved antal kg");
			return true;
		}
		return false;
	}
}
