package gui.view;

import java.io.IOException;
import java.time.LocalDateTime;

import application.model.Order;
import application.model.Payment;
import application.model.PaymentMethod;
import application.model.PriceListLine;
import application.model.evaluators.IntegerEvaluator;
import application.model.evaluators.PriceEvaluator;
import application.service.Service;
import gui.MainApp;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class BetalingsController {
	private static Pane pane;
	private static Stage stage;
	private static Order aktuelOrder;
	private static SalgController parentController;
	
	private ToggleGroup tgPaymentHolder;

	private double addToBalance;
	
	 @FXML
	 private Label lblAmount;
	 
	 @FXML
	 private Label lblError;
		
	
    @FXML
    private VBox vbPayment;
    
    @FXML
    private TextField txfBeloeb;

    @FXML
    private TableView<Payment> tblBetalinger;

    @FXML
    private TableColumn<Payment, PaymentMethod> columnType;

    @FXML
    private TableColumn<Payment, LocalDateTime> columnTidspunkt;

    @FXML
    private TableColumn<Payment, Double> columnPris;

    @FXML
    private TextField txfRestBeloeb;

    @FXML
    private Button btnIndbetal;
    
    @FXML
    private Button btnSletBetaling;
    
	
	public static void initWindow(Order order, SalgController salgController) {
		
		aktuelOrder = order;
		parentController = salgController;
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/BetalingsView.fxml"));
			pane = (Pane) loader.load();
			stage = new Stage();
			stage.setTitle("Kasseapparatet - Nem-Bryg");
			stage.setScene(new Scene(pane));
			stage.setResizable(false);
			stage.getIcons().add(new Image("/resources/images/icon.png"));	
			stage.show();
				
		} catch (IOException e) {
			System.out.println(e.getMessage());

		}
	}
	
	@FXML
	void initialize() {
		
		
		tgPaymentHolder = new ToggleGroup();
		
		for (PaymentMethod paymentMethod : PaymentMethod.values()) {
			RadioButton rb = new RadioButton();
			rb.setUserData(paymentMethod);
			rb.setText(paymentMethod+"");
			rb.setToggleGroup(tgPaymentHolder);
			vbPayment.getChildren().add(rb);
			if (paymentMethod.equals(PaymentMethod.Dankort)) {
				rb.setSelected(true);
			}
		}
		
		 /* Betalings tabellen */
		 columnType.setCellValueFactory(new PropertyValueFactory<Payment, PaymentMethod>("paymentMethod"));
		 columnTidspunkt.setCellValueFactory(new PropertyValueFactory<Payment, LocalDateTime>("date"));	
		 columnPris.setCellValueFactory(new PropertyValueFactory<Payment, Double>("payed"));
		 tblBetalinger.getItems().setAll(aktuelOrder.getLstPayments());
		 
		 txfBeloeb.setText(aktuelOrder.getBalance()+"");
		 txfRestBeloeb.setEditable(false);
		 txfRestBeloeb.setText(aktuelOrder.getBalance()+"");
		 
		 tgPaymentHolder.selectedToggleProperty().addListener(event -> toggleAction());
		 tblBetalinger.getSelectionModel().selectedItemProperty().addListener(event -> activateDelete());
		 
	}
	
	
	public void toggleAction() {
		if (tgPaymentHolder.getSelectedToggle().getUserData() == PaymentMethod.Klippekort) {
			lblAmount.setText("Antal klip");
			btnIndbetal.setText("Anvend klip");
			txfBeloeb.setText("1");
		}
		else {
			lblAmount.setText("Beløb");
			btnIndbetal.setText("Indbetal");
			txfBeloeb.setText(aktuelOrder.getBalance()+"");
		}
	}
	
	
	public void activateDelete() {
		if (tblBetalinger.getSelectionModel().getSelectedItem() != null) {
			btnSletBetaling.setDisable(false);
		}
	}
	
	@FXML
	void addPaymentToOrder() {
		
		if (tgPaymentHolder.getSelectedToggle().getUserData() == PaymentMethod.Klippekort && !Service.getService().isValidInteger(txfBeloeb.getText())){
			
			lblError.setText("Ugyldigt antal klip");
		}
		else if (!Service.getService().isValidDouble(txfBeloeb.getText())) {
			lblError.setText("Ugyldigt beløb");
		}
		else {
			
		
		addToBalance = Double.parseDouble(txfBeloeb.getText());
		if (addToBalance > 0) {
			
		if (tgPaymentHolder.getSelectedToggle().getUserData() != PaymentMethod.Regning) {
			if (tgPaymentHolder.getSelectedToggle().getUserData() == PaymentMethod.Klippekort) {
				addToBalance = (Integer.parseInt(txfBeloeb.getText()) * 30);
				if (addToBalance > aktuelOrder.getBalance()) {
					addToBalance = aktuelOrder.getBalance();
				}
			}
			else {
				addToBalance = Double.parseDouble(txfBeloeb.getText());
			}
			tryToCreateThePayment();
		}
		
		else {
			
			if (aktuelOrder.getCustomer() != null) {
				tryToCreateThePayment();
			}
			else {
				KundeController.initWindow(aktuelOrder, stage, this,null);
			}
		}
		}
		else {
			lblError.setText("Beløbet er for lavt");
		}
			}}
	
	public void updateContent() {
		txfRestBeloeb.setText(aktuelOrder.getBalance()+"");
		
		if (tgPaymentHolder.getSelectedToggle().getUserData() == PaymentMethod.Klippekort) {
			txfBeloeb.setText("1");
		}
		else {
			txfBeloeb.setText(aktuelOrder.getBalance()+"");
		
		}
		
		if (aktuelOrder.getBalance() == 0) {
			btnIndbetal.setDisable(true);
		}
		else {
			btnIndbetal.setDisable(false);
		}
		
		tblBetalinger.getItems().clear();
		tblBetalinger.getItems().setAll(aktuelOrder.getLstPayments());
		parentController.updateOrderRightSide();
		parentController.getUniqePriceListLines();
	}
	
	public void tryToCreateThePayment() {
		
		try {
			Payment p = Service.getService().createPayment(aktuelOrder,(PaymentMethod)tgPaymentHolder.getSelectedToggle().getUserData(), LocalDateTime.now(),addToBalance);
			
			updateContent();	
			
		} catch (IllegalArgumentException e) {
			txfBeloeb.setText(aktuelOrder.getBalance()+"");
		}
	}
	
	@FXML
	void removePayment() {
		if (tblBetalinger.getSelectionModel().getSelectedItem()!= null) {
			try {
				Service.getService().removePayment(aktuelOrder,tblBetalinger.getSelectionModel().getSelectedItem());
				updateContent();
				btnSletBetaling.setDisable(true);
			} catch (Exception e) {
				// TODO: handle exception
			}		
		}
	}

	
}
